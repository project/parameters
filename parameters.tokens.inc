<?php

/**
 * @file
 * Tokens provided by the Parameters module.
 */

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RenderableInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Entity\ParametersCollectionStorage;
use Drupal\parameters\Parameter;
use Drupal\parameters\Plugin\Parameter\NullObject;
use Drupal\parameters\Plugin\ParameterManager;

/**
 * Implements hook_token_info().
 */
function parameters_token_info(): array {
  $info = [];
  $info['types']['parameter'] = [
    'name' => t('Parameter'),
    'description' => t('Tokens provided by configured parameters.'),
  ];
  $plugin_manager = ParameterManager::get();
  /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
  foreach (ParametersCollectionStorage::get()->loadMultiple() as $collection) {
    $parameters_array = $collection->get('parameters');
    if (empty($parameters_array)) {
      continue;
    }
    $id = $collection->id();
    /** @var \Drupal\parameters\Plugin\ParameterInterface $parameter */
    foreach ($parameters_array as $name => $parameter_config) {
      if (!($definition = $plugin_manager->getDefinition($parameter_config['type'], FALSE))) {
        continue;
      }
      $info['tokens']['parameter'][$id . ':' . $name] = [
        'name' => ($parameter_config['label'] ?? $name) . ' (' . $definition['label'] . ')',
        'description' => $parameter_config['description'] ?? '',
      ];
    }
  }
  return $info;
}

/**
 * Implements hook_tokens().
 */
function parameters_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];

  $parameter_tokens = $type === 'parameter' ? $tokens : \Drupal::token()->findWithPrefix($tokens, 'parameter');

  if ($parameter_tokens) {
    $bubbleable_metadata->addCacheTags(\Drupal::entityTypeManager()->getDefinition(ParametersCollectionInterface::ENTITY_TYPE_ID)->getListCacheTags());
    $storage = ParametersCollectionStorage::get();
    foreach ($parameter_tokens as $name => $original) {
      $parts = explode(':', $name, 2);
      if ($collection = $storage->load(reset($parts))) {
        array_shift($parts);
        if (empty($parts)) {
          continue;
        }
        $parameter = Parameter::get(reset($parts), $collection);
        $root_name_parts = explode('.', str_replace(':', '.', reset($parts)), 2);
        $root_parameter = count($root_name_parts) > 1 ? Parameter::get(reset($root_name_parts), $collection) : $parameter;
      }
      else {
        $context = isset($data[$type]) ? [$data[$type]] : (count($data) === 1 ? array_values($data) : []);
        $parameter = Parameter::get($name, ...$context);
        $root_name_parts = explode('.', str_replace(':', '.', $name), 2);
        $root_parameter = count($root_name_parts) > 1 ? Parameter::get(reset($root_name_parts), $collection) : $parameter;
      }

      if ($parameter instanceof NullObject) {
        // A null object represents a non-existent parameter.
        continue;
      }

      if ($parameter instanceof CacheableDependencyInterface) {
        $bubbleable_metadata->addCacheableDependency($parameter);
      }
      if (($parameter !== $root_parameter) && ($root_parameter instanceof CacheableDependencyInterface)) {
        $bubbleable_metadata->addCacheableDependency($root_parameter);
      }

      if ($parameter instanceof RenderableInterface) {
        $build = $parameter->toRenderable();
        /** @var \Drupal\Core\Render\RendererInterface $renderer */
        $renderer = \Drupal::service('renderer');
        $replacements[$original] = $renderer->executeInRenderContext(new RenderContext(), fn () => $renderer->render($build));
        $bubbleable_metadata->addCacheableDependency(BubbleableMetadata::createFromRenderArray($build));
      }
      else {
        $string_value = $parameter->getProcessedData()->getString();
        if (($string_value === '') && is_array($data_value = $parameter->getProcessedData()->getValue())) {
          // Last chance to get a string representation value.
          $scalar_only = TRUE;
          foreach ($data_value as $v) {
            if (!is_scalar($v) && !is_null($v) && !(is_object($v) && method_exists($v, '__toString'))) {
              $scalar_only = FALSE;
            }
          }
          if ($scalar_only) {
            $string_value = implode(', ', $data_value);
          }
        }

        // The parameter plugin itself is responsible for safely handling input.
        // Therefore creating a Markup object here to avoid double escaping.
        $replacements[$original] = Markup::create($string_value);
      }
    }
  }

  return $replacements;
}
