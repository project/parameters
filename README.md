# Parameters

**Configure arbitrary parameters.**

https://www.drupal.org/project/parameters

## 0. Contents

- 1. Introduction
- 2. Requirements
- 3. Installation
- 4. Usage
- 5. Complementary projects
- 6. Maintainers
- 7. Support and contribute
- 8. Setup of passphrase salt

## 1. Introduction

This module enables to configure arbitrary parameters. Parameters are a native
part of configuration management. They may be seen like dynamic and user-defined
configuration properties, fully manageable and exportable. You can create global
parameters for your site, but also manage parameters specific for content types.

## 2. Requirements

This module does not require anything outside of Drupal core.

## 3. Installation

Install the module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

## 4. Usage

After installation, you should take a look at the permissions page at
`/admin/people/permissions` and make sure whether the configured permissions are
properly set.

For a first time usage and to manage parameters configuration through the user
interface, you need to install the included sub-module "Parameters UI". Once
that is installed, you have a new tab in the field UI, i.e. everywhere a
"Manage fields" tab exists for managing the fields of a certain content entity
type, a further tab "Manage parameters" appears. You can configure all existing
parameters at `/admin/config/parameters`.

Parameters are automatically available as tokens. Example tokens:

```html
[parameter:global:maximum_capacity]
For a "maximum_capacity" configured as parameter within the global collection.
```

```html
[parameter:node.article:maximum_capacity]
For a "maximum_capacity" configured as parameter for the content type "article".
```

```html
[node:parameter:maximum_capacity]
When a node is available, this will read out the "maximum_capacity" parameter
configured for its according content type. When the content type has no such
parameter defined, but the global collection has one, the global one will be
used.
```

```html
[parameter:maximum_capacity]
This will read out contextually according parameter collections. For example,
when a node is provided, it will behave just like
[node:parameter:maximum_capacity] as described above. If not, it will always
finally look into the global collection for an available parameter.
```

## 4.1 API usage

Parameters give a guarantee of availability. By default, configurations of
parameters are being automatically locked so that they don't get removed.
This behavior may be adjusted by setting the service parameter
`parameters_collection.autolock` to either `true` or `false`.

A parameter can be gracefully loaded using the `Drupal\parameters\Parameter`
helper class. Example:

```php
use Drupal\parameters\Parameter;

$parameter = Parameter::get('max_capacity', $node);
// May print the number of your configured capacity.
echo $parameter->getProcessedData()->getValue();

// You can also use "strict" mode to let the API throw an exception in case the
// parameter is not available:
$parameter = Parameter::get('max_capacity', $node, 'strict');
```

## 5. Complementary projects

This module contains additional sub-modules, all optional:
- **Parameters UI**: A user interface for Parameters. You most likely want to
  install this module to setup parameters through the web user interface.

When working with Tokens, the contrib Token module
(https://www.drupal.org/project/token) is recommended to be installed.
That module provides a Token browser for a convenient way to browse through
available Tokens.

## 6. Maintainers

* Maximilian Haupt (mxh) - https://www.drupal.org/u/mxh

## 7. Support and contribute

To submit bug reports and feature suggestions, or to track changes visit:
https://www.drupal.org/project/issues/parameters

You can also use this issue queue for contributing, either by submitting ideas,
or new features and mostly welcome - patches and tests.

## 8. Setup of passphrase salt

When using secret parameters, then it is recommended to setup a passphrase salt
within the `settings.php` file. Then secret parameters can be shared accross
multiple environments. Example:

```php
$settings['secret_parameters.salt'] = getenv('SECRET_PARAMETERS_SALT');
```

For an alternative, the salt may be set as Symfony service parameter instead.
The according parameter can be found within the `parameters.services.yml` file.

When not specified either with one of the described methods above, then the
"hash_salt" provided by `settings.php` will be used, if given. In case also that
is not given, then a random salt will be generated and persisted within Drupal
state. This is fine for single environments but may not work well for
multi-environment setups.
