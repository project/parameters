parameters.collection.*:
  type: config_entity
  label: 'Parameters collection'
  mapping:
    id:
      type: string
      label: 'ID'
    label:
      type: label
      label: 'Label'
    status:
      type: boolean
      label: 'Whether this configuration is enabled.'
    locked:
      type: boolean
      label: 'Whether this collection is locked.'
    deletable:
      type: boolean
      label: 'Whether this collection is deletable.'
    parameters:
      type: sequence
      label: 'Parameters'
      sequence:
        type: parameter.[type]
        label: 'Parameter'

parameter_plugin:
  type: mapping
  label: 'Parameter plugin config'
  mapping:
    name:
      type: string
      label: 'Parameter machine name'
    label:
      type: label
      label: 'Parameter human-readable label'
    description:
      type: text
      label: 'Parameter description'
    type:
      type: string
      label: 'Parameter type'
    weight:
      type: integer
      label: 'Weight'
    third_party_settings:
      type: sequence
      label: 'Third party settings'
      sequence:
        type: parameters.third_party.[%key]

parameter.machine_name:
  type: parameter_plugin
  label: 'Machine name'
  mapping:
    value:
      type: string
      label: 'Machine name value'

parameter.string:
  type: parameter_plugin
  label: 'Raw string'
  mapping:
    value:
      type: text
      label: 'Raw string value'

parameter.datetime:
  type: parameter_plugin
  label: 'Date and time'
  mapping:
    value:
      type: string
      label: 'Date and time value as ISO8601 string in UTC timezone.'

parameter.text:
  type: parameter_plugin
  label: 'Formatted text'
  mapping:
    text:
      type: text_format
      label: 'Text value'

parameter.increment:
  type: parameter_plugin
  label: 'Incrementing integer'
  mapping:
    ikey:
      type: string
      label: 'Increment key'
    min_value:
      type: integer
      label: 'Minimum value'

parameter.integer:
  type: parameter_plugin
  label: 'Integer'
  mapping:
    value:
      type: integer
      label: 'Integer value'

parameter.reference:
  type: parameter_plugin
  label: 'Referenced parameter'
  mapping:
    target_id:
      type: string
      label: 'ID of target parameters collection'
    target_name:
      type: string
      label: 'Name of target parameter'

parameter.yaml:
  type: parameter_plugin
  label: 'Nested YAML'
  mapping:
    values:
      type: ignore
      label: 'Decoded values'

parameter.null:
  type: parameter_plugin
  label: 'Null object'

parameter.content:*:
  type: parameter_plugin
  label: 'Content'
  mapping:
    values:
      type: ignore
      label: 'Content values'

parameter.content:*.*:
  type: parameter_plugin
  label: 'Content'
  mapping:
    values:
      type: ignore
      label: 'Content values'

parameter.boolean:
  type: parameter_plugin
  label: 'Boolean'
  mapping:
    value:
      type: boolean
      label: 'Boolean value'

parameter.http:
  type: parameter_plugin
  label: 'Http endpoint'
  mapping:
    url:
      type: string
      label: 'Endpoint URL'
    cache_ttl_min:
      type: integer
      label: 'Integer value'
    cache_ttl_max:
      type: integer
      label: 'Integer value'
    cache_volatility:
      type: integer
      label: 'Integer value'

parameter.bundles:*:
  type: parameter_plugin
  label: 'Selection of entity bundles'
  mapping:
    selected:
      type: sequence
      label: 'Selected entity bundles'
      sequence:
        type: string
        label: 'Bundle machine name'

parameter.fields:*:
  type: parameter_plugin
  label: 'Selection of entity fields'
  mapping:
    bundle:
      type: string
      label: 'Selected entity bundle'
    selected:
      type: sequence
      label: 'Selected entity fields'
      sequence:
        type: string
        label: 'Field machine name'

parameter.roles:*:
  type: parameter_plugin
  label: 'Selection of user roles'
  mapping:
    selected:
      type: sequence
      label: 'Selected user role'
      sequence:
        type: string
        label: 'Role ID'

parameter.icon:
  type: parameter_plugin
  label: 'Icon (SVG)'
  mapping:
    value:
      type: string
      label: 'Raw stored icon'

parameter.secret:
  type: parameter_plugin
  label: 'Secret'
  mapping:
    value:
      type: string
      label: 'Encrypted value'
    encoding:
      type: ignore
      label: 'Settings needed for decryption'

parameter.float:
  type: parameter_plugin
  label: 'Float'
  mapping:
    value:
      type: string
      label: 'Float value'

parameter.options:
  type: parameter_plugin
  label: 'Options'
  mapping:
    options:
      type: sequence
      label: 'Defined options'
      sequence:
        type: label
        label: 'Option label'
    default:
      type: string
      label: 'Default option'
