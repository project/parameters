<?php

namespace Drupal\Tests\parameters\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\parameters\Entity\ParametersCollection;
use Drupal\parameters\Plugin\Parameter\Secret;
use Drupal\parameters\Plugin\ParameterManager;

/**
 * Tests for secret parameters.
 *
 * @group parameters
 */
class SecretParametersTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'filter',
    'text',
    'node',
    'serialization',
    'parameters',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installConfig(static::$modules);

    ParametersCollection::load('global')->setParameters([
      'my_secret' => [
        'name' => 'my_secret',
        'label' => 'My secret',
        'description' => '',
        'type' => 'secret',
        'value' => 'Hello world!',
      ],
    ])->save();
  }

  /**
   * Tests the usage of secret parameters.
   */
  public function testSecretParameters(): void {
    $token = \Drupal::token();

    $collection = ParametersCollection::load('global');
    $parameter = $collection->getParameter('my_secret');

    $this->assertInstanceOf(Secret::class, $parameter);
    assert($parameter instanceof Secret);

    $this->assertNotEquals('Hello world!', $parameter->getConfiguration()['value']);
    $this->assertNotEquals(base64_decode('Hello world!'), $parameter->getConfiguration()['value']);
    $this->assertStringNotContainsString('Hello world!', $parameter->getConfiguration()['value']);
    $this->assertEquals('Hello world!', $parameter->reveal());
    $this->assertEquals('Hello world!', $parameter->getProcessedData()->getValue());
    $this->assertEquals('Hello world!', $token->replace('[parameter:global:my_secret]'));

    $parameter = ParameterManager::get()->createInstance('secret', [
      'name' => 'another',
      'label' => 'Another',
      'type' => 'secret',
    ]);
    assert($parameter instanceof Secret);
    $to_conceal = $this->randomString();

    $parameter->conceal($to_conceal);
    $this->assertNotEquals($to_conceal, $parameter->getConfiguration()['value']);
    $this->assertNotEquals(base64_decode($to_conceal), $parameter->getConfiguration()['value']);
    $this->assertStringNotContainsString($to_conceal, $parameter->getConfiguration()['value']);
    $this->assertEquals($to_conceal, $parameter->getProcessedData()->getValue());
    $this->assertEquals($to_conceal, $parameter->reveal());

    $collection->setParameters([
      'another' => $parameter->getConfiguration(),
    ] + $collection->get('parameters'));
    $collection->save();

    $this->assertEquals($to_conceal, $token->replace('[parameter:global:another]'));
  }

}
