<?php

namespace Drupal\Tests\parameters\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\parameters\Entity\ParametersCollection;
use Drupal\parameters\Entity\ParametersCollectionStorage;

/**
 * Tests for Parameters token replacement.
 *
 * @group parameters
 */
class ParametersTokenTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'filter',
    'text',
    'node',
    'serialization',
    'parameters',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installConfig(static::$modules);

    NodeType::create(['type' => 'page', 'name' => 'Page'])->save();
    NodeType::create(['type' => 'article', 'name' => 'Article'])->save();

    ParametersCollection::load('global')->setParameters([
      'maximum_capacity' => [
        'name' => 'maximum_capacity',
        'label' => 'Maximum capacity',
        'description' => '',
        'type' => 'integer',
        'value' => 101,
      ],
      'nested_stuff' => [
        'name' => 'nested_stuff',
        'label' => 'Nested stuff',
        'description' => 'some nested parameters.',
        'type' => 'yaml',
        'values' => [
          'onetwothree' => 123,
          'zero' => 0,
        ],
      ],
      'state_machine' => [
        'name' => 'state_machine',
        'label' => 'State machine',
        'description' => 'A state machine.',
        'type' => 'machine_name',
        'value' => 'hello_there',
      ],
      'some_string' => [
        'name' => 'some_string',
        'label' => 'Some string',
        'description' => 'Some raw string.',
        'type' => 'string',
        'value' => '<h1>Hello</h1>',
      ],
      'some_text' => [
        'name' => 'some_text',
        'label' => 'Some text',
        'description' => 'Some text.',
        'type' => 'text',
        'text' => [
          'value' => '<h1>Hello</h1>',
          'format' => 'plain_text',
        ],
      ],
    ])->save();
    ParametersCollection::create([
      'id' => 'node.page',
      'label' => 'node.page',
      'status' => TRUE,
      'locked' => FALSE,
      'langcode' => 'en',
    ])->setParameters([
      'maximum_capacity' => [
        'name' => 'maximum_capacity',
        'label' => 'Maximum capacity',
        'description' => '',
        'type' => 'integer',
        'value' => 49,
      ],
    ])->save();
    ParametersCollection::create([
      'id' => 'node.article',
      'label' => 'node.article',
      'status' => TRUE,
      'locked' => FALSE,
      'langcode' => 'en',
    ])->setParameters([
      'maximum_capacity' => [
        'name' => 'maximum_capacity',
        'label' => 'Maximum capacity',
        'description' => '',
        'type' => 'integer',
        'value' => -2,
      ],
      'page' => [
        'name' => 'page',
        'label' => 'Page parameters',
        'description' => 'Referenced page parameters.',
        'type' => 'reference',
        'target_id' => 'node.page',
        'target_name' => NULL,
      ],
      'page_max_cap' => [
        'name' => 'page_max_cap',
        'label' => 'Page max. cap.',
        'description' => '',
        'type' => 'reference',
        'target_id' => 'node.page',
        'target_name' => 'maximum_capacity',
      ],
    ])->save();
  }

  /**
   * Tests various token replacements using parameters.
   */
  public function testTokenReplacements(): void {
    $article = Node::create([
      'type' => 'article',
      'tnid' => 0,
      'uid' => 0,
      'status' => 1,
      'title' => $this->randomMachineName(),
    ]);
    $article->save();

    $token = \Drupal::token();

    $this->assertEquals('101', $token->replace('[parameter:global:maximum_capacity]'));
    $this->assertEquals('101', $token->replace('[parameter:maximum_capacity]'));
    $this->assertEquals('[parameter:global:maximum_capacity_1]', $token->replace('[parameter:global:maximum_capacity_1]'));

    $this->assertEquals('123', $token->replace('[parameter:nested_stuff.onetwothree]'));
    $this->assertEquals('123', $token->replace('[parameter:nested_stuff:onetwothree]'));
    $this->assertEquals('0', $token->replace('[parameter:nested_stuff:zero]'));
    $this->assertEquals('[parameter:nested_stuff:nonexistent]', $token->replace('[parameter:nested_stuff:nonexistent]'));

    $this->assertEquals('hello_there', $token->replace('[parameter:state_machine]'));
    $this->assertEquals('<h1>Hello</h1>', $token->replace('[parameter:some_string]'));
    $this->assertEquals('<p>&lt;h1&gt;Hello&lt;/h1&gt;</p>', $token->replace('[parameter:some_text]'));

    $this->assertEquals('101', $token->replace('[parameter:global:maximum_capacity]', ['node' => $article]));
    $this->assertEquals('-2', $token->replace('[node:parameter:maximum_capacity]', ['node' => $article]));
    $this->assertEquals('-2', $token->replace('[parameter:maximum_capacity]', ['node' => $article]));
    $this->assertEquals('49', $token->replace('[parameter:page.maximum_capacity]', ['node' => $article]));
    $this->assertEquals('49', $token->replace('[parameter:page:maximum_capacity]', ['node' => $article]));
    $this->assertEquals('49', $token->replace('[parameter:page_max_cap]', ['node' => $article]));

    $page = Node::create([
      'type' => 'page',
      'tnid' => 0,
      'uid' => 0,
      'status' => 1,
      'title' => $this->randomMachineName(),
    ]);
    $page->save();

    $this->assertEquals('101', $token->replace('[parameter:global:maximum_capacity]'));
    $this->assertEquals('101', $token->replace('[parameter:maximum_capacity]'));
    $this->assertEquals('[parameter:global:maximum_capacity_1]', $token->replace('[parameter:global:maximum_capacity_1]'));

    $this->assertEquals('101', $token->replace('[parameter:global:maximum_capacity]', ['node' => $page]));
    $this->assertEquals('49', $token->replace('[node:parameter:maximum_capacity]', ['node' => $page]));
    $this->assertEquals('49', $token->replace('[parameter:maximum_capacity]', ['node' => $page]));

    $this->assertEquals('101', $token->replace('[parameter:maximum_capacity]', ['page' => $page, 'article' => $article]));
    $this->assertEquals('101', $token->replace('[parameter:maximum_capacity]', ['article' => $article, 'page' => $page]));

    $this->assertEquals('-2', $token->replace('[parameter:node.article:maximum_capacity]', ['node' => $page]));
    $this->assertEquals('49', $token->replace('[parameter:node.page:maximum_capacity]', ['node' => $page]));
  }

  /**
   * Tests parameters that got added on runtime.
   */
  public function testRuntimeParamters(): void {
    $token = \Drupal::token();
    $this->assertEquals('101', $token->replace('[parameter:global:maximum_capacity]'));

    $parameters = ParametersCollection::load('global');
    $parameters_array = $parameters->get('parameters');
    $parameters_array['runtime_one'] = [
      'name' => 'runtime_one',
      'label' => 'Runtime one',
      'description' => '',
      'type' => 'integer',
      'value' => 443,
    ];
    $parameters->set('parameters', $parameters_array);

    $this->assertEquals('[parameter:global:runtime_one]', $token->replace('[parameter:global:runtime_one]'), "Not added to the list of statically cached collections will not lead to changes on runtime.");
    ParametersCollectionStorage::$cachedCollections['global'] = $parameters;
    $this->assertEquals('443', $token->replace('[parameter:global:runtime_one]'));
    $this->assertEquals('101', $token->replace('[parameter:global:maximum_capacity]'));

    // Simulate loading the global collection with unchanged values.
    $parameters = ParametersCollectionStorage::get()->loadUnchanged('global');
    $this->assertEquals('443', $token->replace('[parameter:global:runtime_one]'));
    $this->assertEquals('101', $token->replace('[parameter:global:maximum_capacity]'));

    $parameters_array = $parameters->get('parameters');
    $this->assertFalse(isset($parameters_array['runtime_one']));

    $parameters_array['runtime_two'] = [
      'name' => 'runtime_one',
      'label' => 'Runtime one',
      'description' => '',
      'type' => 'integer',
      'value' => 774,
    ];
    $parameters->set('parameters', $parameters_array);
    $parameters->save();
    $this->assertEquals('[parameter:global:runtime_one]', $token->replace('[parameter:global:runtime_one]'));
    $this->assertEquals('774', $token->replace('[parameter:global:runtime_two]'));
    $this->assertEquals('101', $token->replace('[parameter:global:maximum_capacity]'));
  }

}
