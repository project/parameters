<?php

namespace Drupal\parameters;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\parameters\Internals\ContentSerializationCompilerPass;

/**
 * Provider for dynamically provided services by Parameters.
 */
class ParametersServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // Add a compiler pass for adding Normalizers and Encoders to the Parameters
    // content serializer.
    $container->addCompilerPass(new ContentSerializationCompilerPass());
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    [$version] = explode('.', \Drupal::VERSION, 2);
    if (((int) $version) < 10) {
      // Use normalizers that are compatible with Symfony 4.
      $definition = $container->getDefinition('serializer.normalizer.parameters__content_entity');
      $definition->setClass('Drupal\parameters\Normalizer\Legacy\ParametersContentEntityNormalizer');
      $definition = $container->getDefinition('serializer.normalizer.parameters__entity_reference_field_item');
      $definition->setClass('Drupal\parameters\Normalizer\Legacy\ParametersEntityReferenceFieldItemNormalizer');
    }
  }

}
