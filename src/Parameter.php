<?php

namespace Drupal\parameters;

use Drupal\parameters\Exception\ParameterNotFoundException;
use Drupal\parameters\Plugin\Parameter\NullObject;
use Drupal\parameters\Plugin\ParameterInterface;

/**
 * Helper class for convenient loading of parameters.
 */
abstract class Parameter {

  /**
   * Gracefully get a parameter.
   *
   * When a requested parameter does not exist, this method returns a Null
   * object. If you want to let this method not return a Null object and
   * instead let it throw an exception, set 'strict' as second argument.
   * If the requested parameter was not found and strict mode is enabled, then
   * an exception of \Drupal\parameters\Exception\ParameterNotFoundException
   * will be thrown.
   *
   * @param string $name
   *   The machine name of the parameter.
   * @param mixed &...$context
   *   Additional arguments used as according context. This is mostly an entity.
   *
   * @return \Drupal\parameters\Plugin\ParameterInterface
   *   The parameter plugin instance.
   */
  public static function get(string $name, &...$context): ParameterInterface {
    try {
      return ParameterRepository::get()->getParameter($name, ...$context);
    }
    catch (ParameterNotFoundException $e) {
      if (in_array('strict', $context, TRUE)) {
        throw $e;
      }
      return new NullObject($name);
    }
  }

}
