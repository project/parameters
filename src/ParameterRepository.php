<?php

namespace Drupal\parameters;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Event\CollectionsPreparationEvent;
use Drupal\parameters\Event\ParameterEvents;
use Drupal\parameters\Exception\ParameterNotFoundException;
use Drupal\parameters\Plugin\ParameterPluginCollection;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * The repository for getting parameters.
 */
class ParameterRepository {

  /**
   * The service name of this class.
   *
   * @var string
   */
  const SERVICE_NAME = 'parameter.repository';

  /**
   * The manager of parameter plugins.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $pluginManager;

  /**
   * The storage of collections of parameters.
   *
   * @var \Drupal\parameters\Entity\ParametersCollectionStorage
   */
  protected EntityStorageInterface $collectionStorage;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Whether auto-locking is enabled.
   *
   * @var bool
   */
  protected bool $autolock;

  /**
   * Get the service instance of this class.
   *
   * @return \Drupal\parameters\ParameterRepository
   *   The service instance.
   */
  public static function get(): ParameterRepository {
    return \Drupal::service(self::SERVICE_NAME);
  }

  /**
   * Constructs a new ParameterRepository object.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $plugin_manager
   *   The manager of parameter plugins.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param bool $autolock
   *   Whether auto-locking is enabled.
   */
  public function __construct(PluginManagerInterface $plugin_manager, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, bool $autolock) {
    $this->pluginManager = $plugin_manager;
    $this->collectionStorage = $entity_type_manager->getStorage(ParametersCollectionInterface::ENTITY_TYPE_ID);
    $this->eventDispatcher = $event_dispatcher;
    $this->autolock = $autolock;
  }

  /**
   * Get a parameter.
   *
   * @param string $name
   *   The machine name of the parameter.
   * @param mixed &...$context
   *   Additional arguments used as according context. This is mostly an entity.
   *
   * @return \Drupal\parameters\Plugin\ParameterInterface
   *   The parameter plugin instance.
   *
   * @throws \Drupal\parameters\Exception\ParameterNotFoundException
   *   When no parameter was found for the requested machine name.
   */
  public function getParameter(string $name, &...$context): ParameterInterface {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface[] $collections */
    $collections = [];
    $this->prepareCollections($collections, $name, ...$context);

    reset($collections);
    foreach ($collections as $collection) {
      if (!$collection->getStatus()) {
        continue;
      }

      $config = NULL;
      $slug = '';
      $name_parts = $this->getNameParts($name);
      $popped_parts = [];
      while ($name_parts) {
        $slug = implode('.', $name_parts);
        if ($config = $collection->get("parameters.$slug")) {
          if ($this->autolock && !$collection->isLocked()) {
            $collection->lockAndSave();
          }
          break 2;
        }
        array_unshift($popped_parts, array_pop($name_parts));
      }
    }

    if (isset($config)) {
      $plugin = $this->pluginManager->createInstance($config['type'], $config);

      if (empty($popped_parts)) {
        return $plugin;
      }

      if (($plugin instanceof PropertyParameterInterface) && ($property = $plugin->getProperty(implode('.', $popped_parts)))) {
        return $property;
      }
    }

    throw new ParameterNotFoundException($name);
  }

  /**
   * Get all parameters.
   *
   * @param mixed &$context
   *   Additional arguments used as according context. This is mostly an entity.
   *
   * @return \Drupal\parameters\Plugin\ParameterPluginCollection
   *   The parameter collection.
   */
  public function getParameters(&...$context): ParameterPluginCollection {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface[] $collections */
    $collections = [];
    $nothing = NULL;
    $this->prepareCollections($collections, $nothing, ...$context);
    $configs = [];
    foreach ($collections as $collection) {
      $configs = array_merge($collection->get('parameters'), $configs);
    }
    return new ParameterPluginCollection($this->pluginManager, $configs);
  }

  /**
   * Get the contained parts of a parameter name.
   *
   * return string[]
   *   The contained parts.
   */
  protected function getNameParts(string $name): array {
    // Remove brackets accidentally coming from token syntax.
    if (mb_substr($name, 0, 1) === '[') {
      $name = mb_substr($name, 1);
    }
    if (mb_substr($name, -1) === ']') {
      $name = mb_substr($name, 0, -1);
    }

    // Replace colon with a simple dot.
    $name = str_replace(':', '.', $name);

    return explode('.', $name);
  }

  /**
   * Prepares the collections before being asked for a parameter.
   *
   * Collections will be asked in the ascending order of this collection list.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface[] &$collections
   *   The current list of collections of parameters.
   * @param string|null &$name
   *   When requesting a single parameter, the machine name of the parameter.
   *   Otherwise this argument is NULL when requesting whole collections.
   * @param mixed &$context
   *   Additional arguments used as according context. This is mostly an entity.
   */
  protected function prepareCollections(array &$collections, ?string &$name = NULL, &...$context): void {
    $entity = NULL;

    // Add collections provided as context first.
    foreach ($context as $arg) {
      if ($arg instanceof ParametersCollectionInterface) {
        if (!in_array($arg, $collections, TRUE)) {
          $collections[] = $arg;
        }
      }
      elseif (!$entity && ($arg instanceof EntityInterface)) {
        $entity = $arg;
      }
    }

    // Add the collection specified as part of the named argument.
    if (isset($name) && strpos($name, ':')) {
      $parts = explode(':', $name, 2);
      if ($collection = $this->collectionStorage->load(reset($parts))) {
        array_shift($parts);
        $name = reset($parts);
        if (!in_array($collection, $collections, TRUE)) {
          $collections[] = $collection;
        }
      }
    }

    if ($entity) {
      // If available, add the collection according to the entity.
      $collection_id = $entity->getEntityTypeId() . '.' . $entity->bundle();
      if ($collection = $this->collectionStorage->load($collection_id)) {
        if (!in_array($collection, $collections, TRUE)) {
          $collections[] = $collection;
        }
      }
    }
    elseif ($collections && $context && ($collections === $context)) {
      // In case the provided context is all about collections, abort collecting
      // further collections.
      return;
    }

    // Append the global collection, if not yet done otherwise.
    if ($global_collection = $this->collectionStorage->load('global')) {
      if (!in_array($global_collection, $collections, TRUE)) {
        $collections[] = $global_collection;
      }
    }

    // Allow external components to optionally enrich the collection list.
    $this->eventDispatcher->dispatch(new CollectionsPreparationEvent($collections, $name, $context), ParameterEvents::COLLECTIONS_PREPARATION);
  }

}
