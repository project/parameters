<?php

namespace Drupal\parameters\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Attribute\FormElement;
use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\Render\Markup;

/**
 * Form element for a secret parameter.
 */
#[FormElement('parameter_secret')]
class ParameterSecret extends Textfield {

  /**
   * {@inheritdoc}
   */
  public static function preRenderTextfield($element) {
    $element = parent::preRenderTextfield($element);

    $id = Html::getUniqueId('secret-parameter-value');

    $element['#type'] = 'textfield';
    $element['#attributes']['id'] = $id;
    // By default the value should not be visible.
    $element['#attributes']['type'] = 'password';

    $shown = t('Shown');
    $hidden = t('Hidden');

    $js = <<<JS
      (function () {
        window.parameterSecretHideShow = window.parameterSecretHideShow || function (el, id) {
          if (el.classList.contains("action-link--icon-hide")) {
            el.classList.remove("action-link--icon-hide");
            el.classList.add("action-link--icon-show");
            el.innerHTML = "$shown";
            document.getElementById(id).type = 'text';
          }
          else if (el.classList.contains("action-link--icon-show")) {
            el.classList.remove("action-link--icon-show");
            el.classList.add("action-link--icon-hide");
            el.innerHTML = "$hidden";
            document.getElementById(id).type = 'password';
          }
        }
      })();
    JS;

    $element['#field_suffix'] = Markup::create("<a class=\"action-link action-link--icon-hide\" onclick=\"parameterSecretHideShow(this, '$id');\">{$hidden}</a><script>$js</script>");

    return $element;
  }

}
