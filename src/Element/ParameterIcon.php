<?php

namespace Drupal\parameters\Element;

use Drupal\Core\Render\Attribute\RenderElement;
use Drupal\Core\Render\Element\HtmlTag;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Entity\ParametersCollectionInterface;

/**
 * Render element for Icon (SVG) parameters.
 */
#[RenderElement("parameter_icon")]
class ParameterIcon extends HtmlTag {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#parameter' => NULL,
      '#tag' => 'svg',
      '#value' => '',
      '#title' => NULL,
      '#cache' => [
        'tags' => \Drupal::entityTypeManager()->getDefinition(ParametersCollectionInterface::ENTITY_TYPE_ID)->getListCacheTags(),
      ],
    ] + parent::getInfo();
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderHtmlTag($element) {
    /** @var \Drupal\parameters\Plugin\Parameter\Icon $parameter */
    $parameter = $element['#parameter'];
    $element['#attributes'] += $parameter->getIconAttributes();
    $title = $element['#title'] ?? ($element['#attributes']['aria-label'] ?? new TranslatableMarkup($parameter->getLabel()));
    if ($title !== '') {
      $title = (string) $title;
      if (!isset($element['#attributes']['aria-label'])) {
        $element['#attributes']['aria-label'] = $title;
      }
      $title = '<title>' . $title . '</title>';
    }
    $element['#value'] = Markup::create($title . $element['#value'] . $parameter->getIconContents());
    return parent::preRenderHtmlTag($element);
  }

}
