<?php

namespace Drupal\parameters\Event;

/**
 * All events provided by the Parameters module.
 */
final class ParameterEvents {

  /**
   * Dispatches when collections are being prepared for requesting parameters.
   *
   * @Event
   *
   * @var string
   */
  const COLLECTIONS_PREPARATION = 'parameters.collections_preparation';

}
