<?php

namespace Drupal\parameters\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Dispatched when collections are being prepared for requesting parameters.
 */
class CollectionsPreparationEvent extends Event {

  /**
   * The current list of collections as reference.
   *
   * Collections will be asked in the ascending order of this collection list.
   *
   * @var \Drupal\parameters\Entity\ParametersCollectionInterface[]
   */
  public array $collections;

  /**
   * When requesting a single parameter, the machine name of the parameter.
   *
   * Otherwise this argument is NULL when requesting whole collections.
   *
   * @var string|null
   */
  public ?string $name;

  /**
   * Additional arguments used as according context. This is mostly an entity.
   *
   * @var array
   */
  public array $context;

  /**
   * Constructs a new CollectionsPreparationEvent object.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface[] &$collections
   *   The current list of collections.
   * @param string|null $name
   *   The machine name of the parameter, if available.
   * @param array $context
   *   Additoonal arguments used as context.
   */
  public function __construct(array &$collections, ?string $name, array $context) {
    $this->collections = &$collections;
    $this->name = $name;
    $this->context = $context;
  }

}
