<?php

namespace Drupal\parameters\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\parameters\Exception\ParameterNotFoundException;
use Drupal\parameters\ParameterRepository;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\ParameterPluginCollection;
use Drupal\parameters\Plugin\UsageParameterInterface;

/**
 * Config entity holding a collection of configured parameters.
 *
 * @ConfigEntityType(
 *   id = "parameters_collection",
 *   label = @Translation("Parameters collection"),
 *   entity_keys = {
 *     "id" = "id",
 *     "status" = "status",
 *     "uuid" = "uuid",
 *     "label" = "label"
 *   },
 *   handlers = {
 *     "storage" = "Drupal\parameters\Entity\ParametersCollectionStorage"
 *   },
 *   config_prefix = "collection",
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "locked",
 *     "deletable",
 *     "parameters"
 *   }
 * )
 */
class ParametersCollection extends ConfigEntityBase implements ParametersCollectionInterface {

  /**
   * The collection ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The collection label.
   *
   * @var string
   */
  protected string $label;

  /**
   * A list of parameter configurations, keyed by parameter name.
   *
   * @var array
   */
  protected array $parameters = [];

  /**
   * Whether this collection is locked.
   *
   * @var bool
   */
  protected bool $locked = FALSE;

  /**
   * Whether this collection can be deleted.
   *
   * @var bool
   */
  protected bool $deletable = FALSE;

  /**
   * Initialized flag whether autolock is enabled.
   *
   * @var bool|null
   */
  public static ?bool $autolock = NULL;

  /**
   * An array holding current lock saving operations.
   *
   * Keyed by collection ID, each value is always 1.
   * This array is used to detect and prevent a possible lock recursion.
   *
   * @var array
   */
  public static array $lockSaves = [];

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): bool {
    return (bool) $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): ParametersCollectionInterface {
    $this->status = (bool) $status;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'parameter.plugins' => $this->getParameters(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setParameters(array $parameters): ParametersCollectionInterface {
    uasort($parameters, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
    $this->parameters = [];
    foreach ($parameters as $parameter) {
      $this->parameters[$parameter['name']] = $parameter;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter(string $name): ?ParameterInterface {
    try {
      return ParameterRepository::get()->getParameter($name, $this);
    }
    catch (ParameterNotFoundException $e) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters(): ParameterPluginCollection {
    return ParameterRepository::get()->getParameters($this);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    if (!isset($values['label']) && isset($values['id'])) {
      $label = $values['id'];
      if (strpos($label, '.')) {
        $parts = explode('.', $label);
        $etm = \Drupal::entityTypeManager();
        if ($entity_type = $etm->getDefinition($parts[0], FALSE)) {
          $label = $entity_type->getLabel();
          if (isset($parts[1]) && ($bundle_type_id = $entity_type->getBundleEntityType())) {
            if ($etm->hasDefinition($bundle_type_id) && ($bundle_config = $etm->getStorage($bundle_type_id)->load($parts[1]))) {
              $label = $bundle_config->label() . ' ' . $label;
            }
          }
        }
      }
      $values['label'] = $label;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    unset(ParametersCollectionStorage::$saveStack[$this->id()]);
    parent::postSave($storage, $update);
    if (function_exists('token_clear_cache')) {
      token_clear_cache();
    }
    else {
      \Drupal::token()->resetInfo();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {
    if (mb_substr($property_name, 0, 11) === 'parameters.') {
      $parts = explode('.', $property_name, 2);
      return $this->parameters[end($parts)] ?? NULL;
    }
    return parent::get($property_name);
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked(): bool {
    return $this->locked;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocked(bool $locked = TRUE): ParametersCollectionInterface {
    $this->locked = $locked;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function lockAndSave(bool $locked = TRUE, bool $trust_data = FALSE): ParametersCollectionInterface {
    if (!$this->isNew()) {
      if (isset(static::$lockSaves[$this->id])) {
        return $this;
      }
      static::$lockSaves[$this->id] = 1;
    }
    $collection = $this->setLocked($locked);
    if ($trust_data) {
      $collection = $collection->trustData();
    }
    $collection->save();
    unset(static::$lockSaves[$this->id]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isDeletable(): bool {
    return $this->deletable && !$this->isLocked();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyName() {
    if (!isset(static::$autolock)) {
      static::$autolock = \Drupal::getContainer()->getParameter('parameters_collection.autolock');
    }
    if (static::$autolock && !$this->isLocked()) {
      $this->lockAndSave(TRUE, TRUE);
      $this->trustedData = FALSE;
    }
    return parent::getConfigDependencyName();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    $autolock = static::$autolock;
    static::$autolock = FALSE;
    $tags = parent::getCacheTagsToInvalidate();
    static::$autolock = $autolock;
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    $autolock = static::$autolock;
    static::$autolock = FALSE;
    parent::preDelete($storage, $entities);
    static::$autolock = $autolock;
    $op = 'delete';
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    foreach ($entities as $collection) {
      foreach ($collection->getParameters() as $parameter) {
        if ($parameter instanceof UsageParameterInterface) {
          $parameter->onRemoval($collection, $op);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    ParametersCollectionStorage::$saveStack[$this->id()] = $this->id();
    $op = $this->isNew() ? 'insert' : 'update';

    /** @var \Drupal\parameters\Plugin\ParameterPluginCollection $lazy_original_parameters */
    $lazy_original_parameters = isset($this->original) ? $this->original->getParameters() : new ParameterPluginCollection(ParameterManager::get(), []);
    $lazy_current_parameters = $this->getParameters();

    $original_parameter_names = array_keys((isset($this->original) ? ($this->original->get('parameters') ?? []) : []));
    $current_parameter_names = array_keys(($this->get('parameters') ?? []));

    foreach ($current_parameter_names as $name) {
      /** @var \Drupal\parameters\Plugin\ParameterInterface $parameter */
      $parameter = $lazy_current_parameters->get($name);
      if ($parameter instanceof UsageParameterInterface) {
        if (!in_array($name, $original_parameter_names, TRUE) || $this->isNew()) {
          $parameter->onAddition($this, $op);
        }
        elseif ($parameter->getConfiguration() !== $lazy_original_parameters->get($name)->getConfiguration()) {
          $parameter->onChange($lazy_original_parameters->get($name)->getConfiguration(), $this, $op);
        }
      }
    }

    /** @var \Drupal\parameters\Plugin\ParameterInterface $parameter */
    foreach ($original_parameter_names as $name) {
      $parameter = $lazy_original_parameters->get($name);
      if ($parameter instanceof UsageParameterInterface) {
        if (!in_array($name, $current_parameter_names, TRUE)) {
          $parameter->onRemoval($this, $op);
        }
      }
    }

    parent::preSave($storage);
  }

}
