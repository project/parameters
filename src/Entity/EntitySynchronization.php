<?php

namespace Drupal\parameters\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\parameters\Plugin\EntityParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;

/**
 * Synchronizes entities that are managed via parameters.
 */
class EntitySynchronization {

  /**
   * The state system.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Whether synchronization is enabled or not.
   *
   * @var bool
   */
  protected static bool $enabled = TRUE;

  /**
   * Get the service instance of this class.
   *
   * @return \Drupal\parameters\Entity\EntitySynchronization
   *   The service instance.
   */
  public static function get(): EntitySynchronization {
    return \Drupal::service('parameters.entity_synchronization');
  }

  /**
   * Constructs a new ContentSynchronization object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state system.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(StateInterface $state, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * Synchronizes the entity.
   *
   * @throws \InvalidArgumentException
   *   When the provided entity is new.
   */
  public function synchronize(EntityInterface $entity): void {
    if (!self::$enabled) {
      return;
    }
    $this->setEnabled(FALSE);

    if ($entity->isNew()) {
      throw new \InvalidArgumentException("The entity must not be new.");
    }

    if (!($collection_id = $this->state->get($this->getStateKey($entity)))) {
      return;
    }

    $collection_storage = $this->entityTypeManager->getStorage(ParametersCollectionInterface::ENTITY_TYPE_ID);

    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    if (!($collection = $collection_storage->load($collection_id))) {
      return;
    }

    $entity_parameter = NULL;
    $parameter_entity = NULL;
    foreach ($collection->getParameters() as $parameter) {
      if ($parameter instanceof EntityParameterInterface) {
        $parameter_entity = $parameter->getEntity();
        if ($parameter_entity && $parameter_entity->uuid() && ($parameter_entity->uuid() === $entity->uuid())) {
          $entity_parameter = $parameter;
          break;
        }
      }
    }

    /** @var \Drupal\parameters\Plugin\EntityParameterInterface $entity_parameter */
    if ($entity_parameter) {
      if (interface_exists('Drupal\language\ConfigurableLanguageManagerInterface') && ($this->languageManager instanceof ConfigurableLanguageManagerInterface) && $collection->language()->getId() !== $entity->language()->getId() && ($entity instanceof TranslatableInterface)) {
        $collection = $this->languageManager->getLanguageConfigOverride($entity->language()->getId(), $collection->getConfigDependencyName());
        $parameter_configs = $collection->get('parameters') ?? [];
        if (isset($parameter_configs[$entity_parameter->getName()])) {
          /** @var \Drupal\parameters\Plugin\Parameter\Content $entity_parameter */
          $entity_parameter = ParameterManager::get()->createInstance($entity_parameter->getPluginId(), $parameter_configs[$entity_parameter->getName()]);
        }
      }
      else {
        $parameter_configs = $collection->get('parameters');
      }

      $entity_parameter->setEntity($entity);
      if (!isset($parameter_configs[$entity_parameter->getName()]) || $parameter_configs[$entity_parameter->getName()] !== $entity_parameter->getConfiguration()) {
        $parameter_configs[$entity_parameter->getName()] = $entity_parameter->getConfiguration();
        $collection->set('parameters', $parameter_configs);
        $collection->save();
      }
    }

    $this->setEnabled(TRUE);
  }

  /**
   * Registers an entity for synchronization.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to register.
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface $collection
   *   The collection that holds the configuration of the given entity.
   */
  public function register(EntityInterface $entity, ParametersCollectionInterface $collection): void {
    $this->state->set($this->getStateKey($entity), $collection->id());
  }

  /**
   * Un-registers an entity from synchronization.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to register.
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface $collection
   *   The collection that holds (or held) the config of the given entity.
   */
  public function unregister(EntityInterface $entity, ParametersCollectionInterface $collection): void {
    $this->state->delete($this->getStateKey($entity));
  }

  /**
   * Generates a state key for registry lookup.
   *
   * @return string
   *   The state key.
   */
  public function getStateKey(EntityInterface $entity): string {
    return "parameters:managed:{$entity->getEntityTypeId()}:{$entity->uuid()}";
  }

  /**
   * Whether synchronization is enabled.
   *
   * @return bool
   *   Returns TRUE if enabled, FALSE otherwise.
   */
  public function isEnabled(): bool {
    return self::$enabled;
  }

  /**
   * Enable or disable the synchronization service.
   *
   * @var bool $enabled
   *   Set to TRUE to enable, or FALSE to disable the service. Default is TRUE.
   *
   * @return $this
   */
  public function setEnabled(bool $enabled = TRUE): EntitySynchronization {
    self::$enabled = $enabled;
    return $this;
  }

}
