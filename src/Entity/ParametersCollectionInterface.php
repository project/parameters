<?php

namespace Drupal\parameters\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterPluginCollection;

/**
 * Interface for config entities holding a collection of configured parameters.
 */
interface ParametersCollectionInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * The entity type ID of a parameters collection.
   *
   * @var string
   */
  public const ENTITY_TYPE_ID = 'parameters_collection';

  /**
   * Get the status value, i.e. whether this configuration is enabled.
   *
   * @return bool
   *   The status.
   */
  public function getStatus(): bool;

  /**
   * Set the default status value.
   *
   * @param bool $status
   *   The default status value.
   *
   * @return $this
   */
  public function setStatus($status): ParametersCollectionInterface;

  /**
   * Whether this collection is locked.
   *
   * @return bool
   *   Returns TRUE if locked, FALSE otherwise.
   */
  public function isLocked(): bool;

  /**
   * Whether this collection can be deleted.
   *
   * @return bool
   *   Returns TRUE if can be deleted.
   */
  public function isDeletable(): bool;

  /**
   * Lock this collection.
   *
   * In case you also need to save this collection afterwards, it is recommended
   * to call ::lockAndSave() instead.
   *
   * @param bool $locked
   *   (optional) Whether to lock this collection. Default is TRUE.
   *
   * @return $this
   */
  public function setLocked(bool $locked = TRUE): ParametersCollectionInterface;

  /**
   * Lock and save this collection.
   *
   * This method takes care of preventing infinite lock recursion. It is
   * recommended to use this method when locking and saving the collection.
   *
   * @param bool $locked
   *   (optional) Whether to lock this collection. Default is TRUE.
   * @param bool $trust_data
   *   (optional) Whether trust_data flag is set when saving. Default is FALSE.
   *   @see \Drupal\Core\Config\Entity\ConfigEntityInterface::trustData()
   *
   * @return $this
   */
  public function lockAndSave(bool $locked = TRUE, bool $trust_data = FALSE): ParametersCollectionInterface;

  /**
   * Get a parameter from this collection.
   *
   * @param string $name
   *   The machine name of the parameter.
   *
   * @return \Drupal\parameters\Plugin\ParameterInterface|null
   *   The parameter plugin instance, or NULL if no parameter exists for the
   *   given machine name.
   */
  public function getParameter(string $name): ?ParameterInterface;

  /**
   * Get all parameters from this collection as plugins.
   *
   * @return \Drupal\parameters\Plugin\ParameterPluginCollection
   *   The parameter plugin collection.
   */
  public function getParameters(): ParameterPluginCollection;

  /**
   * Set the list of parameter plugin configurations.
   *
   * @param array $parameters
   *   The list of parameter configurations. Each item of this list must be
   *   a configuration array that is compatible of the configuration schema.
   *   Available parameter plugin schemas are typed as "type: parameter_plugin".
   *
   * @return $this
   */
  public function setParameters(array $parameters): ParametersCollectionInterface;

}
