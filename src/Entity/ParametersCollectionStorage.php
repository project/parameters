<?php

namespace Drupal\parameters\Entity;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Parameters collection storage.
 */
class ParametersCollectionStorage extends ConfigEntityStorage {

  /**
   * Statically cached collections, keyed by ID.
   *
   * @var \Drupal\parameters\Entity\ParametersCollectionInterface[]
   */
  public static array $cachedCollections = [];

  /**
   * The stack of collections being saved, keys and values are collection IDs.
   *
   * @var array
   */
  public static array $saveStack = [];

  /**
   * Get the handler instance of this class.
   *
   * @return \Drupal\parameters\Entity\ParametersCollectionStorage
   *   The handler instance.
   */
  public static function get(): EntityStorageInterface {
    return \Drupal::entityTypeManager()->getStorage(ParametersCollectionInterface::ENTITY_TYPE_ID);
  }

  /**
   * Get all available collections as select options.
   *
   * return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The options.
   */
  public static function options(): array {
    $options = [];
    foreach (self::get()->loadMultiple() as $collection) {
      $options[$collection->id()] = $collection->label();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities) {
    parent::delete(array_filter($entities, function ($collection) {
      /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
      unset(self::$cachedCollections[$collection->id()]);
      if ($collection->isLocked()) {
        \Drupal::logger('parameters')->error("Skipped attempted deletion of locked parameters collection %id. Please avoid attempts of deleting locked collections, or explicitly unlock the collection before deleting it.", ['%id' => $collection->id()]);
        return FALSE;
      }
      return TRUE;
    }));
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(?array $ids = NULL) {
    $collections = [];
    if ($ids) {
      foreach ($ids as $i => $id) {
        if (isset(self::$cachedCollections[$id])) {
          $collections[$id] = self::$cachedCollections[$id];
          unset($ids[$i]);
        }
      }
    }
    if ($ids || (NULL === $ids)) {
      $collections += parent::loadMultiple($ids);
    }
    return $collections;
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache(?array $ids = NULL) {
    parent::resetCache($ids);
    if (!empty($ids)) {
      foreach ($ids as $id) {
        unset(self::$cachedCollections[$id]);
      }
    }
    else {
      self::$cachedCollections = [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadUnchanged($id) {
    $cached_item = self::$cachedCollections[$id] ?? NULL;
    $unchanged = parent::loadUnchanged($id);
    if ($cached_item) {
      self::$cachedCollections[$id] = $cached_item;
    }
    return $unchanged;
  }

}
