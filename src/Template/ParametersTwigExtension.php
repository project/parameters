<?php

namespace Drupal\parameters\Template;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RenderableInterface;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Parameter;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extensions provided by the Parameters module.
 */
class ParametersTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('p', $this->getRenderableParameter(...)),
    ];
  }

  /**
   * Gets a parameter in a renderable format.
   *
   * @param string $name
   *   The machine name of the parameter.
   * @param mixed &...$context
   *   Additional arguments used as according context. This is mostly an entity.
   *
   * @return array
   *   The parameter as renderable array.
   */
  public function getRenderableParameter($name, &...$context): array {
    assert(is_string($name), 'Name argument must be a string.');

    $parameter = Parameter::get($name, ...$context);

    $build = $parameter instanceof RenderableInterface ? $parameter->toRenderable() : [
      '#markup' => Markup::create($parameter->getProcessedData()->getString()),
    ];

    $metadata = BubbleableMetadata::createFromRenderArray($build);
    $metadata->addCacheTags(\Drupal::entityTypeManager()->getDefinition(ParametersCollectionInterface::ENTITY_TYPE_ID)->getListCacheTags());
    if ($parameter instanceof CacheableDependencyInterface) {
      $metadata->addCacheableDependency($parameter);
    }
    foreach ($context as $item) {
      if ($item instanceof CacheableDependencyInterface) {
        $metadata->addCacheableDependency($item);
      }
    }
    $metadata->applyTo($build);

    return $build;
  }

}
