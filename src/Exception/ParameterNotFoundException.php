<?php

namespace Drupal\parameters\Exception;

/**
 * Thrown when no parameter was found for a requested name.
 */
class ParameterNotFoundException extends \InvalidArgumentException {

  /**
   * The name which was not found.
   *
   * @var string
   */
  protected string $parameterName;

  /**
   * Constructs a new ParameterNotFoundException object.
   *
   * @param string $parameter_name
   *   The name which was not found.
   * @param int $code
   *   (optional) The exception code.
   * @param \Throwable|null $previous
   *   (optional) The previous throwable used for the exception chaining.
   */
  public function __construct(string $parameter_name, int $code = 0, ?\Throwable $previous = null) {
    parent::__construct(sprintf("No parameter found for machine name %s.", $parameter_name), $code, $previous);
    $this->parameterName = $parameter_name;
  }

  /**
   * Get the name which was not found.
   *
   * @return string
   *   The parameter name.
   */
  public function getParameterName(): string {
    return $this->parameterName;
  }

}
