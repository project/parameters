<?php

namespace Drupal\parameters\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for parameter plugins.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class Parameter extends Plugin {

  /**
   * The parameter type as plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the parameter type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A sorting weight.
   *
   * @var int
   */
  public $weight = 0;

}
