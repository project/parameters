<?php

namespace Drupal\parameters\Plugin;

use Drupal\parameters\Entity\ParametersCollectionInterface;

/**
 * Interface for parameter plugins that need to take action on usage.
 */
interface UsageParameterInterface extends ParameterInterface {

  /**
   * Takes action when the parameter is being added.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface $collection
   *   The collection that holds the parameter.
   * @param string $operation
   *   The operation that is performed on the collection. Can be one of
   *   "insert", "update", "delete", "config_override_save" and
   *   "config_override_delete".
   */
  public function onAddition(ParametersCollectionInterface $collection, string $operation): void;

  /**
   * Takes action when the parameter configuration has been changed.
   *
   * @param array $previous_configuration
   *   The previous plugin configuration of the parameter.
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface $collection
   *   The collection that holds the parameter.
   * @param string $operation
   *   The operation that is performed on the collection. Can be one of
   *   "insert", "update", "delete", "config_override_save" and
   *   "config_override_delete".
   */
  public function onChange(array $previous_configuration, ParametersCollectionInterface $collection, string $operation): void;

  /**
   * Takes action when the parameter is being removed.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface $collection
   *   The collection that held the parameter.
   * @param string $operation
   *   The operation that is performed on the collection. Can be one of
   *   "insert", "update", "delete", "config_override_save" and
   *   "config_override_delete".
   */
  public function onRemoval(ParametersCollectionInterface $collection, string $operation): void;

}
