<?php

namespace Drupal\parameters\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for parameter plugins.
 */
abstract class ParameterBase extends PluginBase implements ParameterInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected TypedDataManagerInterface $typedDataManager;

  /**
   * The data type that is returned by ::getProcessedData().
   *
   * @var string
   */
  protected string $dataType = 'string';

  /**
   * The (already) processed data.
   *
   * @var Drupal\Core\TypedData\TypedDataInterface|null
   */
  protected ?TypedDataInterface $processedData = NULL;

  /**
   * Third party settings.
   *
   * An array of key/value pairs keyed by provider.
   *
   * @var array
   */
  protected array $thirdPartySettings = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->setStringTranslation($container->get('string_translation'));
    $instance->setTypedDataManager($container->get('typed_data_manager'));
    $instance->setConfiguration($configuration);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Value'),
      '#default_value' => $this->configuration['value'] ?? '',
      '#required' => TRUE,
      '#weight' => 10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    foreach (array_keys($this->defaultConfiguration()) as $key) {
      $this->configuration[$key] = $form_state->getValue($key);
      if (is_array($this->configuration[$key])) {
        $this->configuration[$key] = array_filter($this->configuration[$key]);
      }
    }
    if (($result = $this->validate()) !== TRUE) {
      $form_state->setError($form, $result);
    }
    $this->setConfiguration($config);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    foreach (array_keys($this->defaultConfiguration()) as $key) {
      $this->configuration[$key] = $form_state->getValue($key);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['value' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
    $this->processedData = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    foreach (array_keys($this->defaultConfiguration()) as $key) {
      if ('' === ($this->configuration[$key] ?? '')) {
        return $this->t('The @key must not be empty.', ['@key' => $key]);
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessedData(): TypedDataInterface {
    if (!isset($this->processedData)) {
      $value = $this->processDataValue();
      $this->processedData = $this->typedDataManager->createInstance($this->dataType, [
        'data_definition' => $this->typedDataManager->createDataDefinition($this->dataType),
        'name' => NULL,
        'parent' => NULL,
      ]);
      $this->processedData->setValue($value);
    }
    return $this->processedData;
  }

  /**
   * Processes the data value to be returned via ::getProcessedData().
   *
   * @return mixed
   *   The data value. This value must be compatible with the typed data type
   *   that is defined via ::$dataType, because it will be set as a value for
   *   an instance of that type of typed data.
   */
  protected function processDataValue() {
    return $this->configuration['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string|MarkupInterface {
    $keys = array_keys($this->defaultConfiguration());
    $key = reset($keys) ?: 'value';
    $preview = (string) ($this->configuration[$key] ?? '');
    if (mb_strlen($preview) > 100) {
      $preview = mb_substr($preview, 0, 100) . '...';
    }
    return (string) Html::escape($preview);
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->configuration['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->configuration['label'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->configuration['description'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->configuration['weight'] ?? 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartySetting($provider, $key, $default = NULL) {
    return $this->thirdPartySettings[$provider][$key] ?? $default;
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartySettings($provider) {
    return $this->thirdPartySettings[$provider] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setThirdPartySetting($provider, $key, $value) {
    $this->thirdPartySettings[$provider][$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetThirdPartySetting($provider, $key) {
    unset($this->thirdPartySettings[$provider][$key]);
    // If the third party is no longer storing any information, completely
    // remove the array holding the settings for this provider.
    if (empty($this->thirdPartySettings[$provider])) {
      unset($this->thirdPartySettings[$provider]);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartyProviders() {
    return array_keys($this->thirdPartySettings);
  }

  /**
   * Set the typed data manager.
   *
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   */
  public function setTypedDataManager(TypedDataManagerInterface $typed_data_manager) {
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsageHelp(EntityInterface $entity): array {
    $name = $entity->id() . ':' . $this->configuration['name'];
    $token = '[parameter:' . $name . ']';

    $usages = [
      'name' => [
        'label' => ['#markup' => Markup::create('<div><label for="name[' . $this->configuration['name'] . ']">' . $this->t('Name') . '</label>:</div>')],
        'usage' => ['#markup' => Markup::create('<div><input type="text" size="' . strlen($name) . '" disabled id="name[' .  $this->configuration['name'] . ']" name="name[' . $this->configuration['name'] . ']" value="' . $name . '" /></div>')],
      ],
      'token' => [
        'label' => ['#markup' => Markup::create('<div><label for="token[' . $this->configuration['name'] . ']">' . $this->t('Token') . '</label>:</div>')],
        'usage' => ['#markup' => Markup::create('<div><input type="text" size="' . strlen($token) . '" disabled id="token[' . $this->configuration['name'] . ']" name="token[' . $this->configuration['name'] . ']" value="' . $token . '" /></div>')],
      ],
      'twig' => [
        'label' => ['#markup' => Markup::create('<div><label for="twig[' . $this->configuration['name'] . ']">' . $this->t('Twig') . '</label>:</div>')],
        'usage' => ['#markup' => Markup::create('<div><input type="text" size="' . strlen($token) . '" disabled id="twig[' . $this->configuration['name'] . ']" name="twig[' . $this->configuration['name'] . ']" value="{{ p(&quot;' . $name . '&quot;) }}" /></div>')],
      ],
    ];

    return $usages;
  }

}
