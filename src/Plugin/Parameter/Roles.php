<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Internals\ItemSortTrait;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a selection of user roles as parameter.
 *
 * @Parameter(
 *   id = "roles",
 *   label = @Translation("Selection of User roles"),
 *   weight = 80,
 *   entity_type = "user_role"
 * )
 */
#[Parameter(
  id: "roles",
  label: new TranslatableMarkup("Selection of User roles"),
  weight: 80,
  entity_type: "user_role"
)]
class Roles extends ParameterBase implements PropertyParameterInterface, DependentPluginInterface, AccessibleInterface, CacheableDependencyInterface {

  use ItemSortTrait;

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'list';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'selected' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $role_configs = $this->entityTypeManager->getStorage($this->pluginDefinition['entity_type'])->loadMultiple();
    $accessible_roles = array_filter(_parameters_user_role_names(), function ($rid) use ($role_configs) {
      if (isset($role_configs[$rid])) {
        /** @var \Drupal\user\RoleInterface $role_config */
        $role_config = $role_configs[$rid];
        return $role_config->status() && $role_config->access('view');
      }
      return TRUE;
    }, ARRAY_FILTER_USE_KEY);

    foreach ($accessible_roles as $rid => $label) {
      $options[$rid] = $this->t('@label (@machine_name)', [
        '@label' => $label,
        '@machine_name' => $rid,
      ]);
    }

    uasort($options, function ($a, $b) {
      return strnatcasecmp((string) $a, (string) $b);
    });

    $form['selected'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Selected @label', ['@label' => $this->t('roles')]),
      '#options' => $options,
      '#default_value' => $this->configuration['selected'] ?? [],
      '#required' => FALSE,
    ] + $this->ajaxHandlerForOrderFormElement();

    $this->buildOrderFormElementForSelected('selected', $options, $form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $selected = array_keys(array_filter($form_state->getValue('selected', [])));

    $this->submitOrderFormElementForSelected($selected, $form, $form_state);

    $this->configuration['selected'] = $selected;
  }

  /**
   * Set the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(){
    return $this->entityTypeManager->getDefinition('user_role')->getListCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];

    $role_storage = $this->entityTypeManager->getStorage($this->pluginDefinition['entity_type']);
    foreach ($role_storage->loadMultiple($this->configuration['selected'] ?? []) as $role_config) {
      $dependencies[$role_config->getConfigDependencyKey()][] = $role_config->getConfigDependencyName();
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $account = $account ?? \Drupal::currentUser();
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $result = AccessResult::allowedIfHasPermission($account, $entity_type->getAdminPermission());
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    if ($name === 'selected') {
      return $this;
    }

    $selected = $this->configuration['selected'];
    if (empty($selected)) {
      return NULL;
    }

    if (!($parts = explode('.', $name))) {
      return NULL;
    }

    $return_list = !ctype_digit(strval(reset($parts)));

    if (!$return_list) {
      $index = (int) array_shift($parts);
      if (!isset($selected[$index])) {
        return NULL;
      }
      if (empty($parts)) {
        return $this->parameterManager->createInstance('string', ['value' => $selected[$index]]);
      }
    }

    $name = implode('.', $parts);

    if (!in_array($name, ['id', 'rid', 'label', 'name'])) {
      return NULL;
    }

    $ids = $return_list ? $selected : [$selected[$index]];
    $_parameters_user_role_names = array_filter(_parameters_user_role_names(), function ($rid) use ($ids) {
      return in_array($rid, $ids, TRUE);
    }, ARRAY_FILTER_USE_KEY);

    $values = [];

    foreach ($_parameters_user_role_names as $rid => $label) {
      if ($name === 'id' || $name === 'rid') {
        $values[] = $rid;
      }
      elseif ($name === 'label' || $name === 'name') {
        $values[] = $label;
      }
    }

    if (empty($values)) {
      return NULL;
    }

    $value = $return_list ? array_unique($values) : reset($values);

    if ($value === NULL) {
      return NULL;
    }
    elseif (is_scalar($value)) {
      return $this->parameterManager->createInstance('string', ['value' => $value]);
    }
    else {
      return $this->parameterManager->createInstance('yaml', ['values' => $value]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $storage = $this->entityTypeManager->getStorage($entity_type->id());

    $selected = $this->configuration['selected'] ?? [];
    $selected = array_combine($selected, $selected);
    $configs = $storage->loadMultiple($selected);

    $missing = array_diff_key($selected, [RoleInterface::ANONYMOUS_ID => 1, RoleInterface::AUTHENTICATED_ID => 1] + $configs);

    if (!empty($missing)) {
      return $this->t('Following @type configs are missing: @missing', [
        '@type' => $entity_type->getLabel(),
        '@missing' => implode(', ', $missing),
      ]);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    return $this->configuration['selected'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $output = '';
    if ($selected = $this->configuration['selected']) {
      $num_items = count($selected);
      [$subset, $num_rest] = $num_items > 4 ? [array_slice($selected, 0, 3), ($num_items - 3)] : [$selected, 0];
      $output .= 'selected:';
      $output .= '<br/>';
      $output .= '<ul><li>' . implode('</li><li>', $subset) . '</li></ul>';
      if ($num_rest) {
        $output .= $this->t('+ @num more', ['@num' => $num_rest]);
      }
    }
    else {
      $output .= (string) $this->t('No items selected.');
    }
    return $output;
  }

}
