<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Internals\ItemSortTrait;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a selection of entity types as parameter.
 */
#[Parameter(
  id: "types",
  label: new TranslatableMarkup("Selection of entity types"),
  weight: 25
)]
class Types extends ParameterBase implements PropertyParameterInterface, DependentPluginInterface, AccessibleInterface {

  use ItemSortTrait;

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'list';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'selected' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];

    $entity_types = $this->entityTypeManager->getDefinitions();

    foreach ($entity_types as $entity_type) {
      $options[$entity_type->id()] = $this->t('@label (@machine_name)', [
        '@label' => $entity_type->getLabel(),
        '@machine_name' => $entity_type->id(),
      ]);
    }

    uasort($options, function ($a, $b) {
      return strnatcasecmp((string) $a, (string) $b);
    });

    $form['selected'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Selected @label', ['@label' => $this->t('entity types')]),
      '#options' => $options,
      '#default_value' => $this->configuration['selected'] ?? [],
      '#required' => FALSE,
    ] + $this->ajaxHandlerForOrderFormElement();

    $this->buildOrderFormElementForSelected('selected', $options, $form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $selected = array_keys(array_filter($form_state->getValue('selected', [])));

    $this->submitOrderFormElementForSelected($selected, $form, $form_state);

    $this->configuration['selected'] = $selected;
  }

  /**
   * Set the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = ['module' => []];
    foreach (($this->configuration['selected'] ?? []) as $entity_type_id) {
      $provider = $this->entityTypeManager->getDefinition($entity_type_id)->getProvider();
      if (!in_array($provider, ['core', 'component'], TRUE) && !in_array($provider, $dependencies['module'], TRUE)) {
        $dependencies['module'][] = $provider;
      }
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    if ($name === 'selected') {
      return $this;
    }

    $selected = $this->configuration['selected'];
    if (empty($selected)) {
      return NULL;
    }

    if (!($parts = explode('.', $name))) {
      return NULL;
    }

    $return_list = !ctype_digit(strval(reset($parts)));

    if (!$return_list) {
      $index = (int) array_shift($parts);
      if (!isset($selected[$index])) {
        return NULL;
      }
      if (empty($parts)) {
        return $this->parameterManager->createInstance('string', ['value' => $selected[$index]]);
      }
    }

    $name = implode('.', $parts);

    if (!in_array($name, ['id', 'label', 'name'])) {
      return NULL;
    }

    $ids = $return_list ? $selected : [$selected[$index]];

    /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
    $entity_types = [];
    foreach ($ids as $id) {
      if ($this->entityTypeManager->hasDefinition($id)) {
        $entity_types[$id] = $this->entityTypeManager->getDefinition($id);
      }
    }

    $values = [];

    foreach ($entity_types as $id => $entity_type) {
      if ($name === 'id') {
        $values[] = $id;
      }
      elseif ($name === 'label' || $name === 'name') {
        $values[] = $entity_type->getLabel();
      }
    }

    if (empty($values)) {
      return NULL;
    }

    $value = $return_list ? array_unique($values) : reset($values);

    if ($value === NULL) {
      return NULL;
    }
    elseif (is_scalar($value)) {
      return $this->parameterManager->createInstance('string', ['value' => $value]);
    }
    else {
      return $this->parameterManager->createInstance('yaml', ['values' => $value]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $selected = $this->configuration['selected'] ?? [];
    $selected = array_combine($selected, $selected);

    $entity_types = $this->entityTypeManager->getDefinitions();
    $missing = array_diff_key($selected, $entity_types);

    if (!empty($missing)) {
      return $this->t('Following entity types are missing: @missing', [
        '@missing' => implode(', ', $missing),
      ]);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    return $this->configuration['selected'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $output = '';
    if ($selected = $this->configuration['selected']) {
      $num_items = count($selected);
      [$subset, $num_rest] = $num_items > 4 ? [array_slice($selected, 0, 3), ($num_items - 3)] : [$selected, 0];
      $output .= 'selected:';
      $output .= '<br/>';
      $output .= '<ul><li>' . implode('</li><li>', $subset) . '</li></ul>';
      if ($num_rest) {
        $output .= $this->t('+ @num more', ['@num' => $num_rest]);
      }
    }
    else {
      $output .= (string) $this->t('No items selected.');
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $account = $account ?? \Drupal::currentUser();
    $result = AccessResult::allowedIfHasPermission($account, 'administer site configuration');
    return $return_as_object ? $result : $result->isAllowed();
  }

}
