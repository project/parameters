<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\UsageParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a secret as parameter.
 */
#[Parameter(
  id: "secret",
  label: new TranslatableMarkup("Secret")
)]
class Secret extends ParameterBase implements UsageParameterInterface {

  use MessengerTrait;

  /**
   * Identifies the salt either in settings, state or as service parameter.
   *
   * @var string
   */
  const SALT_KEY = 'secret_parameters.salt';

  /**
   * The fallback salt to use when no salt is specified by SALT_KEY.
   *
   * @var string
   */
  const FALLBACK_SALT = 'hash_salt';

  /**
   * The cipher algorithm to use for encryption.
   *
   * @var string
   */
  protected string $cipherAlgo;

  /**
   * The password generator.
   *
   * @var \Drupal\Core\Password\PasswordGeneratorInterface
   */
  protected PasswordGeneratorInterface $passwordGenerator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->cipherAlgo = $container->getParameter('secret_parameters.cipher_algo');
    $instance->passwordGenerator = $container->get('password_generator');
    $instance->setMessenger($container->get('messenger'));
    if (empty($configuration['encoding'] ?? NULL) && (($configuration['value'] ?? '') !== '')) {
      // Given value seems to be unencrypted, therefore conceal it now.
      $instance->conceal($configuration['value']);
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['encoding' => []] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    try {
      if (!empty($this->configuration['encoding'])) {
        $revealed = !empty($this->configuration['encoding']) ? $this->reveal() : '';
      }
    }
    catch (\RuntimeException $e) {
      $this->messenger()->addError($this->t($e->getMessage()));
      $revealed = '';
    }

    $form['value']['#type'] = 'parameter_secret';
    $form['value']['#required'] = FALSE; // An empty secret is allowed.
    $form['value']['#default_value'] = empty($this->configuration['encoding']) ? '' : $revealed;

    $form['help']['#prefix'] = '<p>';
    $form['help']['#markup'] = $this->t('Storing the value as a secret means, that the value is encrypted within configuration management. It will automatically be revealed on runtime, for example when accessed as a token or variable.');
    $form['help']['#weight'] = 100;
    $form['help']['#suffix'] = '</p>';

    static::getSalt($used_source);
    if ($used_source === 'state') {
      $this->messenger()->addWarning($this->t('The salt for the passphrase to encrypt the secret is currently set via Drupal state. This is fine for single environments but may not work well for multi-environment setups. Have a look at the <a href=":url" target="_blank" rel="nofollow noreferrer">Readme section 8</a> to see how to setup the passphrase salt.', [
        ':url' => 'https://git.drupalcode.org/project/parameters/-/blob/1.0.x/README.md#8-setup-of-passphrase-salt',
      ]));
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->conceal((string) $form_state->getValue('value'));
    if (($result = $this->validate()) !== TRUE) {
      $form_state->setError($form['value'], $result);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->conceal((string) $form_state->getValue('value'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $encoding = $this->configuration['encoding'];
    if (!empty($encoding)) {
      try {
        $this->reveal();
      }
      catch (\RuntimeException $e) {
        return $this->t($e->getMessage());
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    return $this->reveal();
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string|MarkupInterface {
    try {
      if (!empty($this->configuration['encoding'])) {
        $revealed = !empty($this->configuration['encoding']) ? $this->reveal() : '';
      }
    }
    catch (\RuntimeException $e) {
      $this->messenger()->addError($this->t($e->getMessage()));
      return $this->t('Broken');
    }

    $build = [
      '#type' => 'parameter_secret',
      '#value' => $revealed,
      '#attributes' => [
        'disabled' => 1,
        'style' => 'width: auto;',
      ],
      '#size' => mb_strlen($revealed) + 5,
    ];
    return \Drupal::service('renderer')->renderInIsolation($build);
  }

  /**
   * Use this parameter to conceal the given string value.
   *
   * The value can be revealed again by calling ::reveal().
   *
   * @param string $unencrypted
   *   The unencrypted string to conceal.
   *
   * @return $this
   *   The parameter instance itself.
   */
  public function conceal(string $unencrypted): static {
    static::ensureCipherAlgoExists($this->cipherAlgo);

    $ivlen = openssl_cipher_iv_length($this->cipherAlgo);
    $iv = openssl_random_pseudo_bytes($ivlen);
    $tag = NULL;
    $pass = $this->generatePassphrase();
    $salt = static::getSalt();

    $encrypted = openssl_encrypt(
      $unencrypted,
      $this->cipherAlgo,
      $pass . $salt,
      \OPENSSL_RAW_DATA,
      $iv,
      $tag
    );

    $this->configuration['value'] = base64_encode($encrypted);
    $this->configuration['encoding'] = [
      'pass' => $pass,
      'iv' => bin2hex($iv),
      'tag' => bin2hex($tag),
      'checksum' => hash('sha256', $unencrypted . $pass . $salt),
    ];

    return $this;
  }

  /**
   * Reveals the stored secret.
   *
   * @return string
   *   An unencrypted string value.
   *
   * @throws \RuntimeException
   *   When the reveal attempt failed.
   */
  public function reveal(): string {
    static::ensureCipherAlgoExists($this->cipherAlgo);

    $secret = base64_decode($this->configuration['value']);
    $encoding = $this->configuration['encoding'];
    $salt = static::getSalt();

    $revealed = openssl_decrypt(
      $secret,
      $this->cipherAlgo,
      $encoding['pass'] . $salt,
      \OPENSSL_RAW_DATA,
      hex2bin($encoding['iv']),
      isset($encoding['tag']) ? hex2bin($encoding['tag']) : NULL
    );

    if ((FALSE === $revealed) || ($encoding['checksum'] !== hash('sha256', $revealed . $encoding['pass'] . $salt))) {
      throw new \RuntimeException('Failed to reveal the stored secret (wrong salt?)');
    }

    return $revealed;
  }

  /**
   * Ensures the defined cipher algorithm is avaible on the current environment.
   *
   * @param string $cipher_algo
   *   The cipher algorithm to check.
   * @param bool $throw_exception
   *   (optional) Set to FALSE if no exception should be thrown when not exists.
   *
   * @throws \RuntimeException
   *   When the currently configured cipher algorithm does not exist.
   *
   * @return bool
   *   Returns TRUE when it exists.
   */
  public static function ensureCipherAlgoExists(string $cipher_algo, bool $throw_exception = TRUE): bool {
    $exists = &drupal_static(__FUNCTION__, []);

    if (!isset($exists[$cipher_algo])) {
      $exists[$cipher_algo] = in_array($cipher_algo, openssl_get_cipher_methods(), TRUE);
    }

    if (!$exists[$cipher_algo] && $throw_exception) {
      throw new \RuntimeException(sprintf("The requested cipher %s method for parameters is not available on this environment.", $cipher_algo));
    }

    return $exists[$cipher_algo];
  }

  /**
   * Generates a passphrase for encryption, without the salt.
   *
   * @return string
   *   The passphrase.
   */
  public function generatePassphrase(): string {
    return $this->passwordGenerator->generate(10);
  }

  /**
   * Get the passphrase salt.
   *
   * @param ?string &$used_source
   *   (optional) Passed by reference, this variable stores from where the salt
   *   has been used (such as "parameter", "settings" or "state").
   *
   * @return string
   *   The salt.
   */
  public static function getSalt(&$used_source = NULL): string {
    $key = static::SALT_KEY;

    $alt = \Drupal::getContainer()->getParameter(static::SALT_KEY);
    if ('' !== $alt) {
      $used_source = 'parameter';
      return $alt;
    }
    $alt = Settings::get(static::SALT_KEY, '');
    if ('' !== $alt) {
      $used_source = 'settings:' . static::SALT_KEY;
      return $alt;
    }
    $alt = \Drupal::state()->get(static::SALT_KEY, '');
    if ('' !== $alt) {
      $used_source = 'state';
      return $alt;
    }
    $alt = Settings::get(static::FALLBACK_SALT, '');
    if ('' !== $alt) {
      $used_source = 'settings:' . static::FALLBACK_SALT;
      return $alt;
    }

    // Still no hash salt provided, so generate one and persist it in state.
    $alt = \Drupal::service('password_generator')->generate(10);
    \Drupal::state()->set($key, $alt);
    $used_source = 'state';

    return $alt;
  }

  /**
   * {@inheritdoc}
   */
  public function onAddition(ParametersCollectionInterface $collection, string $operation): void {
    if (empty($this->configuration['encoding']) && ($this->configuration['value'] !== '')) {
      // Conceal and synchronize the configuration accordingly.
      $this->conceal($this->configuration['value']);
      $parameters_array = $collection->get('parameters');
      $parameters_array[$this->getName()] = $this->getConfiguration();
      $collection->setParameters($parameters_array);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onChange(array $previous_configuration, ParametersCollectionInterface $collection, string $operation): void {
    $this->onAddition($collection, $operation);
  }

  /**
   * {@inheritdoc}
   */
  public function onRemoval(ParametersCollectionInterface $collection, string $operation): void {}

}
