<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Internals\ItemSortTrait;
use Drupal\parameters\Plugin\Derivative\Parameter\BundlesDeriver;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a selection of entity bundles as parameter.
 */
#[Parameter(
  id: "bundles",
  label: new TranslatableMarkup("Selection of entity bundles"),
  weight: 50,
  deriver: BundlesDeriver::class
)]
class Bundles extends ParameterBase implements PropertyParameterInterface, DependentPluginInterface, AccessibleInterface, CacheableDependencyInterface {

  use ItemSortTrait;

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'list';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'selected' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType());

    $options = [];
    foreach ($this->entityTypeManager->getStorage($bundle_entity_type->id())->loadMultiple() as $bundle_config) {
      if (!$bundle_config->access('view')) {
        continue;
      }
      $options[$bundle_config->id()] = $this->t('@label (@machine_name)', [
        '@label' => $bundle_config->label(),
        '@machine_name' => $bundle_config->id(),
      ]);
    }

    uasort($options, function ($a, $b) {
      return strnatcasecmp((string) $a, (string) $b);
    });

    $form['selected'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Selected @label', ['@label' => $bundle_entity_type->getPluralLabel()]),
      '#options' => $options,
      '#default_value' => $this->configuration['selected'] ?? [],
      '#required' => FALSE,
    ] + $this->ajaxHandlerForOrderFormElement();

    $this->buildOrderFormElementForSelected('selected', $options, $form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $selected = array_keys(array_filter($form_state->getValue('selected', [])));

    $this->submitOrderFormElementForSelected($selected, $form, $form_state);

    $this->configuration['selected'] = $selected;
  }

  /**
   * Set the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(){
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType());
    return $bundle_entity_type->getListCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType());
    $bundle_storage = $this->entityTypeManager->getStorage($bundle_entity_type->id());

    foreach ($bundle_storage->loadMultiple($this->configuration['selected'] ?? []) as $bundle_config) {
      $dependencies[$bundle_config->getConfigDependencyKey()][] = $bundle_config->getConfigDependencyName();
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType());
    $bundle_storage = $this->entityTypeManager->getStorage($bundle_entity_type->id());
    $bundle_query_result = $bundle_storage->getQuery()->accessCheck(TRUE)->range(0, 1)->execute();
    $bundle_config = NULL;
    if ($bundle_query_result) {
      $id = reset($bundle_query_result);
      $bundle_config = $bundle_storage->load($id);
      return $bundle_config->access('view', $account, $return_as_object);
    }

    $result = AccessResult::forbidden("No accessible bundle config was found.");
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    if ($name === 'selected') {
      return $this;
    }

    $selected = $this->configuration['selected'];
    if (empty($selected)) {
      return NULL;
    }

    if (!($parts = explode('.', $name))) {
      return NULL;
    }

    $return_list = !ctype_digit(strval(reset($parts)));

    if (!$return_list) {
      $index = (int) array_shift($parts);
      if (!isset($selected[$index])) {
        return NULL;
      }
      if (empty($parts)) {
        return $this->parameterManager->createInstance('string', ['value' => $selected[$index]]);
      }
    }

    $name = implode('.', $parts);

    $ids = $return_list ? $selected : [$selected[$index]];

    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType());
    $bundle_storage = $this->entityTypeManager->getStorage($bundle_entity_type->id());

    $values = [];

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $bundle_config */
    foreach ($bundle_storage->loadMultiple($ids) as $bundle_config) {
      if ($parts) {
        $value = $bundle_config->get($name);
        if (NULL === $value) {
          $bundle_array = $bundle_config->toArray();
          $value = NestedArray::getValue($bundle_array, $parts);
        }
        if (is_array($value)) {
          $scalar_only = TRUE;
          array_walk_recursive($value, function($v) use (&$scalar_only) {
            if (!is_scalar($v) && !is_null($v)) {
              $scalar_only = FALSE;
            }
          });
          if (!$scalar_only) {
            $value = NULL;
          }
          unset($scalar_only);
        }
        if (is_scalar($value) || is_array($value)) {
          $values[] = $value;
        }
      }
      else {
        $value = $bundle_config->toArray();

        $scalar_only = TRUE;
        array_walk_recursive($value, function($v) use (&$scalar_only) {
          if (!is_scalar($v) && !is_null($v)) {
            $scalar_only = FALSE;
          }
        });
        if (!$scalar_only) {
          $value = NULL;
        }
        unset($scalar_only);

        if (is_scalar($value) || is_array($value)) {
          $values[] = $value;
        }
      }
    }

    if (empty($values)) {
      return NULL;
    }

    $value = $return_list ? array_unique($values) : reset($values);

    if ($value === NULL) {
      return NULL;
    }
    elseif (is_scalar($value)) {
      return $this->parameterManager->createInstance('string', ['value' => $value]);
    }
    else {
      return $this->parameterManager->createInstance('yaml', ['values' => $value]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType());
    $bundle_storage = $this->entityTypeManager->getStorage($bundle_entity_type->id());

    $selected = $this->configuration['selected'] ?? [];
    $selected = array_combine($selected, $selected);
    $bundle_configs = $bundle_storage->loadMultiple($selected);

    $missing = array_diff_key($selected, $bundle_configs);

    if (!empty($missing)) {
      return $this->t('Following bundles are missing: @missing', [
        '@missing' => implode(', ', $missing),
      ]);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    return $this->configuration['selected'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $output = '';
    if ($selected = $this->configuration['selected']) {
      $num_items = count($selected);
      [$subset, $num_rest] = $num_items > 4 ? [array_slice($selected, 0, 3), ($num_items - 3)] : [$selected, 0];
      $output .= 'selected:';
      $output .= '<br/>';
      $output .= '<ul><li>' . implode('</li><li>', $subset) . '</li></ul>';
      if ($num_rest) {
        $output .= $this->t('+ @num more', ['@num' => $num_rest]);
      }
    }
    else {
      $output .= (string) $this->t('No items selected.');
    }
    return $output;
  }

}
