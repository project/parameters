<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Entity\ParametersCollection;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a referenced parameter.
 */
#[Parameter(
  id: "reference",
  label: new TranslatableMarkup("Referenced parameter")
)]
class Reference extends ParameterBase implements PropertyParameterInterface, DependentPluginInterface {

  /**
   * The parameters collection storage.
   *
   * @var \Drupal\parameters\Entity\ParametersCollectionStorage
   */
  protected EntityStorageInterface $collectionStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\parameters\Plugin\Parameter\Reference $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setCollectionStorage($container->get('entity_type.manager')->getStorage(ParametersCollectionInterface::ENTITY_TYPE_ID));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['target_id' => NULL, 'target_name' => NULL];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $wrapper_id = Html::getUniqueId('target-name-wrapper');
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';
    $form['target_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Target collection'),
      '#default_value' => $this->configuration['target_id'],
      '#required' => TRUE,
      '#weight' => 10,
      '#options' => $this->collectionStorage::options(),
      '#ajax' => [
        'callback' => [static::class, 'ajaxTargetSelect'],
        'wrapper' => $wrapper_id,
      ],
      '#executes_submit_callback' => TRUE,
      '#limit_validation_errors' => [['target_id'], ['target_name']],
      '#submit' => [[$this, 'ajaxTargetSubmit']],
    ];
    $all_option = $this->t('- All -');
    $parameter_options = [
      '_NONE' => $this->t('- Choose -'),
      '_ALL' => $all_option,
    ];
    $collection_id = $form_state->has('collection_target_id') ? $form_state->get('collection_target_id') : $this->configuration['target_id'];
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    $collection = $collection_id ? $this->collectionStorage->load($collection_id) : NULL;
    $is_same_collection = $form_state->has('collection') && ($form_state->get('collection')->id() === ($form_state->get('collection_target_id') ?? $this->configuration['target_id']));
    if ($is_same_collection) {
      // Prevent some obvious infinite recursion.
      unset($parameter_options['_ALL']);
    }

    if ($collection) {
      foreach ($collection->getParameters() as $parameter) {
        $config = $parameter->getConfiguration();
        if ($is_same_collection && ($config['name'] === $this->configuration['name'])) {
          // Prevent some obvious infinite recursion.
          continue;
        }
        $parameter_options[$config['name']] = $config['label'] . ' (' . $config['name'] . ')';
      }
    }
    $form['target_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Target parameter'),
      '#default_value' => $this->configuration['target_name'] ?? '_ALL',
      '#required' => TRUE,
      '#empty_value' => '_NONE',
      '#weight' => 20,
      '#options' => $parameter_options,
      '#disabled' => !$collection,
    ];
    if (isset($parameter_options['_ALL'])) {
      $form['target_name']['#description'] = $this->t('When choosing %all then all parameters of the whole target collection are being referenced.', ['%all' => $all_option]);
    }
    return $form;
  }

  /**
   * Ajax callback for selecting a target collection.
   *
   * @param array $form
   *   The current form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The according form state.
   *
   * @return array
   *   The part of the form that got refreshed via Ajax.
   */
  public static function ajaxTargetSelect(array $form, FormStateInterface $form_state): array {
    return $form['parameter']['settings'];
  }

  /**
   * Submit Ajax callback for selecting a target collection.
   *
   * @param array $form
   *   The current form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The according form state.
   */
  public function ajaxTargetSubmit(array $form, FormStateInterface $form_state): void {
    $this->submitConfigurationForm($form, $form_state);
    $form_state->set('collection_target_id', $form_state->getValue('target_id'));
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('target_name', '_NONE') === '_NONE') {
      if (isset($form['target_name']['#options']) && count($form['target_name']['#options']) === 1) {
        $form_state->setError($form['target_name'], $this->t('The target collection does not contain any parameter to select from yet. At least one other parameter on that collection needs to be added first, then it can be referenced from here.'));
      }
      else {
        $form_state->setError($form['target_name'], $this->t('A target parameter must be selected.'));
      }
    }
    else {
      parent::validateConfigurationForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['target_id'] = $form_state->getValue('target_id');
    $this->configuration['target_name'] = $form_state->getValue('target_name', '');
    if ($this->configuration['target_name'] === '_ALL') {
      $this->configuration['target_name'] = NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    if (!isset($this->configuration['target_id']) || ($this->configuration['target_id'] === '')) {
      return $this->t('No target ID specified.');
    }
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    if (!($collection = $this->collectionStorage->load($this->configuration['target_id']))) {
      return $this->t('There is no parameters collection of specified ID %id.', ['%id' => $this->configuration['target_id']]);
    }
    if (isset($this->configuration['target_name']) && ($this->configuration['target_name'] !== '_ALL') && !$collection->getParameter($this->configuration['target_name'])) {
      return $this->t('The target collection has no parameter with name %name', ['%name' => $this->configuration['target_name']]);
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    if ($collection = $this->collectionStorage->load($this->configuration['target_id'])) {
      $parameter_name = $this->configuration['target_name'] ?? '';
      if ('' === $parameter_name) {
        // Only allow single property access, no exposure of whole collections.
        return '';
      }
      if ($parameter = $collection->getParameter($parameter_name)) {
        $data = $parameter->getProcessedData();
        $this->dataType = $data->getDataDefinition()->getDataType();
        return $data->getValue();
      }
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    if ($collection = $this->collectionStorage->load($this->configuration['target_id'])) {
      $referenced_parameter_name = $this->configuration['target_name'] ?? '';
      if ('' === $referenced_parameter_name) {
        return $collection->getParameter($name);
      }
      if ($parameter = $collection->getParameter($referenced_parameter_name)) {
        if ($referenced_parameter_name === $name) {
          return $parameter;
        }
        elseif ($parameter instanceof PropertyParameterInterface) {
          return $parameter->getProperty($name);
        }
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $preview = $this->configuration['target_id'] . '.' . ($this->configuration['target_name'] ?? '*');
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    if ($collection = $this->collectionStorage->load($this->configuration['target_id'])) {
      // Prevent auto-locking on previews.
      $lock_exists = isset(ParametersCollection::$lockSaves[$collection->id()]);
      ParametersCollection::$lockSaves[$collection->id()] = 1;
      $referenced_parameter_name = $this->configuration['target_name'] ?? '';
      if (('' !== $referenced_parameter_name) && ($parameter = $collection->getParameter($referenced_parameter_name))) {
        $preview .= ':<div><em>' . $parameter->getPreview() . '</em></div>';
      }
      if (!$lock_exists) {
        unset(ParametersCollection::$lockSaves[$collection->id()]);
      }
    }
    return $preview;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];
    if (isset($this->configuration['target_id']) && ($collection = $this->collectionStorage->load($this->configuration['target_id']))) {
      $dependencies[$collection->getConfigDependencyKey()][] = $collection->getConfigDependencyName();
    }
    return $dependencies;
  }

  /**
   * Set the parameters collection storage.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionStorage $storage
   *   The parameters collection storage.
   */
  public function setCollectionStorage(EntityStorageInterface $storage): void {
    $this->collectionStorage = $storage;
  }

}
