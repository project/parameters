<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;

/**
 * Defines a boolean as parameter.
 */
#[Parameter(
  id: 'boolean',
  label: new TranslatableMarkup("Boolean")
)]
class Boolean extends ParameterBase {

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'boolean';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['value' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['value']['#type'] = 'select';
    $form['value']['#options'] = [
      'true' => $this->t('True'),
      'false' => $this->t('False'),
    ];
    $form['value']['#default_value'] = !isset($this->configuration['value']) || !empty($this->configuration['value']) ? 'true' : 'false';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['value'] = $form_state->getValue('value') === 'true' ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    return $this->configuration['value'] ? $this->t('True') : $this->t('False');
  }

}
