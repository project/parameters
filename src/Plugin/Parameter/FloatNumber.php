<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;

/**
 * Defines a float as parameter.
 */
#[Parameter(
  id: "float",
  label: new TranslatableMarkup("Float")
)]
class FloatNumber extends ParameterBase {

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'float';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['value']['#type'] = 'number';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['value'] = $form_state->getValue('value');
  }

}
