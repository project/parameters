<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Yaml as SerializationYaml;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\BackendChain;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an Http endpoint parameter.
 */
#[Parameter(
  id: "http",
  label: new TranslatableMarkup("Http endpoint")
)]
class Http extends ParameterBase implements PropertyParameterInterface, CacheableDependencyInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The cache backend that is an in-memory cache for retrieved data.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $memoryCache;

  /**
   * The consistent cache backend for retrieved data.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $consistentCache;

  /**
   * The chained cache backends for retrieved data.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $chainedCache;

  /**
   * A list of lock wait counts.
   *
   * @var array
   */
  protected static array $lockWait = [];

  /**
   * The lock service.
   *
   * var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\parameters\Plugin\Parameter\Http $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));

    $instance->memoryCache = $container->get('parameter.data_memory_cache');
    $instance->consistentCache = $container->get('cache.parameter_data');
    $instance->chainedCache = (new BackendChain())
      ->appendBackend($instance->memoryCache)
      ->appendBackend($instance->consistentCache);

    $default_user_agent = method_exists('GuzzleHttp\Utils' , 'defaultUserAgent') ? \GuzzleHttp\Utils::defaultUserAgent() : \GuzzleHttp\default_user_agent();

    $client_config = [
      'headers' => [
        'User-Agent' => 'Drupal/' . \Drupal::VERSION . ' (+https://www.drupal.org/) ' . $default_user_agent . ' Parameters/1',
      ],
    ];
    $client = $container->get('http_client_factory')->fromOptions($client_config);
    $instance->setHttpClient($client);

    $instance->setLock($container->get('lock'));
    $instance->logger = $container->get('logger.factory')->get('parameters');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'url' => '',
      'cache_ttl_min' => 60,
      'cache_ttl_max' => -1,
      'cache_volatility' => 10,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint URL'),
      '#description' => $this->t('This must be a valid, publicly accessible URL. Both absolute and relative URLs are supported.<br/><strong>Please note:</strong> Fetched contents will be cached internally and will <em>always</em> be shared accross multiple sessions of different users.'),
      '#default_value' => $this->configuration['url'] ?? '',
      '#required' => TRUE,
      '#weight' => 10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $this->submitConfigurationForm($form, $form_state);
    if (($result = $this->validate()) !== TRUE) {
      $form_state->setError($form, $result);
    }
    $this->setConfiguration($config);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $defaults = $this->defaultConfiguration();
    $this->configuration['url'] = $form_state->getValue('url', $defaults['url']);
    $this->configuration['cache_ttl_min'] = $form_state->getValue('cache_ttl_min', $defaults['cache_ttl_min']);
    $this->configuration['cache_ttl_max'] = $form_state->getValue('cache_ttl_max', $defaults['cache_ttl_max']);
    $this->configuration['cache_volatility'] = $form_state->getValue('cache_volatility', $defaults['cache_volatility']);
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    if (($empty_check = parent::validate()) !== TRUE) {
      return $empty_check;
    }
    $url = NULL;
    try {
      $url = Url::fromUri($this->configuration['url']);
    }
    catch (\InvalidArgumentException $e1) {
      try {
        $url = Url::fromUserInput($this->configuration['url']);
      }
      catch (\InvalidArgumentException $e2) {
        $url = NULL;
      }
    }
    if (!$url) {
      return $this->t('The provided URL is not valid.');
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    $data = $this->getData($this->configuration['url']);
    $this->dataType = is_scalar($data) ? gettype($data) : 'map';
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $data = $this->getProcessedData();
    if ($data->getDataDefinition()->getDataType() === 'map') {
      return (string) nl2br(Html::escape(mb_substr(SerializationYaml::encode($data->getValue() ?? []), 0, 100)));
    }
    return (string) nl2br(Html::escape(mb_substr($data->getString(), 0, 100)));
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    $data = $this->getProcessedData();
    $value = NULL;
    if ($data->getDataDefinition()->getDataType() === 'map') {
      $values = $data->getValue();
      $value = NestedArray::getValue($values, explode('.', $name));
    }
    if ($value === NULL) {
      return NULL;
    }
    elseif (is_scalar($value)) {
      return $this->parameterManager->createInstance('string', ['value' => $value]);
    }
    else {
      return $this->parameterManager->createInstance('yaml', ['values' => $value]);
    }
  }

  /**
   * Set the lock service.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   */
  public function setLock(LockBackendInterface $lock): void {
    $this->lock = $lock;
  }

  /**
   * Set the Http client.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The client.
   */
  public function setHttpClient($client) {
    $this->client = $client;
  }

  /**
   * Get data from the given URL.
   *
   * @param \Drupal\Core\Url|string $url
   *   The URL to get the data from.
   * @param \Drupal\Core\Render\BubbleableMetadata|null $bubbleable_metadata
   *   (optional) The bubbleable metadata for attaching cacheability metadata.
   * @param bool $throw_exception
   *   Set to TRUE when an exception should be thrown on errors.
   * @param bool $use_cache
   *   Set to FALSE to bypass caching and always fire up a request instead.
   * @param bool &$faulty
   *   This flag will be set to TRUE in case the endpoint behaves faulty.
   *
   * @return mixed
   *   Either the decoded data, or FALSE on failure.
   */
  public function getData($url, BubbleableMetadata $bubbleable_metadata = NULL, $throw_exception = FALSE, $use_cache = TRUE, bool &$faulty = FALSE) {
    if (!($url instanceof Url) && !is_string($url)) {
      throw new \InvalidArgumentException('The $url param must be a Url object or string.');
    }

    if (is_string($url)) {
      try {
        $url = Url::fromUri($this->configuration['url']);
      }
      catch (\InvalidArgumentException $e1) {
        $url = Url::fromUserInput($this->configuration['url']);
      }
    }

    $uri = $url instanceof Url ? (clone $url)->setAbsolute(TRUE)->toString() : $url;
    if (!isset($bubbleable_metadata)) {
      $bubbleable_metadata = new BubbleableMetadata();
    }

    if ($data = $this->readCache($use_cache, $uri, $bubbleable_metadata)) {
      if (['___error' => TRUE] === $data) {
        $this->logger->error(t('The endpoint @url seems to be faulty. Please see previous logged error messages for more information. This error message would disappear on its own, in case the endpoint has recovered.',
        ['@url' => $uri]));
        $faulty = TRUE;
        return FALSE;
      }
      return $data;
    }

    $body = NULL;
    $data = NULL;

    $lock = $this->lock;
    $lid = $this->buildLockId($uri);
    $lock_acquired = $lock->acquire($lid, 5.0);
    if (!$lock_acquired) {
      if (!isset(self::$lockWait[$lid])) {
        self::$lockWait[$lid] = 0;
      }
      sleep(1);
      self::$lockWait[$lid]++;
      if (self::$lockWait[$lid] > 3) {
        $this->logger->warning(t('Lock wait exceeded for endpoint @uri. Now trying to fetch data from it. This may cause more traffic than needed.', ['@url' => $uri]));
      }
      else {
        return $this->getData($url, $bubbleable_metadata, $throw_exception, $use_cache);
      }
    }
    unset(self::$lockWait[$lid]);

    try {
      $response = $this->client->request('GET', $uri);
    }
    catch (GuzzleException $e) {
      $this->logger->error(t('Failed to fetch data from @url. Exception message: @message',
        ['@url' => $uri, '@message' => $e->getMessage()]));
      if ($throw_exception) {
        throw $e;
      }
      return $this->fallbackOnFailure($use_cache, $uri, $body, $data, $bubbleable_metadata);
    }

    if (!($response->getStatusCode() == 200)) {
      $message = t('Failed to fetch data from @url. Returned status code was: @status',
      ['@url' => $uri, '@status' => $response->getStatusCode()]);
      $this->logger->error($message);
      if ($throw_exception) {
        throw new \RuntimeException($message);
      }
      return $this->fallbackOnFailure($use_cache, $uri, $body, $data, $bubbleable_metadata, $response);
    }

    try {
      $body = trim($response->getBody()->getContents());
    }
    catch (\RuntimeException $e) {
      $this->logger->error(t('Failed to read response body contents from @url. Exception message: @message',
        ['@url' => $uri, '@message' => $e->getMessage()]));
      if ($throw_exception) {
        throw $e;
      }
      return $this->fallbackOnFailure($use_cache, $uri, $body, $data, $bubbleable_metadata, $response);
    }
    $data = $this->decode($body);

    if (is_array($data) && !isset($data['previous']) && !empty($data) && !static::arrayIsSequential($data)) {
      if ($previous = $this->readCache($use_cache, $uri, new BubbleableMetadata(), TRUE)) {
        if (isset($previous['previous'])) {
          $prev_prev = $previous['previous'];
          // Unset the "previous" key from the previously fetched data, in order
          // to avoid unlimited growth.
          unset($previous['previous']);
          if ($previous == $data) {
            // Previously fetched data appears to be the same, thus jump once
            // more back to previous data of the previously fetched one.
            unset($prev_prev['previous']);
            $previous = $prev_prev;
          }
        }
        $data['previous'] = $previous;
        // We trick a little bit here, telling ::writeCache that we originally
        // received JSON-encoded data, but now having the "previous" data key.
        $body = json_encode($data);
      }
    }
    if (is_array($data)) {
      $cache_data = $data;
    }
    else {
      $cache_data = ['__string' => $data];
    }
    $this->writeCache($use_cache, $uri, $body, $cache_data, $bubbleable_metadata, $response);
    if ($lock_acquired) {
      $lock->release($lid);
    }

    return $data;
  }

  /**
   * Get the memory cache.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The memory cache.
   */
  public function getDataMemoryCache() {
    return $this->memoryCache;
  }

  /**
   * Get the consistent cache.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The consistent cache.
   */
  public function getDataConsistentCache() {
    return $this->consistentCache;
  }

  /**
   * Get the chained cache.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The chained cache.
   */
  public function getDataChainedCache() {
    return $this->chainedCache;
  }

  /**
   * Implementation method of reading data from cache.
   *
   * @param bool $use_cache
   *   Whether caching should be used or not.
   * @param string &$uri
   *   The URI that is being used to perform the request.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata
   *   The bubbleable metadata for attaching cacheability metadata.
   * @param bool $allow_invalid
   *   (optional) If TRUE, a cache item may be returned even if it is expired or
   *   has been invalidated.
   *
   * @return mixed
   *   The decoded data successful cache hit, FALSE otherwise.
   */
  protected function readCache($use_cache, &$uri, BubbleableMetadata $bubbleable_metadata, $allow_invalid = FALSE) {
    if (!$use_cache) {
      return FALSE;
    }
    $cid = $this->buildCacheId($uri);
    // When TTL min is set to a permanent cache lifetime, it would mean that
    // one does not care about any TTLs from received responses. For that reason
    // we allow invalid items to be returned, as this may also save some lives.
    $allow_invalid = $allow_invalid || ($this->configuration['cache_ttl_min'] == CacheBackendInterface::CACHE_PERMANENT);
    if ($cached = $this->chainedCache->get($cid, $allow_invalid)) {
      if ($cached->expire != CacheBackendInterface::CACHE_PERMANENT) {
        $request_time = \Drupal::time()->getRequestTime();
        $max_age = $cached->expire > $request_time ? $cached->expire - $request_time : 0;
        $max_age = $this->configuration['cache_ttl_min'] > $max_age ? $this->configuration['cache_ttl_min'] : $max_age;
        $bubbleable_metadata->setCacheMaxAge($max_age);
      }
      elseif (!$cached->valid) {
        $max_age = $this->configuration['cache_ttl_min'] > 0 ? $this->configuration['cache_ttl_min'] : 30;
        $bubbleable_metadata->setCacheMaxAge($max_age);
      }
      $data = $cached->data;
      if (is_array($data) && isset($data['__string'])) {
        $data = $data['__string'];
      }
      if (is_string($data)) {
        // A string indicates that the contents of the response body was saved.
        // Thus we may try to convert it to a decoded array here, so that
        // subsequent calls within the same process don't need to decode again.
        $data = $this->decode($data);
        $this->memoryCache->set($cid, $data, $cached->expire, $cached->tags);
        return $data;
      }
      return $data;
    }
    return FALSE;
  }

  /**
   * Implementation method of writing data into the cache.
   *
   * @param bool $use_cache
   *   Whether caching should be used or not.
   * @param string $uri
   *   The URI that was used for performing the request.
   * @param string $body
   *   The extracted contents of the response body.
   * @param array &$data
   *   The data to cache.
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata
   *   The bubbleable metadata for attaching cacheability metadata.
   * @param \Psr\Http\Message\ResponseInterface|null $response
   *   The received response object (if any).
   */
  protected function writeCache($use_cache, $uri, $body, array &$data, BubbleableMetadata $bubbleable_metadata, ResponseInterface $response = NULL) {
    if (!$use_cache || $this->configuration['cache_ttl_max'] === 0) {
      return;
    }
    $expire = $this->configuration['cache_ttl_min'];
    $exclude = ['private', 'no-cache', 'no-store', 'must-revalidate'];
    $current_time = \Drupal::time()->getCurrentTime();
    // Read out TTL from response header (if available),
    // compare it with TTL max and set expiring time accordingly.
    if ($response && $this->configuration['cache_ttl_min'] != CacheBackendInterface::CACHE_PERMANENT) {
      $values = [];
      foreach ($response->getHeader('Cache-Control') as $header) {
        foreach (explode(',', $header) as $value) {
          $value = mb_strtolower(trim($value));
          if (in_array($value, $exclude)) {
            $expire = $this->configuration['cache_ttl_min'];
            $values = [];
            break 2;
          }
          if ($value == 'public') {
            $expire = $this->configuration['cache_ttl_max'];
            continue;
          }
          $values[] = $value;
        }
      }
      foreach ($values as $value) {
        if (strpos($value, 'age=')) {
          $value = explode('=', $value);
          $value = intval(end($value));
          // Fetch the highest possible value.
          $expire = $expire < $value ? $value : $expire;
        }
      }
      if ($this->configuration['cache_ttl_max'] != CacheBackendInterface::CACHE_PERMANENT && $expire > $this->configuration['cache_ttl_max']) {
        $expire = $this->configuration['cache_ttl_max'];
      }
      elseif ($expire >= PHP_INT_MAX - $current_time) {
        $expire = CacheBackendInterface::CACHE_PERMANENT;
      }
      elseif ($expire < 0 && $expire != CacheBackendInterface::CACHE_PERMANENT) {
        $expire = $this->configuration['cache_ttl_min'];
      }
    }
    if ($expire > 0 && $this->configuration['cache_volatility'] > 0) {
      $vola = $this->configuration['cache_volatility'] < $expire ? random_int(-$this->configuration['cache_volatility'], $this->configuration['cache_volatility']) : random_int(-$expire + 1, $expire - 1);
      $expire += $vola;
    }

    $cid = $this->buildCacheId($uri);
    $tags = ['parameters:http'];
    if ($expire === 0 && $this->configuration['cache_ttl_max'] !== 0) {
      // At least store in memory cache, so that the current PHP process is not
      // getting busy on redundant request calls.
      $expire = $this->configuration['cache_ttl_min'] > 0 ? $this->configuration['cache_ttl_min'] : 30;
      $bubbleable_metadata->setCacheMaxAge($expire);
      // Convert from the relative time period to an absolute Unix timestamp.
      $expire += $current_time;
      $this->memoryCache->set($cid, $data, $expire, $tags);
      return;
    }
    elseif ($expire > 0) {
      $bubbleable_metadata->setCacheMaxAge($expire);
      // Convert from the relative time period to an absolute Unix timestamp.
      $expire += $current_time;
    }
    $this->memoryCache->set($cid, $data, $expire, $tags);
    // Write the response body as string into the consistent backend, in order
    // to avoid serialization and possibly dangerous unserialization of the
    // retrieved data. We cannot verdict the data's trustworthiness here.
    $this->consistentCache->set($cid, $body, $expire, $tags);
  }

  /**
   * Builds the cache ID.
   *
   * @param string $uri
   *   The URI that was or is being used for performing the request.
   *
   * @return string
   *   The cache ID.
   */
  protected function buildCacheId($uri) {
    return 'parameters:http:' . $uri;
  }

  /**
   * Builds the lock ID.
   *
   * @param string $uri
   *   The URI that was or is being used for performing the request.
   *
   * @return string
   *   The lock ID.
   */
  protected function buildLockId($uri) {
    return 'parameters:http:' . $uri;
  }

  /**
   * Decodes the contents of a response body.
   *
   * @param string $body
   *   The extracted contents of the response body.
   *
   * @return mixed
   *   The decoded data.
   */
  protected function decode($body) {
    $data = json_decode($body, TRUE);
    if ($data === NULL && $body !== '') {
      $data = $body;
    }
    return $data;
  }

  /**
   * Implementation of a fallback behavior when something went wrong.
   *
   * @param bool $use_cache
   *   Whether caching should be used or not.
   * @param string $uri
   *   The URI that was used for performing the request.
   * @param mixed &$body
   *   The extracted contents of the response body (if any).
   * @param mixed &$data
   *   The decoded data (if any).
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata
   *   The bubbleable metadata for attaching cacheability metadata.
   * @param \Psr\Http\Message\ResponseInterface|null $response
   *   The received response object (if any).
   *
   * @return mixed
   *   Either decoded data to be used as fallback value, or FALSE if nothing
   *   else could have helped as a fallback solution.
   */
  protected function fallbackOnFailure($use_cache, $uri, &$body, &$data, BubbleableMetadata $bubbleable_metadata, ResponseInterface $response = NULL) {
    if ($ttl_min = $this->configuration['cache_ttl_min']) {
      $this->configuration['cache_ttl_min'] = 30;
    }
    // Allow to self-heal by setting max-age to 0 by default. Writing into the
    // cache would most probably raise the value, but it shouldn't be possible
    // to cache faulty and stale data indefinitely.
    $bubbleable_metadata->setCacheMaxAge(0);
    // As long as we still have something in our cache, use it and let's hope
    // that the endpoint will get well soon.
    if ($data = $this->readCache($use_cache, $uri, $bubbleable_metadata, TRUE)) {
      // We trick a little bit here, telling ::writeCache that we received
      // valid Json (but we didn't).
      $body = json_encode($data);
      $this->writeCache($use_cache, $uri, $body, $data, $bubbleable_metadata);
      $this->configuration['cache_ttl_min'] = $ttl_min;
      return $data;
    }
    // Flag this operation for our own cache, so that we don't retry too often
    // within the next few seconds. Give the endpoint a chance to breathe.
    $error_data = ['___error' => TRUE];
    $this->writeCache($use_cache, $uri, json_encode($error_data), $error_data, $bubbleable_metadata);
    $this->configuration['cache_ttl_min'] = $ttl_min;
    return FALSE;
  }

  /**
   * Checks whether the given array is sequential.
   *
   * @param array $array
   *   The array to check for.
   *
   * @return bool
   *   Returns TRUE when it is sequential, FALSE otherwise.
   */
  protected static function arrayIsSequential(array $array) {
    $i = 0;
    foreach (array_keys($array) as $key) {
      if ($key !== $i) {
        return FALSE;
      }
      $i++;
    }
    return TRUE;
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return ['parameters:http'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    if (!isset($this->processedData)) {
      $this->getProcessedData();
    }
    $bubbleable_metadata = new BubbleableMetadata();
    if (FALSE === $this->getData($this->configuration['url'], $bubbleable_metadata)) {
      return 0;
    }
    return $bubbleable_metadata->getCacheMaxAge();
  }

}
