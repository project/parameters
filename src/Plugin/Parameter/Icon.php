<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Drupal\parameters\Plugin\RenderableParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an icon as parameter.
 */
#[Parameter(
  id: "icon",
  label: new TranslatableMarkup("Icon (SVG)")
)]
class Icon extends ParameterBase implements RenderableParameterInterface, PropertyParameterInterface {

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['value']['#type'] = 'textarea';
    $form['value']['#description'] = $this->t('Insert raw markup here. It must start and end with a valid &lt;svg&gt; tag.');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $value = $form_state->getValue('value', '');
    if (!str_starts_with($value, '<svg') || !str_ends_with($value, '</svg>')) {
      $form_state->setErrorByName('value', $this->t('The value must start with "&lt;svg" and must end with "&lt;/svg&gt;"'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $value = $this->configuration['value'] ?? '';
    if (!str_starts_with($value, '<svg') || !str_ends_with($value, '</svg>')) {
      return $this->t('The value must start with "&lt;svg" and must end with "&lt;/svg&gt;"');
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): MarkupInterface {
    $build = $this->toRenderableInline();
    $build['#attributes']['width'] = 100;
    $build['#attributes']['height'] = 100;
    $build['#attributes']['fill'] = '#676767';
    return $this->renderer->render($build);
  }

  /**
   * {@inheritdoc}
   */
  public function getUsageHelp(EntityInterface $entity): array {
    $name = $entity->id() . ':' . $this->configuration['name'];
    $token = '[parameter:' . $name . ':100:100:#676767' . ']';

    $usages = parent::getUsageHelp($entity);

    $usages['token'] = [
      'label' => ['#markup' => Markup::create('<div><label for="token[' . $this->configuration['name'] . ']">' . $this->t('Token') . '</label>:</div>')],
      'usage' => ['#markup' => Markup::create('<div><input type="text" size="' . strlen($token) . '" disabled id="token[' . $this->configuration['name'] . ']" name="token[' . $this->configuration['name'] . ']" value="' . $token . '" /></div>')],
    ];
    $twig_example = '{{ p(&quot;' . $name . '&quot;)|set_attribute(&quot;fill&quot;, &quot;#676767&quot;) }}';
    $usages['twig'] = [
      'label' => ['#markup' => Markup::create('<div><label for="twig[' . $this->configuration['name'] . ']">' . $this->t('Twig') . '</label>:</div>')],
      'usage' => ['#markup' => Markup::create('<div><input type="text" size="' . strlen($twig_example) - 35 . '" disabled id="twig[' . $this->configuration['name'] . ']" name="twig[' . $this->configuration['name'] . ']" value="' . $twig_example . '" /></div>')],
    ];

    return $usages;
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    if ($name === 'value') {
      $value = $this->configuration['value'] ?? NULL;
      return NULL == $value ? NULL : $this->parameterManager->createInstance('string', ['value' => (string) $value]);
    }

    if ($name === 'attributes') {
      return $this->parameterManager->createInstance('yaml', ['values' => $this->getIconAttributes()]);
    }

    if ($name === 'contents') {
      return $this->parameterManager->createInstance('string', ['value' => $this->getIconContents()]);
    }

    $build = $this->toRenderableInline();
    $parts = explode('.', $name);
    [$width, $height, $fill] = array_merge($parts, [NULL, NULL, NULL]);
    if (isset($width)) {
      $build['#attributes']['width'] = $width;
    }
    if (isset($height)) {
      $build['#attributes']['height'] = $height;
    }
    if (isset($fill)) {
      $build['#attributes']['fill'] = $fill;
    }

    $output = $this->renderer->renderInIsolation($build);
    return $this->parameterManager->createInstance('string', ['value' => (string) $output]);
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function toRenderable() {
    return $this->toRenderableInline();
  }

  /**
   * Returns a renderable array for an inline icon.
   *
   * @return array
   *   A renderable array for an inline icon.
   */
  public function toRenderableInline(): array {
    $build = [
      '#type' => 'parameter_icon',
      '#parameter' => $this,
    ];

    return $build;
  }

  /**
   * Get configured icon attributes.
   *
   * @return array
   *   Configured attributes.
   */
  public function getIconAttributes(): array {
    $attributes = [];

    $element = $this->getIconAsDomElement();

    // @todo Properly retrieve the namespaces.
    $attributes['xmlns'] = $element->namespaceURI ?? 'http://www.w3.org/2000/svg';

    foreach ($element->attributes as $attribute) {
      /** @var \DOMAttr $attribute */
      $attributes[$attribute->name] = ($attribute->value);
    }

    return $attributes;
  }

  /**
   * Get configured icon contents.
   *
   * @return string
   *   Configured contents.
   */
  public function getIconContents(): string {
    $value = trim($this->configuration['value'] ?? '<svg></svg>');
    $contents = mb_substr(strstr($value, '>'), 1, -6);
    return $contents;
  }

  /**
   * Get the configured icon as a DOM element.
   *
   * @return \DOMElement
   *   The icon as DOM element.
   */
  public function getIconAsDomElement(): \DOMElement {
    $dom = Html::load($this->configuration['value'] ?? '<svg></svg>');
    $xpath = new \DOMXPath($dom);
    $element = $xpath->query("//svg")->item(0);
    return $element;
  }

}
