<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;

/**
 * Defines a raw string as parameter.
 */
#[Parameter(
  id: "string",
  label: new TranslatableMarkup("Raw string")
)]
class RawString extends ParameterBase {}
