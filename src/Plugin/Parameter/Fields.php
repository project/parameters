<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Internals\ItemSortTrait;
use Drupal\parameters\Plugin\Derivative\Parameter\FieldsDeriver;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a selection of fields as parameter.
 */
#[Parameter(
  id: "fields",
  label: new TranslatableMarkup("Selection of entity fields"),
  weight: 75,
  deriver: FieldsDeriver::class
)]
class Fields extends ParameterBase implements PropertyParameterInterface, DependentPluginInterface, AccessibleInterface, CacheableDependencyInterface {

  use ItemSortTrait;

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'list';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setEntityFieldManager($container->get('entity_field.manager'));
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    $instance->setModuleHandler($container->get('module_handler'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'bundle' => NULL,
      'selected' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = NULL !== $entity_type->getBundleEntityType() ? $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType()) : NULL;
    $bundle_configs = $bundle_entity_type ? $this->entityTypeManager->getStorage($bundle_entity_type->id())->loadMultiple() : [];
    $bundle_configs = array_filter($bundle_configs, function ($bundle_config) {
      /** @var \Drupal\Core\Entity\EntityInterface $bundle_config */
      return $bundle_config->access('view');
    });

    $bundle_options = [
      '_none' => $this->t('- All fields -'),
    ];
    foreach ($bundle_configs as $bundle_config) {
      $bundle_options[$bundle_config->id()] = $this->t('@label (@machine_name)', [
        '@label' => $bundle_config->label(),
        '@machine_name' => $bundle_config->id(),
      ]);
    }

    uasort($bundle_options, function ($a, $b) {
      return strnatcasecmp((string) $a, (string) $b);
    });

    $route_params = \Drupal::routeMatch()->getParameters()->all();
    $bundle_selected = $form_state->isRebuilding() ? ($form_state->getUserInput()['bundle'] ?? NULL) : (
      $this->configuration['bundle'] ?? (($form_state->get('parameter_is_new') && isset($route_params['entity_type_id'], $route_params['bundle']) && $route_params['entity_type_id'] === $entity_type->id()) ? $route_params['bundle'] : NULL)
    );
    if ($bundle_selected === '_none') {
      $bundle_selected = NULL;
    }

    $wrapper_id = Html::getUniqueId('selected-wrapper');

    $form['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Fields of @type', ['@type' => $bundle_entity_type->getLabel()]),
      '#options' => $bundle_options,
      '#default_value' => $bundle_selected,
      '#required' => FALSE,
      '#access' => isset($bundle_entity_type),
      '#weight' => 10,
      '#ajax' => [
        'wrapper' => $wrapper_id,
        'callback' => [static::class, 'bundleAjax'],
        'method' => 'html',
      ],
      '#executes_submit_callback' => TRUE,
      '#limit_validation_errors' => [],
      '#submit' => [[static::class, 'bundleSubmit']],
    ];

    $field_options = [];
    $bundles = $bundle_entity_type ? (isset($bundle_selected) ? [$bundle_selected] : array_keys($bundle_configs)) : [$entity_type->id()];
    foreach ($bundles as $bundle) {
      foreach ($this->entityFieldManager->getFieldDefinitions($entity_type->id(), $bundle) as $definition) {
        $field_options[$definition->getName()] = $this->t('@label (@machine_name)', [
          '@label' => $definition->getLabel(),
          '@machine_name' => $definition->getName(),
        ]);
      }
    }

    uasort($field_options, function ($a, $b) {
      return strnatcasecmp((string) $a, (string) $b);
    });

    $form['selected'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Selected fields'),
      '#options' => $field_options,
      '#default_value' => $this->configuration['selected'] ?? [],
      '#required' => FALSE,
      '#weight' => 20,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ] + $this->ajaxHandlerForOrderFormElement();

    $this->buildOrderFormElementForSelected('selected', $field_options, $form, $form_state);

    return $form;
  }

  /**
   * Ajax callback of the bundle selection element.
   *
   * @param array &$form
   *   The form build.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form element to refresh.
   */
  public static function bundleAjax(array &$form, FormStateInterface $form_state): array {
    return $form['parameter']['settings']['selected'];
  }

  /**
   * Submit callback of the bundle selection element.
   *
   * @param array &$form
   *   The form build.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function bundleSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $bundle = $form_state->getValue('bundle', '_none');
    $this->configuration['bundle'] = $bundle === '_none' ? NULL : $bundle;
    $selected = array_keys(array_filter($form_state->getValue('selected', [])));

    $this->submitOrderFormElementForSelected($selected, $form, $form_state);

    $this->configuration['selected'] = $selected;
  }

  /**
   * Set the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * Set the entity field manager.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function setEntityFieldManager(EntityFieldManagerInterface $entity_field_manager): void {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * Set the module handler.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function setModuleHandler(ModuleHandlerInterface $module_handler): void {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(){
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = NULL !== $entity_type->getBundleEntityType() ? $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType()) : NULL;
    $tags = $bundle_entity_type ? $bundle_entity_type->getListCacheTags() : [];
    if ($this->moduleHandler->moduleExists('field')) {
      $tags = array_unique(array_merge($tags, $this->entityTypeManager->getDefinition('field_config')->getListCacheTags()));
    }
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = NULL !== $entity_type->getBundleEntityType() ? $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType()) : NULL;

    $bundles = $bundle_entity_type ? [] : [$entity_type->id()];

    if ($bundle_entity_type) {
      $bundle_storage = $this->entityTypeManager->getStorage($bundle_entity_type->id());
      if (NULL !== ($bundle = $this->configuration['bundle'])) {
        $bundles[] = $bundle;
        if ($bundle_config = $bundle_storage->load($bundle)) {
          $dependencies[$bundle_config->getConfigDependencyKey()][] = $bundle_config->getConfigDependencyName();
        }
      }
      else {
        $bundles = array_merge($bundles, array_values($bundle_storage->getQuery()->accessCheck(FALSE)->execute()));
      }
    }

    if ($this->moduleHandler->moduleExists('field')) {
      $field_config_storage = $this->entityTypeManager->getStorage('field_config');
      $selected_fields = $this->configuration['selected'] ?? [];
      foreach ($bundles as $bundle) {
        $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type->id(), $bundle);
        foreach ($selected_fields as $field_name) {
          if (isset($field_definitions[$field_name]) && ($field_config = $field_config_storage->load($entity_type->id() . '.' . $bundle . '.' . $field_name))) {
            $dependencies[$field_config->getConfigDependencyKey()][] = $field_config->getConfigDependencyName();
          }
        }
      }
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = NULL !== $entity_type->getBundleEntityType() ? $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType()) : NULL;

    if ($bundle_entity_type) {
      $bundle_storage = $this->entityTypeManager->getStorage($bundle_entity_type->id());
      $bundle_query_result = $bundle_storage->getQuery()->accessCheck(TRUE)->range(0, 1)->execute();
      $bundle_config = NULL;
      if ($bundle_query_result) {
        $id = reset($bundle_query_result);

        if ($bundle_config = $bundle_storage->load($id)) {
          return $bundle_config->access('view', $account, $return_as_object);
        }
      }

      $result = AccessResult::forbidden("No accessible bundle config was found.");
    }
    elseif (FALSE !== ($admin_permission = $entity_type->getAdminPermission())) {
      $account = $account ?? \Drupal::currentUser();
      $result = AccessResult::allowedIf($account->hasPermission($admin_permission))
        ->addCacheContexts(['user.permissions']);
    }
    else {
      $result = AccessResult::forbidden("No administrative permission defined for the entity type.");
    }

    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    if ($name === 'selected') {
      return $this;
    }

    if ($name === 'bundle') {
      $bundle = $this->configuration['bundle'];
      return NULL == $bundle ? NULL : $this->parameterManager->createInstance('string', ['value' => (string) $bundle]);
    }

    if (!$this->moduleHandler->moduleExists('field')) {
      return NULL;
    }

    $selected = $this->configuration['selected'];
    if (empty($selected)) {
      return NULL;
    }

    if (!($parts = explode('.', $name))) {
      return NULL;
    }

    $return_list = !ctype_digit(strval(reset($parts)));

    if (!$return_list) {
      $index = (int) array_shift($parts);
      if (!isset($selected[$index])) {
        return NULL;
      }
      if (empty($parts)) {
        return $this->parameterManager->createInstance('string', ['value' => $selected[$index]]);
      }
    }

    $name = implode('.', $parts);

    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = NULL !== $entity_type->getBundleEntityType() ? $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType()) : NULL;
    $field_config_storage = $this->entityTypeManager->getStorage('field_config');

    $bundles = $bundle_entity_type ? (isset($this->configuration['bundle']) ? [$this->configuration['bundle']] : $this->entityTypeManager->getStorage($bundle_entity_type->id())->getQuery()->accessCheck(FALSE)->execute()) : [$entity_type->id()];

    $values = [];

    $ids = [];
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $field_config */
    foreach ($bundles as $bundle) {
      foreach (($return_list ? $selected : [$selected[$index]]) as $field_name) {
        $ids[] = $entity_type->id() . '.' . $bundle . '.' . $field_name;
      }
    }
    $ids = array_unique($ids);

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $field_config */
    foreach ($field_config_storage->loadMultiple($ids) as $field_config) {
      if ($parts) {
        $value = $field_config->get($name);
        if (NULL === $value) {
          $config_array = $field_config->toArray();
          $value = NestedArray::getValue($config_array, $parts);
        }
        if (is_array($value)) {
          $scalar_only = TRUE;
          array_walk_recursive($value, function($v) use (&$scalar_only) {
            if (!is_scalar($v) && !is_null($v)) {
              $scalar_only = FALSE;
            }
          });
          if (!$scalar_only) {
            $value = NULL;
          }
          unset($scalar_only);
        }
        if (is_scalar($value) || is_array($value)) {
          $values[] = $value;
        }
      }
      else {
        $value = $field_config->toArray();

        $scalar_only = TRUE;
        array_walk_recursive($value, function($v) use (&$scalar_only) {
          if (!is_scalar($v) && !is_null($v)) {
            $scalar_only = FALSE;
          }
        });
        if (!$scalar_only) {
          $value = NULL;
        }
        unset($scalar_only);

        if (is_scalar($value) || is_array($value)) {
          $values[] = $value;
        }
      }
    }

    if (empty($values)) {
      return NULL;
    }

    $value = $return_list ? array_unique($values) : reset($values);

    if ($value === NULL) {
      return NULL;
    }
    elseif (is_scalar($value)) {
      return $this->parameterManager->createInstance('string', ['value' => $value]);
    }
    else {
      return $this->parameterManager->createInstance('yaml', ['values' => $value]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $entity_type = $this->entityTypeManager->getDefinition($this->pluginDefinition['entity_type']);
    $bundle_entity_type = NULL !== $entity_type->getBundleEntityType() ? $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType()) : NULL;

    $bundle = $this->configuration['bundle'] ?? NULL;
    if ($bundle === '_none') {
      $bundle = NULL;
    }

    if ($bundle_entity_type && (NULL !== $bundle)) {
      $bundle_storage = $this->entityTypeManager->getStorage($bundle_entity_type->id());

      if (!$bundle_storage->load($bundle)) {
        return $this->t('The selected @type is missing: @label', [
          '@type' => $bundle_entity_type->getLabel(),
          '@label' => $bundle,
        ]);
      }
    }

    $bundles = $bundle_entity_type ? (isset($bundle) ? [$bundle] : $this->entityTypeManager->getStorage($bundle_entity_type->id())->getQuery()->accessCheck(FALSE)->execute()) : [$entity_type->id()];

    $selected = $this->configuration['selected'] ?? [];
    $field_names = array_combine($selected, $selected);
    $existing_definitions = [];
    foreach ($bundles as $bundle) {
      $existing_definitions += $this->entityFieldManager->getFieldDefinitions($entity_type->id(), $bundle);
    }

    $missing = array_diff_key($field_names, $existing_definitions);

    if (!empty($missing)) {
      return $this->t('Following fields are missing: @missing', [
        '@missing' => implode(', ', $missing),
      ]);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    return $this->configuration['selected'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $output = '';
    if (NULL !== ($bundle = $this->configuration['bundle'])) {
      $output .= 'bundle: ' . $bundle;
      $output .= '<br/>';
    }
    if ($selected = $this->configuration['selected']) {
      $num_items = count($selected);
      [$subset, $num_rest] = $num_items > 4 ? [array_slice($selected, 0, 3), ($num_items - 3)] : [$selected, 0];
      $output .= 'selected:';
      $output .= '<br/>';
      $output .= '<ul><li>' . implode('</li><li>', $subset) . '</li></ul>';
      if ($num_rest) {
        $output .= $this->t('+ @num more', ['@num' => $num_rest]);
      }
    }
    else {
      $output .= (string) $this->t('No items selected.');
    }
    return $output;
  }

}
