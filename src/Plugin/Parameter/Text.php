<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines formatted text as parameter.
 */
#[Parameter(
  id: "text",
  label: new TranslatableMarkup("Formatted text")
)]
class Text extends ParameterBase implements DependentPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\parameters\Plugin\Parameter\FormattedText $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setModuleHandler($container->get('module_handler'));
    $instance->setRenderer($container->get('renderer'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text value'),
      '#default_value' => $this->configuration['text']['value'] ?? '',
      '#required' => TRUE,
      '#weight' => 10,
    ];
    if ($this->moduleHandler->moduleExists('filter')) {
      $form['text']['#type'] = 'text_format';
      $form['text']['#format'] = $this->configuration['text']['format'] ?? NULL;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('text');
    if (is_string($value)) {
      $this->configuration['text'] = [];
      $this->configuration['text']['value'] = $value;
      $this->configuration['text']['format'] = NULL;
    }
    else {
      $this->configuration['text'] = [];
      $this->configuration['text']['value'] = $value['value'];
      $this->configuration['text']['format'] = $value['format'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'text' => [
        'value' => '',
        'format' => function_exists('filter_default_format') ? filter_default_format() : NULL,
      ],
    ];
  }

  /**
   * Set the module handler.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function setModuleHandler(ModuleHandlerInterface $module_handler): void {
    $this->moduleHandler = $module_handler;
  }

  /**
   * Set the renderer.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function setRenderer(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];
    if ($this->moduleHandler->moduleExists('filter')) {
      $dependencies['module'][] = 'filter';
    }
    if (!empty($this->configuration['text']['format']) && ($format = \Drupal::entityTypeManager()->getStorage('filter_format')->load($this->configuration['text']['format']))) {
      $dependencies[$format->getConfigDependencyKey()][] = $format->getConfigDependencyName();
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    $value = $this->configuration['text']['value'] ?? '';
    $format = $this->configuration['text']['format'] ?? NULL;
    if (!isset($format) || !$this->moduleHandler->moduleExists('filter')) {
      $build = ['#markup' => $value];
    }
    else {
      $build = [
        '#type' => 'processed_text',
        '#text' => $value,
        '#format' => $format,
      ];
    }
    return trim((string) $this->renderer->renderInIsolation($build));
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $preview = (string) ($this->configuration['text']['value'] ?? '');
    if (mb_strlen($preview) > 100) {
      $preview = mb_substr($preview, 0, 100) . '...';
    }
    return (string) Html::escape($preview);
  }

}
