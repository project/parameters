<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\Plugin\DataType\Any;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\parameters\Plugin\ParameterInterface;

/**
 * Null object as parameter.
 *
 * This object is only used on API level and is not meant for configuration.
 */
class NullObject extends PluginBase implements ParameterInterface {

  /**
   * Constructs a null object.
   *
   * @param string $name
   *   The parameter machine name.
   */
  public function __construct(string $name) {
    $this->configuration = ['name' => $name, 'type' => 'null'];
    $this->pluginId = 'null';
    $this->pluginDefinition = ['id' => 'null'];
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    return TRUE;
  }

  public function getPreview(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessedData(): TypedDataInterface {
    $data = Any::createInstance(DataDefinition::createFromDataType('any'));
    $data->setValue(NULL, FALSE);
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->configuration['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return 'null';
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartySetting($provider, $key, $default = NULL) {
    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartySettings($provider) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setThirdPartySetting($provider, $key, $value) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetThirdPartySetting($provider, $key) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartyProviders() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getUsageHelp(EntityInterface $entity): array {
    return [];
  }

}
