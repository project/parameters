<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Yaml as SerializationYaml;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

/**
 * Defines a YAML-formatted parameter.
 */
#[Parameter(
  id: "yaml",
  label: new TranslatableMarkup("Nested YAML")
)]
class Yaml extends ParameterBase implements PropertyParameterInterface {

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\parameters\Plugin\Parameter\Yaml $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['values' => NULL];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['values'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Values'),
      '#description' => $this->t('Define key-value pairs in YAML format. Example: <em>mykey: "my_string_value"</em>'),
      '#default_value' => SerializationYaml::encode($this->configuration['values'] ?? []),
      '#required' => TRUE,
      '#weight' => 10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('values', '');
    $parser = new Parser();
    try {
      $parser->parse($values);
    }
    catch (ParseException $e) {
      $form_state->setError($form['values'], $this->t('The provided values are no valid YAML format. The error is: @error', [
        '@error' => $e->getMessage(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['values'] = (new Parser())->parse($form_state->getValue('values', ''));
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    if (!isset($this->configuration['values'])) {
      return $this->t('No decoded values are given.');
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    $this->dataType = is_scalar($this->configuration['values']) ? 'string' : 'map';
    return $this->configuration['values'];
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    if (!is_array($this->configuration['values']) && !($this->configuration['values'] instanceof \ArrayAccess)) {
      return NULL;
    }
    $value = NestedArray::getValue($this->configuration['values'], explode('.', $name));
    if ($value === NULL) {
      return NULL;
    }
    elseif (is_scalar($value)) {
      return $this->parameterManager->createInstance('string', ['value' => $value]);
    }
    else {
      return $this->parameterManager->createInstance('yaml', ['values' => $value]);
    }
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    return (string) nl2br(Html::escape(mb_substr(SerializationYaml::encode($this->configuration['values'] ?? []), 0, 100)));
  }

}
