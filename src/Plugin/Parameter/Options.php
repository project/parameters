<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\FocusFirstCommand;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines options as parameter.
 */
#[Parameter(
  id: "options",
  label: new TranslatableMarkup("Options")
)]
class Options extends ParameterBase implements PropertyParameterInterface {

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'map';

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'options' => [],
      'default' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    if (!array_key_exists('options', $form_state->getStorage())) {
      $form_state->set('options', $configuration['options'] ?? []);
    }

    $options = $form_state->getStorage()['options'];

    if (!$form_state->get('items_count')) {
      $form_state->set('items_count', count($options));
    }

    $wrapper_id = Html::getUniqueId('parameter-options-wrapper');
    $form['options'] = [
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      '#weight' => 10,
    ];
    $form['options']['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Options'),
        $this->t('Delete'),
        $this->t('Weight'),
      ],
      '#attributes' => [
        'id' => 'parameter-options-order',
        'data-field-list-table' => TRUE,
        'class' => ['allowed-values-table'],
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],
      '#attached' => [
        'library' => [
          'core/drupal.fieldListKeyboardNavigation',
        ],
      ],
    ];

    $max = $form_state->get('items_count');
    $current_keys = array_keys($options);
    for ($delta = 0; $delta <= $max; $delta++) {
      $form['options']['table'][$delta] = [
        '#attributes' => [
          'class' => ['draggable'],
        ],
        '#weight' => $delta,
      ];
      $form['options']['table'][$delta]['item'] = [
        'label' => [
          '#type' => 'textfield',
          '#title' => $this->t('Label'),
          '#weight' => -30,
          '#default_value' => isset($current_keys[$delta]) ? $options[$current_keys[$delta]] : '',
          '#required' => FALSE,
          '#parents' => array_merge($form['#parents'], ['options', $delta, 'label']),
        ],
        'key' => [
          '#type' => 'machine_name',
          '#maxlength' => 255,
          '#title' => $this->t('Key'),
          '#description' => '',
          '#default_value' => $current_keys[$delta] ?? '',
          '#required' => FALSE,
          '#weight' => -20,
          '#machine_name' => [
            'source' => array_merge($form['#parents'], ['options', $delta, 'label']),
            'exists' => [static::class, 'keyExistsCallback'],
          ],
          '#process' => array_merge(
            [[static::class, 'processKeyMachineName']],
            \Drupal::service('plugin.manager.element_info')->getInfoProperty('machine_name', '#process', []),
          ),
          '#element_validate' => [],
          '#parents' => array_merge($form['#parents'], ['options', $delta, 'key']),
        ],
      ];
      $form['options']['table'][$delta]['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => "remove_row_button__$delta",
        '#id' => "remove_row_button__$delta",
        '#delta' => $delta,
        '#submit' => [[static::class, 'deleteSubmit']],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => [static::class, 'deleteAjax'],
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ];
      $form['options']['table'][$delta]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for row @number', ['@number' => $delta + 1]),
        '#title_display' => 'invisible',
        '#delta' => 50,
        '#default_value' => 0,
        '#attributes' => ['class' => ['weight']],
        '#parents' => array_merge($form['#parents'], ['options', $delta, 'weight']),
      ];
      // Disable the remove button if there is only one row in the table.
      if ($max === 0) {
        $form['options']['table'][0]['delete']['#attributes']['disabled'] = 'disabled';
      }
    }
    $form['options']['table']['#max_delta'] = $max;
    $form['options']['add_more_options'] = [
      '#type' => 'submit',
      '#name' => 'add_more_options',
      '#value' => $this->t('Add another item'),
      '#attributes' => [
        'class' => ['field-add-more-submit'],
        'data-field-list-button' => TRUE,
      ],
      // Allow users to add another row without requiring existing rows to have
      // values.
      '#limit_validation_errors' => [],
      '#submit' => [[static::class, 'addMoreSubmit']],
      '#ajax' => [
        'callback' => [static::class, 'addMoreAjax'],
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Adding a new item...'),
        ],
      ],
    ];

    $form['default'] = [
      '#type' => 'select',
      '#title' => $this->t('Default option'),
      '#description' => $this->t('You may need to save and reload this configuration to see all available options. This may be improved in the future.'),
      '#options' => ['_none' => $this->t('- None -')] + $options,
      '#empty_value' => '_none',
      '#default_value' => $this->configuration['default'] ?? NULL,
      '#weight' => 20,
    ];

    return $form;
  }

  /**
   * Sets the machine name source to be the label.
   */
  public static function processKeyMachineName(array &$element): array {
    $parents = $element['#array_parents'];
    array_pop($parents);
    $parents[] = 'label';
    $element['#machine_name']['source'] = $parents;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $options = [];

    $weights = [];
    foreach ($form_state->getValue('options', []) as $input_option) {
      if (($input_option['key'] ?? '') === '' || ($input_option['label'] ?? '') === '') {
        continue;
      }
      $weights[$input_option['key']] = $input_option['weight'];
      $options[$input_option['key']] = $input_option['label'];
    }

    uksort($options, fn ($a, $b) => strnatcmp($weights[$b], $weights[$a]));

    $this->configuration['options'] = $options;

    $default = $form_state->getValue('default', '_none');
    if ($default === '_none' || !isset($options[$default])) {
      $default = NULL;
    }

    $this->configuration['default'] = $default;
  }

  /**
   * Adds a new option.
   *
   * @param array $form
   *   The form array to add elements to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function addMoreSubmit(array $form, FormStateInterface $form_state) {
    $form_state->set('items_count', $form_state->get('items_count') + 1);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback for the "Add another item" button.
   */
  public static function addMoreAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form.
    $form = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));
    $delta = $form['table']['#max_delta'];
    $form['table'][$delta]['item']['#prefix'] = '<div class="ajax-new-content" data-drupal-selector="field-list-add-more-focus-target">' . ($form['table'][$delta]['item']['#prefix'] ?? '');
    $form['table'][$delta]['item']['#suffix'] = ($form['table'][$delta]['item']['#suffix'] ?? '') . '</div>';
    // Enable the remove button for the first row if there are more rows.
    if ($delta > 0 && isset($form['table'][0]['delete']['#attributes']['disabled']) && !isset($form['table'][0]['item']['key']['#attributes']['disabled'])) {
      unset($form['table'][0]['delete']['#attributes']['disabled']);
    }

    $response = new AjaxResponse();
    $response->addCommand(new InsertCommand(NULL, $form));
    $response->addCommand(new FocusFirstCommand('[data-drupal-selector="field-list-add-more-focus-target"]'));

    return $response;
  }

  /**
   * Deletes a row/option.
   *
   * @param array $form
   *   The form array to add elements to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function deleteSubmit(array $form, FormStateInterface $form_state) {
    $options = $form_state->getStorage()['options'];
    $button = $form_state->getTriggeringElement();
    $form = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));
    $item_to_be_removed = $form['item']['label']['#default_value'];
    $remaining_options = array_diff($options, [$item_to_be_removed]);
    $form_state->set('options', $remaining_options);

    // The user input is directly modified to preserve the rest of the data on
    // the page as it cannot be rebuilt from a fresh form state.
    $user_input = $form_state->getUserInput();
    NestedArray::unsetValue($user_input, $form['#parents']);
    NestedArray::unsetValue($user_input, ['options', end($form['#parents'])]);

    // Reset the keys in the array.
    $table_parents = $form['#parents'];
    array_pop($table_parents);
    $new_values = array_values(NestedArray::getValue($user_input, $table_parents));
    NestedArray::setValue($user_input, $table_parents, $new_values);
    $new_values = array_values(NestedArray::getValue($user_input, ['options']));
    NestedArray::setValue($user_input, ['options'], $new_values);

    $form_state->setUserInput($user_input);
    $form_state->set('items_count', $form_state->get('items_count') - 1);

    $form_state->setRebuild();
  }

  /**
   * Ajax callback for per row delete button.
   */
  public static function deleteAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    return NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));
  }

  /**
   * Checks for existing keys.
   */
  public static function keyExistsCallback(): bool {
    // Without access to the current form state, we cannot know if a given key
    // is in use. Return FALSE in all cases.
    return FALSE;
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    if (isset($this->configuration['options'][$name])) {
      $option = $this->getProcessedData()->getValue()[$name];
      return $this->parameterManager->createInstance('string', ['value' => $option]);
    }

    if ($name === '_options') {
      return $this;
    }

    if (($name === '_default') && (($this->configuration['default'] ?? NULL) !== NULL)) {
      return $this->parameterManager->createInstance('string', ['value' => $this->configuration['default']]);
    }

    if (($name === '_default_label') && (($this->configuration['default_label'] ?? NULL) !== NULL)) {
      return $this->parameterManager->createInstance('string', ['value' => (string) new TranslatableMarkup($this->configuration['options'][$this->configuration['default']])]);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    $options = $this->configuration['options'] ?? [];
    foreach ($options as $name => $label) {
      $options[$name] = new TranslatableMarkup($label);
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $output = '';
    if ($options = $this->getProcessedData()->getValue()) {
      $num_items = count($options);
      [$subset, $num_rest] = $num_items > 4 ? [array_slice($options, 0, 3), ($num_items - 3)] : [$options, 0];
      array_walk($subset, fn(&$v, $k) => $v = "$k: $v");
      $output .= '_options:';
      $output .= '<br/>';
      $output .= '<ul><li>' . implode('</li><li>', $subset) . '</li></ul>';
      if ($num_rest) {
        $output .= $this->t('+ @num more', ['@num' => $num_rest]);
      }
      if (NULL !== ($default = ($this->configuration['default'] ?? NULL))) {
        $output .= '<br/>' . $this->t('_default: @default', ['@default' => $default]);
        $output .= '<br/>' . $this->t('_default_label: @default', ['@default' => $options[$default]]);
      }
    }
    else {
      $output .= (string) $this->t('No options defined.');
    }
    return $output;
  }

}
