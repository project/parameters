<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an incrementing integer.
 */
#[Parameter(
  id: "increment",
  label: new TranslatableMarkup("Incrementing integer")
)]
class Increment extends ParameterBase {

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'integer';

  /**
   * The lock service.
   *
   * var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\parameters\Plugin\Parameter\Increment $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setLock($container->get('lock'));
    $instance->setState($container->get('state'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['ikey' => '', 'min_value' => 0];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['ikey'] = [
      '#type' => 'hidden',
      '#value' => $this->configuration['ikey'] ?? '',
    ];
    $increment_exists = FALSE;
    if ($this->configuration['ikey']) {
      $ikey = 'parameters:increment:' . $this->configuration['ikey'];
      if ($this->state->get($ikey, NULL) !== NULL) {
        $increment_exists = TRUE;
      }
    }
    $form['min_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum value'),
      '#default_value' => $this->configuration['min_value'] ?? 0,
      '#required' => TRUE,
      '#disabled' => $increment_exists,
      '#weight' => 10,
    ];
    if ($increment_exists) {
      $form['min_value']['#description'] = $this->t('This setting cannot be changed, because the increment is already in use.');
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $this->submitConfigurationForm($form, $form_state);
    if (($result = $this->validate()) !== TRUE) {
      $form_state->setError($form, $result);
    }
    $this->setConfiguration($config);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['ikey'] = $form_state->getValue('ikey', '');
    if (!$this->configuration['ikey']) {
      $this->configuration['ikey'] = uniqid($this->getName() . '_');
    }
    $this->configuration['min_value'] = (int) $form_state->getValue('min_value');
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataValue() {
    $ikey = 'parameters:increment:' . $this->configuration['ikey'];
    $waiting_time = 0.0;
    while (!($acquired = $this->lock->acquire($ikey)) && ($waiting_time <= 25.0)) {
      usleep(250000);
      $waiting_time += 0.25;
    }
    if (!$acquired) {
      $error = sprintf("Failed to increment an integer value. Parameter name: %s, Incrementing key: %s", $this->getName(), $ikey);
      \Drupal::logger('parameters')->error($error);
      throw new \RuntimeException($error);
    }
    $value = $this->state->get($ikey, NULL);
    if ($value === NULL) {
      $value = $this->configuration['min_value'];
      $this->state->set($ikey, $value);
    }
    else {
      $this->state->set($ikey, ++$value);
    }
    $this->lock->release($ikey);
    return (int) $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $ikey = 'parameters:increment:' . $this->configuration['ikey'];
    return $this->t('Current increment is at <strong>@value</strong>.', [
      '@value' => (string) $this->state->get($ikey, $this->configuration['min_value']),
    ]);
  }

  /**
   * Set the lock service.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   */
  public function setLock(LockBackendInterface $lock): void {
    $this->lock = $lock;
  }

  /**
   * Set the state service.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function setState(StateInterface $state): void {
    $this->state = $state;
  }

}
