<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;

/**
 * Defines an integer as parameter.
 */
#[Parameter(
  id: "integer",
  label: new TranslatableMarkup("Integer")
)]
class Integer extends ParameterBase {

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'integer';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['value']['#type'] = 'number';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['value'] = (int) $form_state->getValue('value');
  }

}
