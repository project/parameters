<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\Utility\Token;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Entity\EntitySynchronization;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Entity\ParametersCollectionStorage;
use Drupal\parameters\Internals\NormalizerContainer;
use Drupal\parameters\Plugin\Derivative\Parameter\ContentDeriver;
use Drupal\parameters\Plugin\EntityParameterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\parameters\Plugin\ParameterBase;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\PropertyParameterInterface;
use Drupal\parameters\Plugin\UsageParameterInterface;
use Symfony\Component\Serializer\Serializer;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Defines content as parameter.
 */
#[Parameter(
  id: "content",
  label: new TranslatableMarkup("Content"),
  weight: 100,
  deriver: ContentDeriver::class
)]
class Content extends ParameterBase implements EntityParameterInterface, PropertyParameterInterface, UsageParameterInterface, DependentPluginInterface, AccessibleInterface, CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected string $dataType = 'entity';

  /**
   * The entity form display to use for configuring the content entity.
   *
   * @var string
   */
  protected string $entityFormDisplay = 'parameters';

  /**
   * The configured content entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected ContentEntityInterface $configuredContentEntity;

  /**
   * The form object used for configuring the content entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityFormInterface|null
   */
  protected ?ContentEntityFormInterface $entityForm;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected Serializer $serializer;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $token;

  /**
   * The lock service.
   *
   * var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The entity synchronization service.
   *
   * @var \Drupal\parameters\Entity\EntitySynchronization
   */
  protected EntitySynchronization $entitySynchronization;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\parameters\Plugin\Parameter\Content $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setFormBuilder($container->get('form_builder'));
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setSerializer($container->get('parameters.serializer'));
    $instance->setEntityRepository($container->get('entity.repository'));
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    $instance->setToken($container->get('token'));
    $instance->setLock($container->get('lock'));
    $instance->setEntitySynchronization($container->get('parameters.entity_synchronization'));
    $instance->setLanguageManager($container->get('language_manager'));

    if (isset($plugin_definition['entity_type'], $plugin_definition['bundle'])) {
      $entity_type_id = $plugin_definition['entity_type'];
      $bundle = $plugin_definition['bundle'];
    }
    else {
      [, $derivative_id] = explode(':', $plugin_id, 2);
      [$entity_type_id, $bundle] = explode('.', $derivative_id, 2);
    }
    $entity_type = $instance->entityTypeManager->getDefinition($entity_type_id);
    $instance->dataType = $entity_type->hasKey('bundle') ? 'entity:' . $entity_type_id . ':' . $bundle : 'entity:' . $entity_type_id;

    if (empty($instance->configuration['values'])) {
      $instance->configuration += $instance->defaultConfiguration();
    }
    $instance->initConfiguredContentEntity();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $plugin_definition = $this->getPluginDefinition();
    if (isset($plugin_definition['entity_type'], $plugin_definition['bundle'])) {
      $entity_type_id = $plugin_definition['entity_type'];
      $bundle = $plugin_definition['bundle'];
    }
    else {
      [$entity_type_id, $bundle] = explode('.', $this->getDerivativeId(), 2);
    }
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if ($entity_type->hasKey('uuid') && !isset($this->configuration['values'][$entity_type->getKey('uuid')])) {
      return $this->t('A uuid must be set.');
    }
    if ($entity_type->hasKey('bundle') && !isset($this->configuration['values'][$entity_type->getKey('bundle')])) {
      return $this->t('The bundle must be set.');
    }
    return parent::validate();
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessedData(): TypedDataInterface {
    if (!isset($this->processedData)) {
      $this->processedData = $this->typedDataManager->createInstance($this->dataType, [
        'data_definition' => $this->typedDataManager->createDataDefinition($this->dataType),
        'name' => NULL,
        'parent' => NULL,
      ]);
      $this->processedData->setValue($this->processDataValue());
    }
    return $this->processedData;
  }

  /**
   * Processes the data value to be returned via ::getProcessedData().
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The data value. This value must be compatible with the typed data type
   *   that is defined via ::$dataType, because it will be set as value for
   *   an instance of that typed data type.
   */
  protected function processDataValue() {
    $entity = $this->getConfiguredContentEntity();

    // Save the entity, if not yet available.
    if ($entity->isNew()) {
      $sync_enabled = $this->entitySynchronization->isEnabled();
      $this->entitySynchronization->setEnabled(FALSE);
      $entity = $this->saveEntity($entity, TRUE);
      $this->entitySynchronization->setEnabled($sync_enabled);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreview(): string {
    $entity = $this->getConfiguredContentEntity();
    $label = (string) Html::escape($entity->label());
    return !$entity->isNew() && $entity->hasLinkTemplate('canonical') && $entity->access('view') ? (string) $entity->toLink($label, 'canonical')->toString() : $label;
  }

  /**
   * Set the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    if ($plugin_definition = $this->getPluginDefinition()) {
      $values = [];

      if (isset($plugin_definition['entity_type'], $plugin_definition['bundle'])) {
        $entity_type_id = $plugin_definition['entity_type'];
        $bundle = $plugin_definition['bundle'];
      }
      else {
        [$entity_type_id, $bundle] = explode('.', $this->getDerivativeId(), 2);
      }

      $entity_type = $this->getEntityTypeManager()->getDefinition($entity_type_id);
      if ($entity_type->hasKey('bundle')) {
        $values[$entity_type->getKey('bundle')] = $bundle;
      }

      return ['values' => $values];
    }
    return [];
  }

  /**
   * Returns a JSON-formatted string representation of the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to format as JSON.
   *
   * @return string
   *   The JSON string.
   */
  public function toJson(ContentEntityInterface $entity): string {
    return $this->getSerializer()->serialize($entity, 'json', ['json_encode_options' => JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES]);
  }

  /**
   * Converts the given JSON-formatted string to an entity object.
   *
   * @param string $json
   *   The JSON-formatted string.
   * @param string $entity_class
   *   The entity class to expect.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  public function fromJson(string $json, string $entity_class): ContentEntityInterface {
    return $this->getSerializer()->deserialize($json, $entity_class, 'json');
  }

  /**
   * Get the serializer.
   *
   * @return \Symfony\Component\Serializer\Serializer
   *   The serializer.
   */
  public function getSerializer(): Serializer {
    if (!isset($this->serializer)) {
      $this->serializer = \Drupal::service('parameters.serializer');
    }
    return $this->serializer;
  }

  /**
   * Set the serializer.
   *
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer.
   */
  public function setSerializer(Serializer $serializer): void {
    $this->serializer = $serializer;
  }

  /**
   * Returns a normalized array of the given entity, suitable for configuration.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to normalize.
   *
   * @return array
   *   The normalized array.
   */
  public function toConfigArray(ContentEntityInterface $entity): array {
    $normalizer_container = NormalizerContainer::get();
    $normalizer_container->contentEntityNormalizer()::$cleanupFieldValues = TRUE;
    $normalizer_container->entityReferenceItemNormalizer()::$normalizeNewEntities = TRUE;
    try {
      return $this->getSerializer()->normalize($entity, get_class($entity));
    }
    finally {
      $normalizer_container->contentEntityNormalizer()::$cleanupFieldValues = FALSE;
      $normalizer_container->entityReferenceItemNormalizer()::$normalizeNewEntities = FALSE;
    }
  }

  /**
   * Converts the given normalized config array to an entity object.
   *
   * @param array $array
   *   The normalized array.
   * @param string $entity_class
   *   The entity class to expect.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity.
   */
  public function fromConfigArray(array $array, string $entity_class): ContentEntityInterface {
    $normalizer_container = NormalizerContainer::get();
    $normalizer_container->contentEntityNormalizer()::$cleanupFieldValues = TRUE;
    $normalizer_container->entityReferenceItemNormalizer()::$normalizeNewEntities = TRUE;
    try {
      return $this->getSerializer()->denormalize($array, $entity_class);
    }
    finally {
      $normalizer_container->contentEntityNormalizer()::$cleanupFieldValues = FALSE;
      $normalizer_container->entityReferenceItemNormalizer()::$normalizeNewEntities = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['entity_form_info'] = [
      '#type' => 'container',
      'display_mode' => [
        '#markup' => $this->t('This configuration is using the "@mode" form display mode. <a href=":url" target="_blank">Manage form display modes</a>.', [
          '@mode' => $this->entityFormDisplay,
          ':url' => '/admin/structure/display-modes/form',
        ]),
        '#weight' => 10,
      ],
      '#weight' => -50,
    ];
    // We need to use a process callback for embedding the entity fields,
    // because the fields to embed need to know their "#parents".
    $wrapper_id = Html::getUniqueId('entity-content-fields');
    $entity = $this->getConfiguredContentEntity();
    if ($entity->isNew() && ($entity instanceof TranslatableInterface)) {
      $langcode = $form_state->get('langcode');
      if ($entity->language()->getId() !== $langcode) {
        if ($entity->language()->getId() === LanguageInterface::LANGCODE_NOT_SPECIFIED) {
          if ($entity->getEntityType()->hasKey('langcode') && $entity->getEntityType()->getKey('langcode')) {
            $entity->set($entity->getEntityType()->getKey('langcode'), $langcode);
          }
        }
        else {
          $entity = $entity->hasTranslation($langcode) ? $entity->getTranslation($langcode) : $entity->addTranslation($langcode);
        }
      }
    }
    $form['values'] = [
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      '#wrapper_id' => $wrapper_id,
      '#parameters__entity' => $entity,
      '#parameters__form_display' => $this->entityFormDisplay,
      '#process' => [[static::class, 'processContentConfigurationForm']],
      '#parents' => ['values'],
      '#tree' => TRUE,
    ];
    return $form;
  }

  /**
   * Process callback to insert the content entity form.
   *
   * @param array $element
   *   The containing element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The containing element, with the content entity form inserted.
   */
  public static function processContentConfigurationForm(array $element, FormStateInterface $form_state) {
    $entity = $element['#parameters__entity'];
    $form_display_mode = $element['#parameters__form_display'];
    $wrapper_id = $element['#wrapper_id'];
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, $form_display_mode);

    // A very special treatment for the moderation state field that is coming
    // from core's content_moderation module. We need to wrap the according
    // widget with a decorator that prevents that widget from manipulating
    // entity values. This happens when the configured moderation state differs
    // from the initial state.
    if ($form_display->getComponent('moderation_state')) {
      $closure = \Closure::fromCallable(function () {
        /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $this */
        $this->getRenderer('moderation_state');
        $workaround_class = 'Drupal\flow\Workaround\ModerationStateWidgetWorkaround';
        $this->plugins['moderation_state'] = new $workaround_class($this->plugins['moderation_state']);
      });
      $closure->call($form_display);
    }

    $content_config_entities = $form_state->get('parameters__content_configuration') ?? [];
    $content_config_entities[$wrapper_id] = [$entity, $form_display];
    $form_state->set('parameters__content_configuration', $content_config_entities);
    $form_display->buildForm($entity, $element, $form_state);
    if ($entity->getEntityTypeId() === 'user') {
      self::processUserAccountForm($element, $form_state);
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->hasValue(['values']) || !isset($form['values']['#wrapper_id'])) {
      return;
    }
    $wrapper_id = $form['values']['#wrapper_id'];
    $content_config_entities = $form_state->get('parameters__content_configuration') ?? [];
    if (!isset($content_config_entities[$wrapper_id])) {
      return;
    }

    $complete_form_state = $form_state instanceof SubformStateInterface ? $form_state->getCompleteFormState() : $form_state;
    [$entity, $form_display] = $content_config_entities[$wrapper_id];
    $extracted = $form_display->extractFormValues($entity, $form['values'], $complete_form_state);
    // Extract the values of fields that are not rendered through widgets, by
    // simply copying from top-level form values. This leaves the fields that
    // are not being edited within this form untouched.
    // @see \Drupal\Tests\field\Functional\NestedFormTest::testNestedEntityFormEntityLevelValidation()
    foreach ($form_state->getValue(['values']) as $name => $values) {
      if ($entity->hasField($name) && !isset($extracted[$name])) {
        $entity->set($name, $values);
      }
    }
    $form_display->validateFormValues($entity, $form['values'], $complete_form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->hasValue(['values']) || !isset($form['values']['#wrapper_id'])) {
      return;
    }
    $wrapper_id = $form['values']['#wrapper_id'];
    $content_config_entities = $form_state->get('parameters__content_configuration') ?? [];
    if (!isset($content_config_entities[$wrapper_id])) {
      return;
    }

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    [$entity] = $content_config_entities[$wrapper_id];

    $form_display = EntityFormDisplay::collectRenderDisplay($entity, $this->entityFormDisplay, TRUE);
    $complete_form_state = $form_state instanceof SubformStateInterface ? $form_state->getCompleteFormState() : $form_state;
    $extracted = $form_display->extractFormValues($entity, $form['values'], $complete_form_state);
    foreach ($form_state->getValue(['values']) as $name => $values) {
      if ($entity->hasField($name) && !isset($extracted[$name])) {
        $entity->set($name, $values);
      }
    }

    // Filter field values that are not available on the form display mode.
    $entity_type = $entity->getEntityType();
    $entity_keys = $entity_type->getKeys();
    $components = $form_display->getComponents();
    foreach (array_keys($values) as $k_1) {
      if (!isset($components[$k_1]) && !in_array($k_1, $entity_keys)) {
        unset($values[$k_1]);
      }
    }

    if ($entity->getEntityTypeId() === 'user') {
      self::submitUserAccountForm($form['values'], SubformState::createForSubform($form['values'], $form, $form_state));
    }

    $this->setConfiguredContentEntity($entity);
    $values = $this->toConfigArray($entity);

    if ($entity instanceof UserInterface) {
      // Include the (hashed) password as configuration.
      if ($form_state->hasValue(['values', 'pass'])) {
        // Must save the entity in order to get the hashed password.
        $sync_enabled = $this->entitySynchronization->isEnabled();
        $this->entitySynchronization->setEnabled(FALSE);
        $entity->save();
        $this->entitySynchronization->setEnabled($sync_enabled);
      }
      $values['pass'] = [[
        'value' => $entity->getPassword(),
        'pre_hashed' => TRUE,
      ]];
    }

    $this->configuration['values'] = $values;
  }

  /**
   * Instantiates the configured content entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The initialized entity.
   */
  public function initConfiguredContentEntity(): ContentEntityInterface {
    if (isset($plugin_definition['entity_type'], $plugin_definition['bundle'])) {
      $entity_type_id = $plugin_definition['entity_type'];
      $bundle = $plugin_definition['bundle'];
    }
    else {
      [$entity_type_id, $bundle] = explode('.', $this->getDerivativeId(), 2);
    }
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

    $values = $this->configuration['values'];
    $uuid = $entity_type->hasKey('uuid') && isset($values[$entity_type->getKey('uuid')]) ? $values[$entity_type->getKey('uuid')] : NULL;
    $langcode = $entity_type->hasKey('langcode') && $entity_type->getKey('langcode') && isset($values[$entity_type->getKey('langcode')]) ? $values[$entity_type->getKey('langcode')] : NULL;
    while (is_array($uuid)) {
      $uuid = reset($uuid);
    }
    while (is_array($langcode)) {
      $langcode = reset($langcode);
    }

    $entity = NULL;
    if ($uuid && ($entity = $this->entityRepository->loadEntityByUuid($entity_type_id, $uuid))) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      if ($langcode && ($entity->language()->getId() !== $langcode) && ($entity instanceof TranslatableInterface)) {
        if ($entity->language()->getId() === LanguageInterface::LANGCODE_NOT_SPECIFIED) {
          if ($entity_type->hasKey('langcode') && $entity_type->getKey('langcode')) {
            $entity->set($entity_type->getKey('langcode'), $langcode);
          }
        }
        else {
          $entity = $entity->hasTranslation($langcode) ? $entity->getTranslation($langcode) : $entity->addTranslation($langcode, $this->fromConfigArray($values, $entity_type->getClass())->toArray());
        }
      }
    }
    if (!$entity) {
      $entity = $this->fromConfigArray($values, $entity_type->getClass());
    }

    $this->setConfiguredContentEntity($entity);
    return $this->configuredContentEntity;
  }

  /**
   * Get the configured content entity object.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The initialized entity.
   */
  public function getConfiguredContentEntity(): ContentEntityInterface {
    if (!isset($this->configuredContentEntity)) {
      $this->initConfiguredContentEntity();
    }
    return $this->configuredContentEntity;
  }

  /**
   * Set the configured content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to set as configured content entity.
   */
  public function setConfiguredContentEntity(ContentEntityInterface $entity): void {
    $this->configuredContentEntity = $entity;
  }

  /**
   * Process callback to insert elements of the user account form.
   *
   * @param array &$form
   *   The form element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function processUserAccountForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\user\UserInterface $account */
    $account = $form['#parameters__entity'];

    $form['mail'] = [
      '#type' => 'email',
      '#title' => t('Email address'),
      '#required' => !(!$account->getEmail() && \Drupal::currentUser()->hasPermission('administer users')),
      '#default_value' => $account->getEmail(),
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#maxlength' => UserInterface::USERNAME_MAX_LENGTH,
      '#description' => t("Several special characters are allowed, including space, period (.), hyphen (-), apostrophe ('), underscore (_), and the @ sign."),
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['username'],
        'autocorrect' => 'off',
        'autocapitalize' => 'off',
        'spellcheck' => 'false',
      ],
      '#default_value' => $account->getAccountName(),
      '#access' => $account->name->access('edit'),
    ];

    // Display password field only for existing users or when user is allowed to
    // assign a password during registration.
    if (!$account->isNew()) {
      $form['pass'] = [
        '#type' => 'password_confirm',
        '#size' => 25,
        '#description' => t('To change the current user password, enter the new password in both fields.'),
      ];
    }
    else {
      $form['pass'] = [
        '#type' => 'password_confirm',
        '#size' => 25,
        '#description' => t('Provide a password for the new account in both fields.'),
        '#required' => TRUE,
      ];
    }

    $form['status'] = [
      '#type' => 'radios',
      '#title' => t('Status'),
      '#default_value' => (int) $account->get('status')->value,
      '#options' => [t('Blocked'), t('Active')],
    ];

    $roles = array_map(['\Drupal\Component\Utility\Html', 'escape'], _parameters_user_role_names(TRUE));

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => t('Roles'),
      '#default_value' => $account->getRoles(),
      '#options' => $roles,
    ];

    // Special handling for the inevitable "Authenticated user" role.
    $form['roles'][RoleInterface::AUTHENTICATED_ID] = [
      '#default_value' => TRUE,
      '#access' => FALSE,
    ];
  }

  /**
   * Submit callback for elements of the user account form.
   *
   * @param array &$element
   *   The element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function submitUserAccountForm(array &$form, FormStateInterface $form_state): void {
    $wrapper_id = $form['#wrapper_id'];
    $content_config_entities = $form_state->get('parameters__content_configuration') ?? [];
    if (!isset($content_config_entities[$wrapper_id])) {
      return;
    }

    /** @var \Drupal\user\UserInterface $account */
    [$account] = $content_config_entities[$wrapper_id];

    if ($form_state->hasValue('mail')) {
      $account->setEmail($form_state->getValue('mail'));
    }
    if ($form_state->hasValue('name')) {
      $account->setUsername($form_state->getValue('name'));
    }

    // Set existing password if set in the form state.
    $current_pass = trim($form_state->getValue('current_pass', ''));
    if (strlen($current_pass) > 0) {
      $account->setExistingPassword($current_pass);
    }

    if ($form_state->hasValue('pass')) {
      $account->setPassword($form_state->getValue('pass'));
    }

    if ($form_state->hasValue('roles') && is_string(key($form_state->getValue('roles')))) {
      $form_state->setValue('roles', array_keys(array_filter($form_state->getValue('roles'))));
    }

    $roles = [];
    foreach ($form_state->getValue('roles', []) as $rk => $rv) {
      if ($rv && $rv !== RoleInterface::AUTHENTICATED_ID) {
        $roles[] = $rv;
      }
    }
    $account->get('roles')->setValue($roles);
  }

  /**
   * Set the form builder.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function setFormBuilder(FormBuilderInterface $form_builder): void {
    $this->formBuilder = $form_builder;
  }

  /**
   * Get the form builder.
   *
   * @return \Drupal\Core\Form\FormBuilderInterface
   *   The form builder.
   */
  public function getFormBuilder(): FormBuilderInterface {
    if (!isset($this->formBuilder)) {
      $this->formBuilder = \Drupal::service('form_builder');
    }
    return $this->formBuilder;
  }

  /**
   * Set the entity repository.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function setEntityRepository(EntityRepositoryInterface $entity_repository): void {
    $this->entityRepository = $entity_repository;
  }

  /**
   * Get the entity repository.
   *
   * @return \Drupal\Core\Entity\EntityRepositoryInterface
   *   The entity repository.
   */
  public function getEntityRepository(): EntityRepositoryInterface {
    if (!isset($this->entityRepository)) {
      $this->entityRepository = \Drupal::service('entity.repository');
    }
    return $this->entityRepository;
  }

  /**
   * Set the parameter plugin manager.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $manager
   *   The parameter plugin manager.
   */
  public function setParameterManager(PluginManagerInterface $manager) {
    $this->parameterManager = $manager;
  }

  /**
   * Get the token service.
   *
   * @return \Drupal\Core\Utility\Token
   *   The token service.
   */
  public function getToken(): Token {
    if (!isset($this->token)) {
      $this->token = \Drupal::token();
    }
    return $this->token;
  }

  /**
   * Set the token service.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function setToken(Token $token): void {
    $this->token = $token;
  }

  /**
   * Set the lock service.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   */
  public function setLock(LockBackendInterface $lock): void {
    $this->lock = $lock;
  }

  /**
   * Set the entity synchronization service.
   *
   * @param \Drupal\parameters\Entity\EntitySynchronization $synchronization
   *   The entity synchronization service.
   */
  public function setEntitySynchronization(EntitySynchronization $synchronization): void {
    $this->entitySynchronization = $synchronization;
  }

  /**
   * Set the language manager.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function setLanguageManager(LanguageManagerInterface $language_manager): void {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $entity = $this->getConfiguredContentEntity();
    if (!$entity->isNew()) {
      return $entity->getCacheTags();
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $entity = $this->getConfiguredContentEntity();
    if (!$entity->isNew()) {
      return $entity->getCacheContexts();
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $entity = $this->getConfiguredContentEntity();
    if (!$entity->isNew()) {
      return $entity->getCacheMaxAge();
    }
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];

    $plugin_definition = $this->getPluginDefinition();
    if (isset($plugin_definition['entity_type'], $plugin_definition['bundle'])) {
      $entity_type_id = $plugin_definition['entity_type'];
      $bundle = $plugin_definition['bundle'];
    }
    else {
      [$entity_type_id, $bundle] = explode('.', $this->getDerivativeId(), 2);
    }
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!in_array($entity_type->getProvider(), ['core', 'component'])) {
      $dependencies['module'][] = $entity_type->getProvider();
    }
    if ($bundle_entity_type_id = $entity_type->getBundleEntityType()) {
      if ($bundle_config = $this->entityTypeManager->getStorage($bundle_entity_type_id)->load($bundle)) {
        $dependencies[$bundle_config->getConfigDependencyKey()][] = $bundle_config->getConfigDependencyName();
      }
    }

    $target_uuids = [];
    foreach (($this->configuration['values'] ?? []) as $field) {
      if (is_array($field)) {
        foreach ($field as $value) {
          if (is_array($value) && isset($value['target_uuid'])) {
            $target_uuids[$value['target_uuid']] = $value['target_uuid'];
          }
        }
      }
    }
    if ($target_uuids) {
      /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
      foreach ($this->entityTypeManager->getStorage('parameters_collection')->loadMultiple() as $collection) {
        foreach (($collection->get('parameters') ?? []) as $parameter_config) {
          if (isset($parameter_config['values']['uuid'][0]['value']) && in_array($parameter_config['values']['uuid'][0]['value'], $target_uuids, TRUE)) {
            if (!isset(ParametersCollectionStorage::$saveStack[$collection->id()])) {
              $dependencies[$collection->getConfigDependencyKey()][] = $collection->getConfigDependencyName();
              continue 2;
            }
          }
        }
      }
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name): ?ParameterInterface {
    $entity = $this->getProcessedData()->getValue();
    if (!($entity instanceof ContentEntityInterface)) {
      return NULL;
    }

    // Use the Token replacement system to retrieve a string value. For that,
    // the token type needs to be determined first.
    if (\Drupal::hasService('token.entity_mapper')) {
      /** @var \Drupal\token\TokenEntityMapperInterface $token_entity_mapper */
      $token_entity_mapper = \Drupal::service('token.entity_mapper');
      $token_type = $token_entity_mapper->getTokenTypeForEntityType($entity->getEntityTypeId(), TRUE);
    }
    else {
      $token_type = $entity->getEntityType()->get('token_type') ?: $entity->getEntityTypeId();
    }

    // Now build up the token and replace it with a value.
    if (strpos($name, '.')) {
      $name = str_replace('.', ':', $name);
    }
    $token = "[{$token_type}:{$name}]";
    $value = (string) $this->getToken()->replace($token, [$token_type => $entity], [
      'clear' => TRUE,
      'langcode' => $entity->language()->getId(),
    ]);
    if ($value !== '') {
      return $this->parameterManager->createInstance('string', ['value' => $value]);
    }

    // Last chance to get a field value.
    if ($entity->hasField($name) && !$entity->get($name)->isEmpty()) {
      return $this->parameterManager->createInstance('string', [
        'value' => $entity->get($name)->getString(),
      ]);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function onAddition(ParametersCollectionInterface $collection, string $operation): void {
    $this->synchronizeContent($collection);
    $this->entitySynchronization->register($this->getConfiguredContentEntity(), $collection);
  }

  /**
   * {@inheritdoc}
   */
  public function onChange(array $previous_configuration, ParametersCollectionInterface $collection, string $operation): void {
    $this->synchronizeContent($collection);
    $this->entitySynchronization->register($this->getConfiguredContentEntity(), $collection);
  }

  /**
   * {@inheritdoc}
   */
  public function onRemoval(ParametersCollectionInterface $collection, string $operation): void {
    // Do not delete the content entity. Instead, leave the content entity as
    // is, if it exists.
    $this->entitySynchronization->unregister($this->getConfiguredContentEntity(), $collection);
  }

  /**
   * Synchronizes the content entity with configured values.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface $collection
   *   The collection that holds the parameter.
   */
  public function synchronizeContent(ParametersCollectionInterface $collection): void {
    $sync_enabled = $this->entitySynchronization->isEnabled();
    $this->entitySynchronization->setEnabled(FALSE);

    $plugin_definition = $this->getPluginDefinition();
    if (isset($plugin_definition['entity_type'], $plugin_definition['bundle'])) {
      $entity_type_id = $plugin_definition['entity_type'];
    }
    else {
      [$entity_type_id] = explode('.', $this->getDerivativeId(), 2);
    }
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $id_fields = [$entity_type->getKey('id')];
    if ($entity_type->hasKey('revision')) {
      $id_fields[] = $entity_type->getKey('revision');
    }
    if ($entity_type->hasKey('langcode') && $entity_type->getKey('langcode')) {
      $id_fields[] = $entity_type->getKey('langcode');
    }
    if ($entity_type->hasKey('default_langcode')) {
      $id_fields[] = $entity_type->getKey('default_langcode');
    }

    $parameters = [$collection->language()->getId() => $this];
    if (interface_exists('Drupal\language\ConfigurableLanguageManagerInterface') && ($this->languageManager instanceof ConfigurableLanguageManagerInterface)) {
      foreach ($this->languageManager->getLanguages() as $language) {
        if (isset($parameter[$language->getId()])) {
          continue;
        }

        $override = $this->languageManager->getLanguageConfigOverride($language->getId(), $collection->getConfigDependencyName());
        $param_configs = $override->get('parameters') ?? [];
        if (isset($param_configs[$this->getName()])) {
          /** @var \Drupal\parameters\Plugin\Parameter\Content $parameter */
          $parameter = $this->parameterManager->createInstance($this->getPluginId(), $param_configs[$this->getName()]);
          if (!isset($parameters[$parameter->getConfiguredContentEntity()->language()->getId()])) {
            $parameters[$parameter->getConfiguredContentEntity()->language()->getId()] = $parameter;
          }
        }
      }
    }

    foreach ($parameters as $langcode => $parameter) {
      $configured = $parameter->fromConfigArray($parameter->configuration['values'], $entity_type->getClass());
      $loaded = $parameter->getConfiguredContentEntity();
      if (($loaded->language()->getId() !== $langcode) && ($loaded instanceof TranslatableInterface)) {
        if ($loaded->language()->getId() === LanguageInterface::LANGCODE_NOT_SPECIFIED) {
          if ($entity_type->hasKey('langcode') && $entity_type->getKey('langcode')) {
            $loaded->set($entity_type->getKey('langcode'), $langcode);
          }
        }
        else {
          $loaded = $loaded->hasTranslation($langcode) ? $loaded->getTranslation($langcode) : $loaded->addTranslation($langcode);
        }
      }
      $values_changed = FALSE;
      foreach ($loaded->getFields() as $field_name => $items) {
        if (in_array($field_name, $id_fields, TRUE)) {
          continue;
        }
        $new_value = $configured->get($field_name)->getValue();
        if ($new_value !== $items->getValue()) {
          $items->setValue($new_value);
          $values_changed = TRUE;
        }
      }
      if ($loaded->isNew() || $values_changed) {
        $entity = $parameter->saveEntity($loaded, TRUE);
      }
      else {
        $entity = $loaded;
      }
      $parameter->setConfiguredContentEntity($entity);
    }

    $this->entitySynchronization->setEnabled($sync_enabled);
  }

  /**
   * Saves the entity, if not yet done.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to save.
   * @param bool $force_save
   *   Whether to enforce a save or not. Default is FALSE (no enforcement).
   *
   * @return |Drupal\Core\Entity\ContentEntityInterface
   *   The saved entity.
   */
  protected function saveEntity(ContentEntityInterface $entity, bool $force_save = FALSE): ContentEntityInterface {
    $lock_name = $entity->getEntityType()->hasKey('uuid') ? 'parameters:content:' . $entity->uuid() : 'parameters:content:' . ($entity->id() ?? uniqid());
    $saved = FALSE;
    if ($this->lock->acquire($lock_name)) {
      $loaded = NULL;
      if (!$entity->uuid() || !($loaded = $this->entityRepository->loadEntityByUuid($entity->getEntityTypeId(), $entity->uuid()))) {
        $entity->save();
        $saved = TRUE;
      }
      if ($loaded && ($entity->language()->getId() !== $loaded->language()->getId()) && ($loaded instanceof TranslatableInterface)) {
        if ($loaded->hasTranslation($entity->language()->getId())) {
          $loaded = $loaded->getTranslation($entity->language()->getId());
        }
        elseif ($loaded->language()->getId() === LanguageInterface::LANGCODE_NOT_SPECIFIED) {
          /** @var \Drupal\Core\Entity\ContentEntityInterface $loaded */
          if ($loaded->getEntityType()->hasKey('langcode') && $loaded->getEntityType()->getKey('langcode')) {
            $loaded->set($loaded->getEntityType()->getKey('langcode'), $entity->language()->getId());
          }
        }
        else {
          $loaded = $loaded->addTranslation($entity->language()->getId(), $entity->toArray());
          $loaded->save();
          $saved = TRUE;
        }
      }
      if ($loaded) {
        $entity = $loaded;
      }
      if ($force_save && !$saved) {
        $entity->save();
        $saved = TRUE;
      }
      $this->lock->release($lock_name);
    }
    elseif ($entity->uuid()) {
      $load_by_uuid_and_langcode = function (ContentEntityInterface $entity) {
        $loaded = $this->entityRepository->loadEntityByUuid($entity->getEntityTypeId(), $entity->uuid());
        if ($loaded && ($entity->language()->getId() !== $loaded->language()->getId()) && ($loaded instanceof TranslatableInterface)) {
            $loaded = $loaded->hasTranslation($entity->language()->getId()) ? $loaded->getTranslation($entity->language()->getId()) : NULL;
        }
        return $loaded;
      };

      // Someone else is already saving it. Wait a bit and then try to get it.
      $i = 0;
      $loaded = NULL;
      while (!($loaded = $load_by_uuid_and_langcode($entity))) {
        $i++;
        usleep(10000);
        if ($i > 1000) {
          // More than 10 seconds have elapsed without any luck, abort.
          \Drupal::logger('parameters')->error(sprintf("Failed to load an entity from repository. Type: %s, UUID: %s", $entity->getEntityTypeId(), $entity->uuid()));
          break;
        }
      }
      $entity = $loaded ?? $entity;
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($operation === 'create') {
      $definition = $this->getPluginDefinition();
      $entity_type_id = $definition['entity_type'];

      // Group relationship is not working without a group context.
      // Therefore for that type this special handling is needed.
      // @todo Remove this special handling once group relationship access
      // control works without group context.
      if (($entity_type_id === 'group_relationship') || ($entity_type_id === 'group_content')) {
        $result = AccessResult::allowedIf(((string) \Drupal::currentUser()->id()) === '1');
      }
      else {
        $handler = $this->entityTypeManager->getAccessControlHandler($entity_type_id);
        $result = $handler->createAccess($definition['bundle'], $account, [
          'entity_type_id' => $entity_type_id,
          'parameters_plugin' => $this,
        ], TRUE);
      }
    }
    elseif ($entity = $this->getConfiguredContentEntity()) {
      $result = $entity->access($operation, $account, TRUE);
    }
    else {
      $result = AccessResult::neutral("No configured content entity available.");
    }
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): EntityInterface {
    return $this->getConfiguredContentEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity): EntityParameterInterface {
    $this->setConfiguredContentEntity($entity);
    // This method is usually being called from an external component.
    // To make sure the plugin reflects the same configuration,
    // its configuration values need to be updated accordingly.
    $values = $this->toConfigArray($entity);

    if ($entity instanceof UserInterface) {
      if ($entity->isNew()) {
        // Must save the entity in order to get the hashed password.
        $sync_enabled = $this->entitySynchronization->isEnabled();
        $this->entitySynchronization->setEnabled(FALSE);
        $entity->save();
        $this->entitySynchronization->setEnabled($sync_enabled);
      }
      $values['pass'] = [[
        'value' => $entity->getPassword(),
        'pre_hashed' => TRUE,
      ]];
    }

    $this->configuration['values'] = $values;
    return $this;
  }

}
