<?php

namespace Drupal\parameters\Plugin\Parameter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Attribute\Parameter;
use Drupal\parameters\Plugin\ParameterBase;

/**
 * Defines a machine name as parameter.
 */
#[Parameter(
  id: "machine_name",
  label: new TranslatableMarkup("Machine name")
)]
class MachineName extends ParameterBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['value']['#type'] = 'machine_name';
    $form['value']['#machine_name']['exists'] = [static::class, 'exists'];
    return $form;
  }

  /**
   * Exists callback for the machine name input.
   *
   * @return bool
   *   The exists result.
   */
  public static function exists(): bool {
    return FALSE;
  }

}
