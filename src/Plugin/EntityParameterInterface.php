<?php

namespace Drupal\parameters\Plugin;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for parameter plugins that provide an entity.
 */
interface EntityParameterInterface extends ParameterInterface {

  /**
   * Get the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity(): EntityInterface;

  /**
   * Set the entity.
   *
   * @return $this
   */
  public function setEntity(EntityInterface $entity): EntityParameterInterface;

}
