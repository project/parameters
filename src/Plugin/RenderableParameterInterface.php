<?php

namespace Drupal\parameters\Plugin;

use Drupal\Core\Render\RenderableInterface;

/**
 * Interface for parameter plugins that provide an output rendering mechanic.
 */
interface RenderableParameterInterface extends ParameterInterface, RenderableInterface {}
