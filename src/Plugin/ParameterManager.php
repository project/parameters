<?php

namespace Drupal\parameters\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\parameters\Attribute\Parameter;

/**
 * The manager for parameter plugins.
 */
class ParameterManager extends DefaultPluginManager {

  /**
   * The service name of this class.
   *
   * @var string
   */
  const SERVICE_NAME = 'plugin.manager.parameter';

  /**
   * Get the service instance of this class.
   *
   * @return \Drupal\parameters\Plugin\ParameterManager
   *   The service instance.
   */
  public static function get(): PluginManagerInterface {
    return \Drupal::service(self::SERVICE_NAME);
  }

  /**
   * The ParameterManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Parameter', $namespaces, $module_handler, 'Drupal\parameters\Plugin\ParameterInterface', Parameter::class, 'Drupal\parameters\Annotation\Parameter');
    $this->alterInfo('parameter_info');
    $this->setCacheBackend($cache_backend, 'parameter_plugins');
  }

  /**
   * Creates a pre-configured instance of a plugin.
   *
   * @param string $plugin_id
   *   The ID of the plugin being instantiated.
   * @param array $configuration
   *   An array of configuration relevant to the plugin instance.
   *
   * @return \Drupal\parameters\Plugin\ParameterInterface
   *   A fully configured plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the instance cannot be created, such as if the ID is invalid.
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return parent::createInstance($plugin_id, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions() {
    $definitions = parent::findDefinitions();
    uasort($definitions, function($a, $b) {
      $a_weight = $a['weight'] ?? 0;
      $b_weight = $b['weight'] ?? 0;
      return $a_weight !== $b_weight ? $a_weight - $b_weight : strnatcmp((string) ($a['label'] ?? ''), (string) ($b['label'] ?? ''));
    });
    return $definitions;
  }

}
