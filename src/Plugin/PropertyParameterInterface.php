<?php

namespace Drupal\parameters\Plugin;

/**
 * Interface implemented by parameter plugins providing nested properties.
 */
interface PropertyParameterInterface extends ParameterInterface {

  /**
   * Get a property as parameter.
   *
   * @param string $name
   *   The property name.
   *
   * @return \Drupal\parameters\Plugin\ParameterInterface|null
   *   The property as parameter. Returns NULL if no property exists for the
   *   given name.
   */
  public function getProperty(string $name): ?ParameterInterface;

}
