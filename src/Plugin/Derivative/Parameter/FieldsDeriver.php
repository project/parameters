<?php

namespace Drupal\parameters\Plugin\Derivative\Parameter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Parameter plugin deriver for fields.
 *
 * @see \Drupal\parameters\Plugin\Parameter\Fields
 */
class FieldsDeriver implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * A statically cached list of derivative definitions.
   *
   * @var array
   */
  protected static ?array $derivatives;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Creates a new ContentDerivativeBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new static(
      $container->get('entity_type.manager')
    );
    $instance->setStringTranslation($container->get('string_translation'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinition($derivative_id, $base_plugin_definition) {
    if (!isset(self::$derivatives)) {
      $this->getDerivativeDefinitions($base_plugin_definition);
    }
    return isset(self::$derivatives[$derivative_id]) ? self::$derivatives[$derivative_id] + $base_plugin_definition : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    self::$derivatives = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if (!($entity_type->entityClassImplements(ContentEntityInterface::class))) {
        continue;
      }

      self::$derivatives[$entity_type_id] = [
        'label' => new TranslatableMarkup('Selection of @label', [
          '@label' => new TranslatableMarkup('@type fields', ['@type' => $entity_type->getLabel()]),
        ]),
        'entity_type' => $entity_type_id,
      ];
    }

    $derivatives = self::$derivatives;
    foreach ($derivatives as &$item) {
      $item += $base_plugin_definition;
    }

    return $derivatives;
  }

}
