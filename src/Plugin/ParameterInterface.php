<?php

namespace Drupal\parameters\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * The interface implemented by all parameter plugins.
 */
interface ParameterInterface extends PluginInspectionInterface, PluginFormInterface, ConfigurableInterface, ThirdPartySettingsInterface {

  /**
   * Validates the configured value(s).
   *
   * @return bool|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Returns TRUE if validation had no errors, or the validation error message
   *   that describes the found error.
   */
  public function validate();

  /**
   * Get the data resulting from configured value(s).
   *
   * @return \Drupal\Core\TypedData\TypedDataInterface
   *   The processed data.
   */
  public function getProcessedData(): TypedDataInterface;

  /**
   * Get a preview of the configured value(s).
   *
   * @return string|\Drupal\Component\Render\MarkupInterface
   *   A preview.
   */
  public function getPreview(): string|MarkupInterface;

  /**
   * Get the type of this parameter.
   *
   * The parameter type is the identifier of the according parameter plugin.
   *
   * @return string
   *   The parameter type.
   */
  public function getType(): string;

  /**
   * Get the configured machine name of this parameter.
   *
   * Please note that nested parameters may not necessarily know their origin.
   *
   * @return string
   *   The parameter name.
   */
  public function getName(): string;

  /**
   * Get the configured human-readable label of this parameter.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Get the description of this parameter.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string;

  /**
   * Get the configured weight of this parameter.
   *
   * @return int
   *   The weight.
   */
  public function getWeight(): int;

  /**
   * Get info about how the plugin can be used.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that is holding the parameter.
   *
   * @return array
   *   The information as renderable array.
   */
  public function getUsageHelp(EntityInterface $entity): array;

}
