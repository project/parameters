<?php

namespace Drupal\parameters\EventSubscriber;

use Drupal\language\Config\LanguageConfigOverrideCrudEvent;
use Drupal\language\Config\LanguageConfigOverrideEvents;
use Drupal\parameters\Entity\ParametersCollectionStorage;
use Drupal\parameters\Plugin\ParameterManager;
use Drupal\parameters\Plugin\UsageParameterInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to trigger parameter config changes on overrides.
 */
class ParametersConfigOverrideSubscriber implements EventSubscriberInterface {

  public function onOverrideSave($event) {
    $this->applyChanges($event, 'config_override_save');
  }

  public function onOverrideDelete($event) {
    $this->applyChanges($event, 'config_override_delete');
  }

  protected function applyChanges($event, string $op): void {
    if (!($event instanceof LanguageConfigOverrideCrudEvent)) {
      return;
    }

    $config_name = $event->getLanguageConfigOverride()->getName();
    [$provider, $type, $id] = array_merge(explode('.', $config_name, 3), ['', '']);
    if (!($provider === 'parameters' && $type === 'collection')) {
      return;
    }
    $override_param_configs = $event->getLanguageConfigOverride()->get('parameters') ?? [];
    if (!$override_param_configs) {
      return;
    }

    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    if (!($collection = ParametersCollectionStorage::get()->load($id))) {
      return;
    }

    // We want to manipulate the object without interfering with others.
    $collection = clone $collection;
    $collection->set('langcode', $event->getLanguageConfigOverride()->getLangcode());
    foreach ($event->getLanguageConfigOverride()->get() as $k => $v) {
      if (is_null($v)) {
        continue;
      }
      if (is_array($collection->get($k)) && is_array($v)) {
        $collection->set($k, $v + $collection->get($k));
      }
      else {
        $collection->set($k, $v);
      }
    }

    $collection_param_configs = $collection->get('parameters') ?? [];
    foreach ($override_param_configs as $param_name => $param_config) {
      if (empty($param_config['type'])) {
        continue;
      }
      $parameter = ParameterManager::get()->createInstance($param_config['type'], $param_config);
      if ($parameter instanceof UsageParameterInterface) {
        // @todo Is there a way of providing the original override values?
        $parameter->onChange($collection_param_configs[$param_name] ?? $param_config, $collection, $op);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    if (class_exists('Drupal\language\Config\LanguageConfigOverrideEvents')) {
      $events[LanguageConfigOverrideEvents::SAVE_OVERRIDE] = 'onOverrideSave';
      $events[LanguageConfigOverrideEvents::DELETE_OVERRIDE] = 'onOverrideDelete';
    }
    return $events;
  }

}
