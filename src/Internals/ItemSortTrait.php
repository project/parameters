<?php

namespace Drupal\parameters\Internals;

use Drupal\Core\Form\FormStateInterface;

/**
 * Trait providing helpers for sorting parameter items.
 */
trait ItemSortTrait {

  /**
   * The wrapper ID for refreshind the list via Ajax.
   *
   * @var string
   */
  protected string $ajaxWrapperId = 'parameters-items-order-wrapper';

  protected function ajaxHandlerForOrderFormElement(): array {
    return [
      '#ajax' => [
        'wrapper' => $this->ajaxWrapperId,
        'callback' => [static::class, 'orderFormElementAjax'],
        'method' => 'replaceWith',
      ],
    ];
  }

  /**
   * Ajax handler for order form element.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function orderFormElementAjax(array &$form, FormStateInterface $form_state): array {
    return $form['parameter']['settings']['order'];
  }

  /**
   * Builds a form element for sorting selected parameter items.
   *
   * @param string $key
   *   The configuration key that identifies the selected items.
   * @param array $options
   *   The available options used to be displayed in the form.
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function buildOrderFormElementForSelected(string $key, array $options, array &$form, FormStateInterface $form_state) {
    $user_input = &$form_state->getUserInput();
    $selected = !empty($user_input) && isset($user_input[$key]) ? array_keys(array_filter($user_input[$key])) : ($this->configuration[$key] ?? []);

    $form['order'] = [
      '#prefix' => '<div id="' . $this->ajaxWrapperId . '">',
      '#suffix' => '</div>',
      '#weight' => 40,
    ];
    if (empty($selected)) {
      $form['order'] += ['#markup' => ''];
      return;
    }

    $form['order'] += [
      '#type' => 'table',
      '#attributes' => ['id' => 'parameters-items-order'],
      '#title' => $this->t('Order'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'item-order-weight',
        ],
      ],
      '#input' => FALSE,
      '#theme_wrappers' => ['form_element'],
    ];

    $submitted_orders = !empty($user_input) && isset($user_input['order']) ? $user_input['order'] : [];
    $max_weight = 0;
    foreach ($submitted_orders as $submitted_order) {
      $max_weight = max($max_weight, $submitted_order['weight'] ?? 0);
    }
    foreach ($selected as $item) {
      $item_weight = $submitted_orders[$item]['weight'] ?? (++$max_weight);
      $label = $options[$item] ?? $item;
      $form['order'][$item]['#attributes']['class'][] = 'draggable';
      $form['order'][$item]['#weight'] = $item_weight;
      $form['order'][$item]['label'] = [
        '#markup' => $label,
      ];
      $form['order'][$item]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $label]),
        '#title_display' => 'invisible',
        '#delta' => 10,
        '#default_value' => $item_weight,
        '#attributes' => ['class' => ['item-order-weight']],
      ];
    }
  }

  /**
   * Submit handler for sorting selected parameter items.
   *
   * @param array $selected
   *   The selected parameter items.
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function submitOrderFormElementForSelected(array &$selected, array &$form, FormStateInterface $form_state) {
    if ($form_state->hasValue('order')) {
      $order = $form_state->getValue('order');
      usort($selected, function($a, $b) use ($order) {
        $left = $order[$a]['weight'] ?? 0;
        $right = $order[$b]['weight'] ?? 0;
        if ($left < $right) {
          return -1;
        }
        if ($left > $right) {
          return 1;
        }
        return 0;
      });
    }
  }

}
