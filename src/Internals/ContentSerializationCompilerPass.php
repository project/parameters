<?php

namespace Drupal\parameters\Internals;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Adds a subset of normalizers and encoders to the Serializer for content.
 *
 * @internal This class is not meant for API usage and is subject to change.
 */
final class ContentSerializationCompilerPass implements CompilerPassInterface {

  /**
   * Adds services to the Parameter content serializer.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
   *   The container to process.
   */
  public function process(ContainerBuilder $container) {
    $definition = $container->getDefinition('parameters.serializer');

    // Retrieve registered Normalizers and Encoders from the container.
    foreach ($container->findTaggedServiceIds('normalizer') + $container->findTaggedServiceIds('parameters_content_normalizer') as $id => $attributes) {
      $normalizer_definition = $container->getDefinition($id);
      $normalizer_class = $normalizer_definition->getClass() ?? '';
      if ((mb_strpos($normalizer_class, 'Drupal\serialization\\') === FALSE) && (mb_strpos($normalizer_class, 'Drupal\parameters') === FALSE)) {
        // Not relevant for the content serializer.
        continue;
      }
      if (in_array($id, ['serializer.normalizer.content_entity', 'serializer.normalizer.entity_reference_field_item'], TRUE)) {
        // Replaced by decorating counterparts in parameters.services.yml.
        continue;
      }

      $priority = $attributes[0]['priority'] ?? 0;
      $normalizers[$priority][] = new Reference($id);
    }

    $encoders = [];
    foreach ($container->findTaggedServiceIds('encoder') as $id => $attributes) {
      $encoder_definition = $container->getDefinition($id);
      $encoder_class = $encoder_definition->getClass() ?? '';
      if ((mb_strpos('Drupal\serialization\\', $encoder_class) === FALSE) && (mb_strpos('Drupal\parameters', $encoder_class) === FALSE)) {
        // Not relevant for the content serializer.
        continue;
      }

      $priority = $attributes[0]['priority'] ?? 0;
      $encoders[$priority][] = new Reference($id);
    }

    // Add the registered Normalizers and Encoders to the Serializer.
    if (!empty($normalizers)) {
      $definition->replaceArgument(0, $this->sort($normalizers));
    }
    if (!empty($encoders)) {
      $definition->replaceArgument(1, $this->sort($encoders));
    }
  }

  /**
   * Sorts by priority.
   *
   * Order services from highest priority number to lowest (reverse sorting).
   *
   * @param array $services
   *   A nested array keyed on priority number. For each priority number, the
   *   value is an array of Symfony\Component\DependencyInjection\Reference
   *   objects, each a reference to a normalizer or encoder service.
   *
   * @return array
   *   A flattened array of Reference objects from $services, ordered from high
   *   to low priority.
   */
  protected function sort($services) {
    krsort($services);
    return array_merge([], ...$services);
  }

}
