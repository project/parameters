<?php

namespace Drupal\parameters\Internals;

use Symfony\Component\Serializer\Serializer;

/**
 * Parameters content serializer.
 *
 * @internal This class is not meant for API usage and is subject to change.
 */
final class ParametersContentSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $normalizers = [], array $encoders = []) {
    // Clone objects so that they do not interfere with the other serializer.
    foreach ($normalizers as $i => $normalizer) {
      $normalizers[$i] = clone $normalizer;
    }
    foreach ($encoders as $i => $encoder) {
      $encoders[$i] = clone $encoder;
    }
    parent::__construct($normalizers, $encoders);
  }

}
