<?php

namespace Drupal\parameters\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a Parameter attribute object.
 *
 * Plugin Namespace: Plugin\Parameter
 *
 * @see plugin_api
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Parameter extends Plugin {

  /**
   * Constructs a Parameter attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $label
   *   The human-readable name of the parameter type.
   * @param int $weight
   *   (optional) A sorting weight.
   * @param string|null $entity_type
   *   (optional) Meta info about which entity type the parameter is based on.
   * @param string|null $bundle
   *   (optional) Meta info about which entity bundle the parameter is based on.
   * @param class-string|null $deriver
   *   (optional) The deriver class.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?TranslatableMarkup $label = NULL,
    public readonly int $weight = 0,
    public readonly ?string $entity_type = NULL,
    public readonly ?string $bundle = NULL,
    public readonly ?string $deriver = NULL
  ) {}

}
