<?php

namespace Drupal\parameters_ui\Controller;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for managing parameters via UI.
 */
class ParametersUiController extends ControllerBase {

  /**
   * The parameters collection storage.
   *
   * @var \Drupal\parameters\Entity\ParametersCollectionStorage
   */
  protected EntityStorageInterface $collectionStorage;

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\parameters_ui\Controller\ParametersUiController $instance */
    $instance = parent::create($container);
    $instance->setCollectionStorage($container->get('entity_type.manager')->getStorage(ParametersCollectionInterface::ENTITY_TYPE_ID));
    $instance->setParameterManager($container->get(ParameterManager::SERVICE_NAME));
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * Returns a form for a parameters collection config.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface|null $parameters_collection
   *   (optional) The parameters collection.
   * @param string|null $parameters_collection_id
   *   (optional) The collection ID.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   */
  public function parametersCollectionForm(?ParametersCollectionInterface $parameters_collection = NULL, ?string $parameters_collection_id = NULL, ?string $entity_type_id = NULL, ?string $bundle = NULL): array {
    if (($parameters_collection === NULL) && ($parameters_collection_id === NULL) && ($entity_type_id === NULL)) {
      throw new NotFoundHttpException();
    }

    if (!$parameters_collection) {
      $id = $parameters_collection_id ?? $entity_type_id . '.' . $bundle;
      $parameters_collection = $this->loadCollection($id);
      if (!$parameters_collection) {
        $parameters_collection = $this->collectionStorage->create(['id' => $id]);
      }
    }
    return [
      'form' => $this->entityFormBuilder()->getForm($parameters_collection, 'default'),
    ];
  }

  /**
   * Title callback for parameters collections.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface|null $parameters_collection
   *   (optional) The parameters collection.
   * @param string|null $parameters_collection_id
   *   (optional) The collection ID.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function parametersCollectionTitle(?ParametersCollectionInterface $parameters_collection = NULL, ?string $parameters_collection_id = NULL, ?string $entity_type_id = NULL, ?string $bundle = NULL): TranslatableMarkup {
    if (($parameters_collection === NULL) && ($parameters_collection_id === NULL) && ($entity_type_id === NULL)) {
      throw new NotFoundHttpException();
    }

    if (!$parameters_collection) {
      $id = $parameters_collection_id ?? $entity_type_id . '.' . $bundle;
      $parameters_collection = $this->loadCollection($id);
      if (!$parameters_collection) {
        $parameters_collection = $this->collectionStorage->create(['id' => $id]);
      }
    }

    return $this->t('Manage %label parameters', ['%label' => $parameters_collection->label()]);
  }

  /**
   * Returns a form for deleting a parameters collection config.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface|null $parameters_collection
   *   (optional) The parameters collection.
   * @param string|null $parameters_collection_id
   *   (optional) The collection ID.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   */
  public function parametersCollectionDeleteForm(?ParametersCollectionInterface $parameters_collection = NULL, ?string $parameters_collection_id = NULL, ?string $entity_type_id = NULL, ?string $bundle = NULL): array {
    if (!$parameters_collection) {
      $id = $parameters_collection_id ?? $entity_type_id . '.' . $bundle;
      $parameters_collection = $this->loadCollection($id);
    }
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $parameters_collection */
    if ($parameters_collection && !$parameters_collection->isLocked()) {
      return [
        'form' => $this->entityFormBuilder()->getForm($parameters_collection, 'delete'),
      ];
    }

    throw new NotFoundHttpException();
  }

  /**
   * Returns a form for locking a parameters collection.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface|null $parameters_collection
   *   (optional) The parameters collection.
   * @param string|null $parameters_collection_id
   *   (optional) The collection ID.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   */
  public function parametersCollectionLockForm(?ParametersCollectionInterface $parameters_collection = NULL, ?string $parameters_collection_id = NULL, ?string $entity_type_id = NULL, ?string $bundle = NULL): array {
    if (!$parameters_collection) {
      $id = $parameters_collection_id ?? $entity_type_id . '.' . $bundle;
      $parameters_collection = $this->loadCollection($id);
    }
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $parameters_collection */
    if ($parameters_collection && !$parameters_collection->isLocked()) {
      return [
        'form' => $this->entityFormBuilder()->getForm($parameters_collection, 'lock'),
      ];
    }

    throw new NotFoundHttpException();
  }

  /**
   * Returns a form for unlocking a parameters collection config.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface|null $parameters_collection
   *   (optional) The parameters collection.
   * @param string|null $parameters_collection_id
   *   (optional) The collection ID.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   */
  public function parametersCollectionUnlockForm(?ParametersCollectionInterface $parameters_collection = NULL, ?string $parameters_collection_id = NULL, ?string $entity_type_id = NULL, ?string $bundle = NULL): array {
    if (!$parameters_collection) {
      $id = $parameters_collection_id ?? $entity_type_id . '.' . $bundle;
      $parameters_collection = $this->loadCollection($id);
    }
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $parameters_collection */
    if ($parameters_collection && $parameters_collection->isLocked()) {
      return [
        'form' => $this->entityFormBuilder()->getForm($parameters_collection, 'unlock'),
      ];
    }

    throw new NotFoundHttpException();
  }

  /**
   * Returns a form for adding a new parameter to a collection.
   *
   * @param string $parameters_parameter_type
   *   The ID that identifies the type of parameter plugin to use.
   * @param string|null $parameters_parameter_name
   *   (optional) The machine name of the new parameter.
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface|null $parameters_collection
   *   (optional) The parameters collection.
   * @param string|null $parameters_collection_id
   *   (optional) The collection ID.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   */
  public function parameterAddForm(string $parameters_parameter_type, ?string $parameters_parameter_name= NULL, ?ParametersCollectionInterface $parameters_collection = NULL, ?string $parameters_collection_id = NULL, ?string $entity_type_id = NULL, ?string $bundle = NULL): array {
    if (($parameters_collection === NULL) && ($parameters_collection_id === NULL) && ($entity_type_id === NULL)) {
      throw new NotFoundHttpException();
    }

    if (!$parameters_collection) {
      $id = $parameters_collection_id ?? $entity_type_id . '.' . $bundle;
      $parameters_collection = $this->loadCollection($id);
      if (!$parameters_collection) {
        $parameters_collection = $this->collectionStorage->create(['id' => $id]);
      }
    }

    if (!$this->parameterManager->hasDefinition($parameters_parameter_type)) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $parameters_collection */
    if (isset($parameters_parameter_name) && $parameters_collection->getParameters()->has($parameters_parameter_name)) {
      throw new NotFoundHttpException();
    }

    $parameter_keys = [
      'type' => $parameters_parameter_type,
      'name' => $parameters_parameter_name,
      'label' => '',
      'description' => '',
    ];
    $parameter = $this->parameterManager->createInstance($parameters_parameter_type, $parameter_keys);

    if (($parameter instanceof AccessibleInterface) && !$parameter->access('create', $this->currentUser())) {
      throw new AccessDeniedHttpException();
    }

    return [
      'form' => $this->formBuilder()->getForm('Drupal\parameters_ui\Form\ParameterForm', $parameters_collection, $parameter),
    ];
  }

  /**
   * Returns a form for editing a parameter.
   *
   * @param string $parameters_parameter_name
   *   The machine name of the parameter.
   * @param \Drupal\parameters\Entity\ParametersCollectionInterface|null $parameters_collection
   *   (optional) The parameters collection.
   * @param string|null $parameters_collection_id
   *   (optional) The collection ID.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   */
  public function parameterEditForm(string $parameters_parameter_name, ?ParametersCollectionInterface $parameters_collection = NULL, ?string $parameters_collection_id = NULL, ?string $entity_type_id = NULL, ?string $bundle = NULL): array {
    if (($parameters_collection === NULL) && ($parameters_collection_id === NULL) && ($entity_type_id === NULL)) {
      throw new NotFoundHttpException();
    }
    if (!$parameters_collection) {
      $id = $parameters_collection_id ?? $entity_type_id . '.' . $bundle;
      $parameters_collection = $this->loadCollection($id);
      if (!$parameters_collection) {
        throw new NotFoundHttpException();
      }
    }

    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $parameters_collection */
    $parameters = $parameters_collection->getParameters();
    if (!$parameters->has($parameters_parameter_name)) {
      throw new NotFoundHttpException();
    }
    $parameter = $parameters->get($parameters_parameter_name);

    if (($parameter instanceof AccessibleInterface) && !$parameter->access('update', $this->currentUser())) {
      throw new AccessDeniedHttpException();
    }

    return [
      'form' => $this->formBuilder()->getForm('Drupal\parameters_ui\Form\ParameterForm', $parameters_collection, $parameter),
    ];
  }

  /**
   * Returns a form for deleting a configured parameter.
   *
   * @param string $parameters_parameter_name
   *   The machine name of the parameter.
   * @param string|null $parameters_collection_id
   *   (optional) The collection ID.
   * @param string|null $entity_type_id
   *   (optional) The entity type ID.
   * @param string|null $bundle
   *   (optional) The entity bundle.
   */
  public function parameterDeleteForm(string $parameters_parameter_name, ?ParametersCollectionInterface $parameters_collection = NULL, ?string $parameters_collection_id = NULL, ?string $entity_type_id = NULL, ?string $bundle = NULL): array {
    if (($parameters_collection === NULL) && ($parameters_collection_id === NULL) && ($entity_type_id === NULL)) {
      throw new NotFoundHttpException();
    }
    if (!$parameters_collection) {
      $id = $parameters_collection_id ?? $entity_type_id . '.' . $bundle;
      $parameters_collection = $this->loadCollection($id);
      if (!$parameters_collection) {
        throw new NotFoundHttpException();
      }
    }

    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $parameters_collection */
    if ($parameters_collection->isLocked()) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $parameters_collection */
    $parameters = $parameters_collection->getParameters();
    if (!$parameters->has($parameters_parameter_name)) {
      throw new NotFoundHttpException();
    }
    $parameter = $parameters->get($parameters_parameter_name);

    if (($parameter instanceof AccessibleInterface) && !$parameter->access('delete', $this->currentUser())) {
      throw new AccessDeniedHttpException();
    }

    return [
      'form' => $this->formBuilder()->getForm('Drupal\parameters_ui\Form\ParameterDeleteForm', $parameters_collection, $parameter),
    ];
  }

  /**
   * Set the parameters collection storage.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionStorage $storage
   *   The storage.
   */
  public function setCollectionStorage(EntityStorageInterface $storage): void {
    $this->collectionStorage = $storage;
  }

  /**
   * Set the parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager $manager
   *   The manager.
   */
  public function setParameterManager(PluginManagerInterface $manager): void {
    $this->parameterManager = $manager;
  }

  /**
   * Loads the collection by requested ID.
   *
   * @param string $id
   *   The entity ID.
   *
   * @return \Drupal\parameters\Entity\ParametersCollectionInterface|null
   *   The collection, or NULL if not found.
   */
  protected function loadCollection(string $id): ?ParametersCollectionInterface {
    // Currently the Parameters UI does not support translations.
    return $this->collectionStorage->loadOverrideFree($id);;
  }

}
