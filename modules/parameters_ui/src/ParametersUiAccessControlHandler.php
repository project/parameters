<?php

namespace Drupal\parameters_ui;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an entity access control handler regarding parameter collections.
 */
class ParametersUiAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $entity */
    $id_parts = explode('.', $entity->id(), 2);
    if (count($id_parts) > 1) {
      $result = parent::checkAccess($entity, $operation, $account)
        ->orIf(AccessResult::allowedIfHasPermission($account, 'administer ' . reset($id_parts) . ' parameters'))
        ->orIf(AccessResult::allowedIfHasPermission($account, 'administer ' . $entity->id() . ' parameters'));
    }
    else {
      $result = parent::checkAccess($entity, $operation, $account)
        ->orIf(AccessResult::allowedIfHasPermission($account, 'administer ' . $entity->id() . ' parameters'));
    }
    if ($operation === 'unlock') {
      $result = $result->andIf(AccessResult::allowedIfHasPermission($account, 'unlock parameters'));
    }
    return $result;
  }

}
