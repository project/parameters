<?php

namespace Drupal\parameters_ui;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\parameters\Entity\ParametersCollectionStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions of the Parameters UI module.
 */
class ParametersUiPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $bundleInfo;

  /**
   * Constructs a new FieldUiPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity type bundle info.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'), $container->get('entity_type.bundle.info'));
  }

  /**
   * Returns an array of Parameters UI permissions.
   *
   * @return array
   *   The permissions.
   */
  public function parameterPermissions(): array {
    $permissions = [
      'administer parameters' => [
        'title' => $this->t('Administer all parameters'),
        'description' => $this->t('Create, edit and delete all collections of parameters.'),
        'restrict access' => TRUE,
      ],
      'unlock parameters' => [
        'title' => $this->t('Unlock parameters'),
        'description' => $this->t('Unlock collections of parameters for removal.'),
        'restrict access' => TRUE,
      ],
    ];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if (!$entity_type->get('field_ui_base_route')) {
        continue;
      }
      // Create a permission for each fieldable entity type.
      $permissions['administer ' . $entity_type_id . ' parameters'] = [
        'title' => $this->t('%entity_label: Administer parameters', ['%entity_label' => $entity_type->getLabel()]),
        'restrict access' => TRUE,
        'dependencies' => ['module' => [$entity_type->getProvider()]],
      ];
      // Create a permission for each bundle of a fieldable entity type.
      foreach ($this->bundleInfo->getBundleInfo($entity_type_id) as $bundle => $bundle_info) {
        $permissions['administer ' . $entity_type_id . '.' . $bundle . ' parameters'] = [
          'title' => $this->t('%entity_label - %bundle: Administer parameters', [
            '%entity_label' => $entity_type->getLabel(),
            '%bundle' => is_array($bundle_info) && isset($bundle_info['label']) ? $bundle_info['label'] : $bundle,
          ]),
          'dependencies' => ['module' => [$entity_type->getProvider()]],
        ];
      }
    }

    foreach (ParametersCollectionStorage::get()->getQuery()->accessCheck(FALSE)->execute() as $collection_id) {
      $permission = 'administer ' . $collection_id . ' parameters';
      if (!isset($permissions[$permission])) {
        $permissions[$permission] = [
          'title' => $this->t('%id: Administer parameters', ['%id' => $collection_id]),
          'dependencies' => ['module' => ['parameters']],
        ];
      }
    }

    return $permissions;
  }

}
