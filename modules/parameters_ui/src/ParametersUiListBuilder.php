<?php

namespace Drupal\parameters_ui;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Defines a class to build a UI listing of parameter collections.
 *
 * @see \Drupal\parameters\Entity\ParametersCollection
 */
class ParametersUiListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['defined'] = $this->t('Defined');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $defined = [];
    $i = 0;
    $parameters = $entity->getParameters();
    /** @var \Drupal\parameters\Plugin\ParameterInterface $parameter */
    foreach ($parameters as $name => $parameter) {
      if ($i >= 5) {
        break;
      }
      $defined[] = $name . ' (' . $parameter->getPluginDefinition()['label'] . ')';
      $i++;
    }
    $rest = $parameters->count() - $i;
    if ($rest > 0) {
      $defined[] = $this->t('... + @num more', ['@num' => $rest]);
    }
    $defined = $defined ? '<ul><li>' . implode('</li><li>', $defined) . '</li></ul>' : '';
    $row['defined'] = Markup::create($defined);

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'Currently no collections are available. Have a look at the <a href=":url" target="_blank" rel="noreferrer noopener">README</a> for knowing how to create your first collection of parameters.',
      [':url' => 'https://git.drupalcode.org/project/parameters/-/blob/1.0.x/README.md#4-usage']
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    $collection = $entity;

    $operations = [];

    $operations['edit'] = [
      'title' => t('Edit'),
      'weight' => 10,
      'url' => Url::fromRoute("entity.parameters_collection.edit_form", [
        'parameters_collection' => $collection->id(),
      ]),
    ];

    return $operations;
  }

}
