<?php

namespace Drupal\parameters_ui\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Parameters UI routes.
 */
class ParametersUiRouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ParametersUiRouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $collection_type_id = ParametersCollectionInterface::ENTITY_TYPE_ID;
    $config_path = "/admin/config/parameters";
    $route = new Route(
      $config_path,
      [
        '_title' => 'Parameters',
        '_entity_list' => $collection_type_id,
      ],
      ['_permission' => 'administer parameters'],
      ['_parameters_ui' => TRUE]
    );
    $collection->add("entity.$collection_type_id.collection", $route);

    $route = new Route(
      "/admin/config/add-parameters-collection",
      [
        '_title' => 'Add new parameters collection',
        '_form' => 'Drupal\parameters_ui\Form\ParametersCollectionAddForm',
      ],
      ['_permission' => 'administer parameters'],
      ['_parameters_ui' => TRUE]
    );
    $collection->add("entity.$collection_type_id.add", $route);

    $route = new Route(
      $config_path . '/{parameters_collection}',
      [
        '_title_callback' => '\Drupal\parameters_ui\Controller\ParametersUiController::parametersCollectionTitle',
        '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parametersCollectionForm',
      ],
      ['_entity_access' => 'parameters_collection.update'],
      ['_parameters_ui' => TRUE]
    );
    $collection->add("entity.$collection_type_id.edit_form", $route);

    $route = new Route(
      $config_path . '/{parameters_collection}/delete',
      [
        '_title' => 'Delete parameters collection',
        '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parametersCollectionDeleteForm',
      ],
      ['_entity_access' => 'parameters_collection.delete'],
      ['_parameters_ui' => TRUE]
    );
    $collection->add("entity.$collection_type_id.delete_form", $route);

    $route = new Route(
      $config_path . '/{parameters_collection}/lock',
      [
        '_title' => 'Lock parameters collection',
        '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parametersCollectionLockForm',
      ],
      ['_entity_access' => 'parameters_collection.lock'],
      ['_parameters_ui' => TRUE]
    );
    $collection->add("entity.$collection_type_id.lock_form", $route);

    $route = new Route(
      $config_path . '/{parameters_collection}/unlock',
      [
        '_title' => 'Unlock parameters collection',
        '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parametersCollectionUnlockForm',
      ],
      ['_entity_access' => 'parameters_collection.unlock'],
      ['_parameters_ui' => TRUE]
    );
    $collection->add("entity.$collection_type_id.unlock_form", $route);

    $defaults = [
      '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parametersCollectionForm',
    ];
    $options = [];
    // Special parameter used to easily recognize all Parameters UI routes.
    $options['_parameters_ui'] = TRUE;

    $route = new Route(
      "$config_path/{parameters_collection}/add/{parameters_parameter_type}/{parameters_parameter_name}",
      [
        '_title' => 'Add parameter',
        '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parameterAddForm',
        'parameters_parameter_name' => '',
      ] + $defaults,
      ['_entity_access' => 'parameters_collection.update'],
      $options
    );
    $collection->add("parameters.parameter.add", $route);
    $route = new Route(
      "$config_path/{parameters_collection}/{parameters_parameter_name}/edit",
      [
        '_title' => 'Edit parameter',
        '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parameterEditForm',
      ] + $defaults,
      ['_entity_access' => 'parameters_collection.update'],
      $options
    );
    $collection->add("parameters.parameter.edit", $route);
    $route = new Route(
      "$config_path/{parameters_collection}/{parameters_parameter_name}/delete",
      [
        '_title' => 'Delete parameter',
        '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parameterDeleteForm',
      ] + $defaults,
      ['_entity_access' => 'parameters_collection.delete'],
      $options
    );
    $collection->add("parameters.parameter.delete", $route);

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if (!$route_name = $entity_type->get('field_ui_base_route')) {
        continue;
      }
      if (!$entity_route = $collection->get($route_name)) {
        continue;
      }
      $entity_path = $entity_route->getPath();

      $options = $entity_route->getOptions();
      if ($bundle_entity_type = $entity_type->getBundleEntityType()) {
        $options['parameters'][$bundle_entity_type] = [
          'type' => 'entity:' . $bundle_entity_type,
        ];
      }
      // Special parameter used to easily recognize all Parameters UI routes.
      $options['_parameters_ui'] = TRUE;

      $defaults = [
        '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parametersCollectionForm',
        'entity_type_id' => $entity_type_id,
      ];
      // If the entity type has no bundles and it doesn't use {bundle} in its
      // admin path, use the entity type.
      if (strpos($entity_path, '{bundle}') === FALSE) {
        $defaults['bundle'] = !$entity_type->hasKey('bundle') ? $entity_type_id : '';
      }

      $permissions = ['_permission' => 'administer parameters+administer ' . $entity_type_id . ' parameters'];

      $route = new Route(
        "$entity_path/parameters",
        [
          '_title' => 'Manage parameters',
        ] + $defaults,
        $permissions,
        $options
      );
      $collection->add("entity.parameters.{$entity_type_id}.edit", $route);

      $route = new Route(
        "$entity_path/parameters/add/{parameters_parameter_type}/{parameters_parameter_name}",
        [
          '_title' => 'Add parameter',
          '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parameterAddForm',
          'parameters_parameter_name' => '',
        ] + $defaults,
        $permissions,
        $options
      );
      $collection->add("entity.parameters.parameter.{$entity_type_id}.add", $route);
      $route = new Route(
        "$entity_path/parameters/{parameters_parameter_name}/edit",
        [
          '_title' => 'Edit parameter',
          '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parameterEditForm',
        ] + $defaults,
        $permissions,
        $options
      );
      $collection->add("entity.parameters.parameter.{$entity_type_id}.edit", $route);
      $route = new Route(
        "$entity_path/parameters/{parameters_parameter_name}/delete",
        [
          '_title' => 'Delete parameter',
          '_controller' => '\Drupal\parameters_ui\Controller\ParametersUiController::parameterDeleteForm',
        ] + $defaults,
        $permissions,
        $options
      );
      $collection->add("entity.parameters.parameter.{$entity_type_id}.delete", $route);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -75];
    return $events;
  }

}
