<?php

namespace Drupal\parameters_ui\Form;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\parameters\Entity\ParametersCollection;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Plugin\ParameterInterface;
use Drupal\parameters\Plugin\ParameterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for configuring a parameter.
 */
class ParameterForm implements FormInterface, ContainerInjectionInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The parameters collection.
   *
   * @var \Drupal\parameters\Entity\ParametersCollectionInterface|null
   */
  protected ?ParametersCollectionInterface $collection;

  /**
   * The parameter plugin.
   *
   * @var \Drupal\parameters\Plugin\ParameterInterface|null
   */
  protected ?ParameterInterface $parameter;

  /**
   * This flag indicates whether a new parameter has been saved.
   *
   * @var bool
   */
  protected bool $savedNewParameter = FALSE;

  /**
   * The ParameterForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\parameters\Entity\ParametersCollectionInterfac|null $collection
   *   The parameters collection.
   * @param \Drupal\parameters\Plugin\ParameterInterface|null $parameter
   *   The parameter plugin.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, RouteMatchInterface $route_match, ?ParametersCollectionInterface $collection = NULL, ?ParameterInterface $parameter = NULL) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->routeMatch = $route_match;
    $this->collection = $collection;
    $this->parameter = $parameter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, ?ParametersCollectionInterface $collection = NULL, ?ParameterInterface $parameter = NULL) {
    $instance = new static($container->get('entity_type.manager'), $container->get('messenger'), $container->get('current_route_match'), $collection, $parameter);
    $instance->setStringTranslation($container->get('string_translation'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'parameters_parameter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?ParametersCollectionInterface $collection = NULL, ?ParameterInterface $parameter = NULL) {
    $form['#tree'] = TRUE;
    $form['#process'][] = '::processForm';
    $form['#after_build'][] = '::afterBuild';
    $this->initProperties($form, $form_state, $collection, $parameter);
    $parameter = $this->parameter;
    $definition = $parameter->getPluginDefinition();
    $config = $parameter->getConfiguration();

    $weight = 0;
    $parameter_is_new = !isset($config['name']) || !$this->collection->getParameters()->has($config['name']);
    $form_state->set('parameter_is_new', $parameter_is_new);

    $weight += 10;

    $form['parameter'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('@type: @name', [
        '@type' => $definition['label'],
        '@name' => $config['name'],
      ]),
      '#weight' => $weight++,
      '#tree' => FALSE,
    ];

    $weight += 10;
    $form['parameter']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('The human-readable label of this parameter.'),
      '#required' => TRUE,
      '#weight' => $weight,
      '#default_value' => $config['label'] ?? '',
    ];
    $weight += 10;
    $form['parameter']['name'] = [
      '#type' => 'machine_name',
      '#default_value' => $config['name'] ?? '',
      '#maxlength' => 32,
      '#disabled' => !$parameter_is_new && $collection->isLocked(),
      '#machine_name' => [
        'exists' => [$this, 'parameterExists'],
        'source' => ['parameter', 'label'],
      ],
      '#required' => TRUE,
      '#weight' => ($weight += 10),
    ];
    if (!$parameter_is_new && $collection->isLocked()) {
      $form['parameter']['name']['#description'] = $this->t('The collection, holding this parameter, is locked and therefore cannot be changed.');
    }
    $weight += 10;
    $form['parameter']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Optionally define an administrative description of this parameter.'),
      '#required' => FALSE,
      '#weight' => $weight,
      '#default_value' => $config['description'] ?? '',
      '#rows' => 2,
    ];

    $weight += 100;
    if ($parameter instanceof PluginFormInterface) {
      $weight += 100;
      $form['parameter']['settings'] = [
        '#parents' => [],
        '#weight' => $weight,
      ];
      $parameter_form_state = SubformState::createForSubform($form['parameter']['settings'], $form, $form_state);
      $form['parameter']['settings'] = $parameter->buildConfigurationForm($form['parameter']['settings'], $parameter_form_state);
    }
    else {
      $form['parameter']['no_settings'] = [
        '#type' => 'markup',
        '#markup' => $this->t('This parameter does not provide any settings.'),
        '#weight' => $weight,
      ];
    }

    $weight += 100;
    $form['actions']['#weight'] = $weight++;
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm', '::save', '::redirectAfterSave'],
      '#weight' => 10,
    ];
    if (!$parameter_is_new && !$collection->isLocked() && (!($parameter instanceof AccessibleInterface) || $parameter->access('delete', \Drupal::currentUser()))) {
      $form['actions']['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
        '#access' => $this->collection->access('delete'),
        '#submit' => ['::delete'],
        '#attributes' => [
          'class' => ['button', 'button--danger'],
        ],
        '#button_type' => 'danger',
        '#weight' => 20,
      ];
    }

    $form['config'] = ['#tree' => TRUE, '#weight' => $weight++];
    $form['config']['collection_id'] = [
      '#type' => 'hidden',
      '#value' => $this->collection->id(),
    ];
    $form['config']['parameter_name'] = [
      '#type' => 'hidden',
      '#value' => $parameter->getName(),
    ];
    $form['config']['parameter_type'] = [
      '#type' => 'hidden',
      '#value' => $parameter->getType(),
    ];
    $form['config']['weight'] = [
      '#type' => 'hidden',
      '#value' => $parameter_is_new ? $this->collection->getParameters()->count() + 1 : $parameter->getWeight(),
    ];

    return $form;
  }

  /**
   * Process callback.
   */
  public function processForm($element, FormStateInterface $form_state, $form) {
    $this->collection = $form_state->get('collection');
    $this->parameter = $form_state->get('parameter');
    return $element;
  }

  /**
   * After build callback.
   */
  public function afterBuild(array $form, FormStateInterface $form_state) {
    $parameter = $this->parameter;

    // Prevent Inline Entity Form from saving nested data.
    // @todo Find a better way to prevent submit handlers from saving data.
    if ($triggering_element = &$form_state->getTriggeringElement()) {
      if (isset($triggering_element['#ief_submit_trigger']) && !empty($triggering_element['#submit']) && is_array($triggering_element['#submit'])) {
        foreach ($triggering_element['#submit'] as $i => $submit_handler) {
          if (is_array($submit_handler) && (reset($submit_handler) === 'Drupal\\inline_entity_form\\ElementSubmit') && end($submit_handler) === 'trigger') {
            unset($triggering_element['#submit'][$i]);
          }
        }
      }
    }

    if ($form_state->hasValue(['parameter', 'settings']) && $parameter instanceof PluginFormInterface) {
      $values = $form_state->getValue(['parameter', 'settings']);
      array_walk_recursive($values, function (&$value) {
        if ($value === '_none') {
          $value = NULL;
        }
      });
      $form_state->setValue(['parameter', 'settings'], $values);
      $parameter_form_state = SubformState::createForSubform($form['parameter']['settings'], $form, $form_state);
      $parameter->submitConfigurationForm($form['parameter']['settings'], $parameter_form_state);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->collection->access('update')) {
      $form_state->setError($form, $this->t('You don\'t have permission to manage this configuration.'));
    }

    if ($triggering_element = &$form_state->getTriggeringElement()) {
      if (isset($triggering_element['#parents']) && reset($triggering_element['#parents']) !== 'actions') {
        return;
      }
    }

    $parameter = $this->parameter;
    if ($parameter instanceof PluginFormInterface) {
      $parameter_form_state = SubformState::createForSubform($form['parameter']['settings'], $form, $form_state);
      $parameter->validateConfigurationForm($form['parameter']['settings'], $parameter_form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!$this->collection->access('update')) {
      return;
    }
    if ($triggering_element = &$form_state->getTriggeringElement()) {
      if (isset($triggering_element['#parents']) && reset($triggering_element['#parents']) !== 'actions') {
        return;
      }
    }

    $parameter = $this->parameter;
    $config = [];
    $config['name'] = $form_state->getValue('name', $form_state->getValue(['config', 'parameter_name'], $config['name'] ?? ''));
    $config['label'] = $form_state->getValue('label', $config['label'] ?? '');
    $config['description'] = $form_state->getValue('description', $config['description'] ?? '');
    $config['weight'] = $form_state->getValue(['config', 'weight'], $config['weight'] ?? 0);
    $parameter->setConfiguration($config);
    if (isset($form['parameter']['settings']) && $parameter instanceof PluginFormInterface) {
      $parameter_form_state = SubformState::createForSubform($form['parameter']['settings'], $form, $form_state);
      $parameter->submitConfigurationForm($form['parameter']['settings'], $parameter_form_state);
    }
  }

  /**
   * Redirect after save submission callback.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function redirectAfterSave(array &$form, FormStateInterface $form_state) {
    if (!$this->collection->access('update')) {
      return;
    }

    $collection = $this->collection;
    $parts = explode('.', $collection->id());
    $target_type = $this->entityTypeManager->hasDefinition($parts[0]) ? $this->entityTypeManager->getDefinition($parts[0]) : NULL;
    $bundle_type_id = $target_type ? ($target_type->getBundleEntityType() ?: 'bundle') : NULL;
    $is_field_ui = $target_type && $target_type->entityClassImplements(FieldableEntityInterface::class) && (substr($this->routeMatch->getRouteName(), 0, 7) === 'entity.') && (substr($this->routeMatch->getRouteName(), 0, 29) !== 'entity.parameters_collection.');

    $parameter = $this->parameter;

    $t_args = [
      '%name' => $parameter->getName(),
    ];
    if ($this->savedNewParameter) {
      $message = $this->t('The new parameter "%name" has been added.', $t_args);
    }
    else {
      $message = $this->t('The changes for parameter "%name" have been saved.', $t_args);
    }

    $this->messenger->addStatus($message);

    if ($is_field_ui) {
      $form_state->setRedirect("entity.parameters.{$parts[0]}.edit", [
        'entity_type_id' => $parts[0],
        $bundle_type_id => $parts[1],
      ]);
    }
    else {
      $form_state->setRedirect("entity.parameters_collection.edit_form", [
        'parameters_collection' => $collection->id(),
      ]);
    }
  }

  /**
   * Save submission callback.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function save(array &$form, FormStateInterface $form_state): void {
    if (!$this->collection->access('update')) {
      return;
    }
    $collection = $this->collection;
    $parameter = $this->parameter;
    $parameter_name = $parameter->getName();
    $parameters_array = $collection->get('parameters');
    $parameter_is_new = !isset($parameters_array[$parameter_name]);
    $parameters_array[$parameter_name] = [
      'name' => $parameter_name,
      'type' => $parameter->getType(),
    ] + $parameter->getConfiguration();
    foreach ($parameter->getThirdPartyProviders() as $provider) {
      $parameters_array[$parameter_name]['third_party_settings'][$provider] = $parameter->getThirdPartySettings($provider);
    }
    if (($form_state->getValue(['config', 'parameter_name'], '') !== '') && $form_state->getValue(['config', 'parameter_name']) !== $parameter_name) {
      unset($parameters_array[$form_state->getValue(['config', 'parameter_name'], '')]);
    }
    $collection->setParameters($parameters_array);
    $collection->save();
    $this->savedNewParameter = $parameter_is_new;
  }

  /**
   * Delete submission callback that redirects to the parameter delete form.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function delete(array &$form, FormStateInterface $form_state): void {
    $collection = $this->collection;
    $parts = explode('.', $collection->id());
    $target_type = $this->entityTypeManager->hasDefinition($parts[0]) ? $this->entityTypeManager->getDefinition($parts[0]) : NULL;
    $bundle_type_id = $target_type ? ($target_type->getBundleEntityType() ?: 'bundle') : NULL;
    $is_field_ui = $target_type && $target_type->entityClassImplements(FieldableEntityInterface::class) && (substr($this->routeMatch->getRouteName(), 0, 7) === 'entity.') && (substr($this->routeMatch->getRouteName(), 0, 29) !== 'entity.parameters_collection.');

    if ($is_field_ui) {
      $form_state->setRedirect("entity.parameters.parameter.{$parts[0]}.delete", [
        'entity_type_id' => $parts[0],
        $bundle_type_id => $parts[1],
        'parameters_parameter_name' => $this->parameter->getName(),
      ]);
    }
    else {
      $form_state->setRedirect("parameters.parameter.delete", [
        'parameters_collection' => $collection->id(),
        'parameters_parameter_name' => $this->parameter->getName(),
      ]);
    }
  }

  /**
   * Initializes the form object properties.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\parameters\Entity\ParametersCollectionInterfac|null $collection
   *   The parameters collection.
   * @param \Drupal\parameters\Plugin\ParameterInterface|null $parameter
   *   The parameter plugin.
   */
  protected function initProperties(array &$form, FormStateInterface $form_state, ?ParametersCollectionInterface $collection = NULL, ?ParameterInterface $parameter = NULL): void {
    if ($form_state->has('collection')) {
      $this->collection = $form_state->get('collection');
      $this->parameter = $form_state->get('parameter');
    }
    elseif (isset($collection, $parameter)) {
      $this->collection = $collection;
      $this->parameter = $parameter;
    }
    elseif ($config_values = $form_state->getValue('config')) {
      $config_values = $form_state->getValue('config');
      $this->collection = ParametersCollection::load($config_values['collection_id']);
      $parameters = $this->collection->getParameters();
      if ($parameters->has($config_values['parameter_name'])) {
        $this->parameter = $parameters->get($config_values['parameter_name']);
      }
      else {
        $parameter_keys = [
          'name' => $config_values['parameter_name'],
          'type' => $config_values['parameter_type'],
        ];
        $this->parameter = ParameterManager::get()->createInstance($config_values['parameter_type'], $parameter_keys);
      }
    }
    else {
      throw new \InvalidArgumentException("Form build error: The parameter form cannot be built without any information about according configuration.");
    }
    $form_state->set('collection', $this->collection);
    $form_state->set('parameter', $this->parameter);
    if ($this->collection) {
      $form_state->set('langcode', $this->collection->language()->getId());
    }
  }

  /**
   * Checks whether the given parameter name already exists.
   *
   * @param mixed $name
   *   The requested name.
   *
   * @return bool
   *   TRUE if the parameter exists, FALSE otherwise.
   */
  public function parameterExists($name): bool {
    return isset($this->collection->get('parameters')[$name]);
  }

}
