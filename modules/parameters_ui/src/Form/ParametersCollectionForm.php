<?php

namespace Drupal\parameters_ui\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\parameters\Plugin\ParameterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for a parameters collection.
 */
class ParametersCollectionForm extends EntityForm {

  /**
   * The parameter plugin manager.
   *
   * @var \Drupal\parameters\Plugin\ParameterManager
   */
  protected PluginManagerInterface $parameterManager;

  /**
   * Constructs a new ParametersCollectionForm object.
   *
   * @param \Drupal\parameters\Plugin\ParameterManager $parameter_manager
   *   The parameter plugin manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(PluginManagerInterface $parameter_manager, RouteMatchInterface $route_match) {
    $this->parameterManager = $parameter_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static($container->get(ParameterManager::SERVICE_NAME), $container->get('current_route_match'));
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setModuleHandler($container->get('module_handler'));
    $instance->setRedirectDestination($container->get('redirect.destination'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    $collection = $this->entity;
    $parts = explode('.', $collection->id());
    $target_type = $this->entityTypeManager->hasDefinition($parts[0]) ? $this->entityTypeManager->getDefinition($parts[0]) : NULL;
    $bundle_type_id = $target_type ? ($target_type->getBundleEntityType() ?: 'bundle') : NULL;
    $is_field_ui = $target_type && $target_type->entityClassImplements(FieldableEntityInterface::class) && (substr($this->routeMatch->getRouteName(), 0, 7) === 'entity.') && (substr($this->routeMatch->getRouteName(), 0, 29) !== 'entity.parameters_collection.');

    $query_options = [
      'query' => [
        'destination' => $form_state->getValue('redirect_destination', $this->redirectDestination->get()),
      ],
    ];

    if (!$collection->getStatus()) {
      $this->messenger()->addWarning($this->t('This configuration is not active and thus will not get applied. Please contact your site administrator if that is not an expected status.'));
    }

    $form = parent::buildForm($form, $form_state);
    $weight = 0;

    $form['parameters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Parameters'),
      '#weight' => $weight++,
    ];
    $parameters = $collection->getParameters();
    if ($parameters->count()) {
      $form['parameters']['table'] = [
        '#attributes' => ['id' => Html::getUniqueId('parameters-table')],
        '#type' => 'table',
        '#parents' => ['parameters'],
        '#header' => [
          $this->t('Parameter'),
          $this->t('Usage'),
          $this->t('Value'),
          $this->t('Weight'),
          $this->t('Operations'),
        ],
        '#weight' => 10,
        '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'parameters-parameter-weight',
          ],
        ],
      ];
      /** @var \Drupal\parameters\Plugin\ParameterInterface $parameter */
      foreach ($parameters as $i => $parameter) {
        $definition = $parameter->getPluginDefinition();
        $config = $parameter->getConfiguration();
        $form['parameters']['table'][$i] = [
          '#attributes' => ['class' => ['draggable']],
          '#weight' => $config['weight'] ?? 0,
        ];
        $form['parameters']['table'][$i]['parameter'] = [
          '#type' => 'markup',
          '#markup' => '<p>' . $parameter->getLabel() . '</p>',
          '#weight' => 10,
        ];
        if ($description = $parameter->getDescription()) {
          $form['parameters']['table'][$i]['parameter']['#markup'] .= '<p><em>' . $description . '</em></p>';
        }
        $form['parameters']['table'][$i]['parameter']['#markup'] .= '<p>' . $this->t('Type: @type', ['@type' => $definition['label']]) . '</p>';

        $form['parameters']['table'][$i]['usage'] = [
          '#weight' => 20,
        ] + $parameter->getUsageHelp($collection);

        $form['parameters']['table'][$i]['value'] = [
          '#type' => 'markup',
          '#markup' => $parameter->getPreview(),
          '#weight' => 30,
        ];

        $form['parameters']['table'][$i]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight'),
          '#title_display' => 'invisible',
          '#default_value' => $config['weight'] ?? 0,
          '#attributes' => ['class' => ['parameters-parameter-weight']],
          '#delta' => 10,
          '#weight' => 40,
        ];

        $operations = [];
        if ($collection->access('update') && (!($parameter instanceof AccessibleInterface) || $parameter->access('update', $this->currentUser()))) {
          $url = $is_field_ui ? Url::fromRoute("entity.parameters.parameter.{$parts[0]}.edit", [
            'entity_type_id' => $parts[0],
            $bundle_type_id => $parts[1],
            'parameters_parameter_name' => $config['name'],
          ], $query_options) : Url::fromRoute("parameters.parameter.edit", [
            'parameters_collection' => $collection->id(),
            'parameters_parameter_name' => $config['name'],
          ], $query_options);
          $operations['edit'] = [
            'title' => $this->t('Edit'),
            'url' => $url,
            'weight' => 10,
          ];
        }
        if ($collection->access('delete') && (!($parameter instanceof AccessibleInterface) || $parameter->access('delete', \Drupal::currentUser()))) {
          if (!$collection->isLocked()) {
            $operations['delete'] = [
              'title' => $this->t('Delete'),
              'url' => $is_field_ui ? Url::fromRoute("entity.parameters.parameter.{$parts[0]}.delete", [
                'entity_type_id' => $parts[0],
                $bundle_type_id => $parts[1],
                'parameters_parameter_name' => $config['name'],
              ], $query_options) : Url::fromRoute("parameters.parameter.delete", [
                'parameters_collection' => $collection->id(),
                'parameters_parameter_name' => $config['name'],
              ], $query_options),
              'weight' => 20,
            ];
          }
        }
        $form['parameters']['table'][$i]['operations'] = [
          '#type' => 'operations',
          '#links' => $operations,
          '#weight' => 50,
        ];
      }
    }
    else {
      $form['parameters']['empty'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No parameters have been added yet.'),
        '#weight' => 10,
      ];
    }

    $weight += 100;
    $wrapper_id = Html::getUniqueId('add-parameter');
    $form['add_parameter'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add new parameter'),
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      '#weight' => $weight++,
    ];
    $form['add_parameter']['table'] = [
      '#type' => 'table',
      '#weight' => 10,
      '#header' => [$this->t('Type'), ''],
      '#attributes' => [
        'class' => ['parameters-form-add-parameter'],
      ],
    ];

    $form['add_parameter']['table'][0] = [
      '#parents' => ['add_parameter'],
    ];
    $form['add_parameter']['table'][0]['type'] = [
      '#type' => $this->moduleHandler->moduleExists('select2') ? 'select2' : 'select',
      '#options' => $this->getParameterTypeOptions($form, $form_state),
      '#default_value' => '_none',
      '#empty_value' => '_none',
      '#weight' => 20,
      '#ajax' => [
        'callback' => [static::class, 'addParameterAjax'],
        'wrapper' => $wrapper_id,
      ],
      '#executes_submit_callback' => TRUE,
      '#submit' => [[static::class, 'submitParameterAjax']],
      '#required' => TRUE,
    ];
    if ($form_state->getValue(['add_parameter', 'type'], '_none') !== '_none') {
      $form['add_parameter']['table'][0]['link'] = [
        '#type' => 'link',
        '#attributes' => [
          'class' => ['button', 'button-action', 'button--primary'],
        ],
        '#title' => $this->t('Add parameter'),
        '#url' => $is_field_ui ? Url::fromRoute("entity.parameters.parameter.{$parts[0]}.add", [
            'entity_type_id' => $parts[0],
            $bundle_type_id => $parts[1],
            'parameters_parameter_type' => $form_state->getValue(['add_parameter', 'type']),
          ], $query_options) : Url::fromRoute("parameters.parameter.add", [
            'parameters_collection' => $collection->id(),
            'parameters_parameter_type' => $form_state->getValue(['add_parameter', 'type']),
          ], $query_options),
      ];
    }
    else {
      $form['add_parameter']['table'][0]['link'] = [
        '#type' => 'button',
        '#disabled' => TRUE,
        '#button_type' => 'primary',
        '#value' => $this->t('Add parameter'),
      ];
    }

    $form['redirect_destination'] = [
      '#type' => 'hidden',
      '#default_value' => $this->redirectDestination->get(),
    ];

    $form['actions']['#weight'] = $weight++;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!$this->entity->access('update')) {
      return;
    }
    // Remove button and internal Form API values from submitted values.
    $form_state->cleanValues();
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    $collection = $this->entity;
    // The only thing that can be done within this form is changing the weight
    // order of the configured plugins. Update the new weights accordingly.
    $parameters_array = $collection->get('parameters');
    foreach ($form_state->getValue('parameters', []) as $i => $values) {
      $parameters_array[$i]['weight'] = $values['weight'];
    }
    $collection->setParameters($parameters_array);
  }

  /**
   * {@inheritdoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $config */
    $config = $this->entity;

    $config->save();

    $t_args = [
      '%label' => $config->label(),
    ];
    $message = $this->t('The %label parameters collection has been saved.', $t_args);

    $this->messenger()->addStatus($message);
  }

  /**
   * Get available parameter type options.
   *
   * @param array $form
   *   The current form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The according form state.
   *
   * @return array
   *   The parameter type options.
   */
  protected function getParameterTypeOptions(array $form, FormStateInterface $form_state): array {
    $collection_keys = [];

    $options = ['_none' => $this->t('- Select a type -')];
    $parameter_plugin_id = $form_state->getValue(['add_parameter', 'type'], '_none');
    /** @var \Drupal\parameters\Plugin\ParameterInterface[] $parameter_plugins */
    $parameter_plugins = [];
    foreach (array_keys($this->parameterManager->getDefinitions()) as $parameter_plugin_id) {
      $parameter_plugins[] = $this->parameterManager->createInstance($parameter_plugin_id, $collection_keys);
    }
    foreach ($parameter_plugins as $parameter_plugin) {
      if (($parameter_plugin instanceof AccessibleInterface) && !$parameter_plugin->access('create', $this->currentUser())) {
        continue;
      }
      $options[$parameter_plugin->getPluginId()] = $parameter_plugin->getPluginDefinition()['label'];
    }
    return $options;
  }

  /**
   * Checks whether the given parameter name already exists.
   *
   * @param mixed $name
   *   The requested name.
   *
   * @return bool
   *   TRUE if the parameter exists, FALSE otherwise.
   */
  public function parameterExists($name): bool {
    return isset($this->entity->get('parameters')[$name]);
  }

  /**
   * Ajax callback for adding a new parameter.
   *
   * @param array $form
   *   The current form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The according form state.
   *
   * @return array
   *   The part of the form that got refreshed via Ajax.
   */
  public static function addParameterAjax(array $form, FormStateInterface $form_state): array {
    return $form['add_parameter'];
  }

  /**
   * Submit ajax callback for adding a new parameter.
   *
   * @param array $form
   *   The current form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The according form state.
   */
  public static function submitParameterAjax(array $form, FormStateInterface $form_state): void {
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = [];

    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    $collection = $this->entity;

    if ($collection->getParameters()->count()) {
      $actions['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save ordering'),
        '#submit' => ['::submitForm', '::save'],
        '#weight' => 10,
      ];
    }

    $parts = explode('.', $collection->id());
    $target_type = $this->entityTypeManager->hasDefinition($parts[0]) ? $this->entityTypeManager->getDefinition($parts[0]) : NULL;
    $bundle_type_id = $target_type ? ($target_type->getBundleEntityType() ?: 'bundle') : NULL;

    if ($collection->isLocked()) {
      $actions['unlock'] = [
        '#type' => 'link',
        '#title' => $this->t('Unlock'),
        '#access' => $collection->access('unlock'),
        '#attributes' => [
          'class' => ['button', 'button--danger'],
        ],
        '#url' => Url::fromRoute("entity.parameters_collection.unlock_form", [
          'parameters_collection' => $collection->id(),
        ]),
        '#weight' => 20,
      ];
    }
    elseif ($collection->getParameters()->count()) {
      $actions['lock'] = [
        '#type' => 'link',
        '#title' => $this->t('Lock this collection'),
        '#access' => $collection->access('lock'),
        '#attributes' => [
          'class' => ['button', 'button--danger'],
        ],
        '#url' => Url::fromRoute("entity.parameters_collection.lock_form", [
          'parameters_collection' => $collection->id(),
        ]),
        '#weight' => 20,
      ];
      $actions['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete all parameters'),
        '#access' => $collection->access('delete'),
        '#attributes' => [
          'class' => ['button', 'button--danger'],
        ],
        '#url' => Url::fromRoute("entity.parameters_collection.delete_form", [
          'parameters_collection' => $collection->id(),
        ]),
        '#weight' => 30,
      ];
    }
    elseif ($collection->isDeletable()) {
      $actions['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete this collection'),
        '#access' => $collection->access('delete'),
        '#attributes' => [
          'class' => ['button', 'button--danger'],
        ],
        '#url' => Url::fromRoute("entity.parameters_collection.delete_form", [
          'parameters_collection' => $collection->id(),
        ]),
        '#weight' => 30,
      ];
    }

    return $actions;
  }

}
