<?php

namespace Drupal\parameters_ui\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form to lock a parameters collection.
 *
 * @ingroup entity_api
 */
class ParametersCollectionLockForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->getEntity()->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('You are about to lock the parameters collection %label.', [
      '%label' => $this->getEntity()->label() ?? $this->getEntity()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('You are about to lock the parameters collection %label. Once the collection is locked, parameters cannot be removed. Once locked, existing parameters can still be changed, and new parameters can still be added, but no parameter can be removed anymore.', [
      '%label' => $this->getEntity()->label() ?? $this->getEntity()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    $collection = $this->getEntity();
    $collection->lockAndSave();
    $this->messenger()->addStatus($this->t("The collection has been locked."));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
