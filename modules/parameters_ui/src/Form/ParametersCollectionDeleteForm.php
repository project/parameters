<?php

namespace Drupal\parameters_ui\Form;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for deleting a parameters collection.
 */
class ParametersCollectionDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('You are about to delete the parameters collection %label.', ['%label' => $this->entity->label()]) . '<p>' . $this->t('This action cannot be undone.') . '</p>';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->getRedirectUrl();
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigNamesToDelete(ConfigEntityInterface $entity) {
    /** @var \Drupal\parameters\Entity\ParametersCollection $entity */
    $autolock = $entity::$autolock;
    $entity::$autolock = FALSE;
    $names = parent::getConfigNamesToDelete($entity);
    $entity::$autolock = $autolock;
    return $names;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\parameters\Entity\ParametersCollection $collection */
    $collection = $this->getEntity();
    if (strpos($collection->id(), '.') || $collection->isDeletable()) {
      $collection->delete();
      $this->logger('parameters')->notice("User with ID %uid deleted parameters collection having ID %collection_id.", [
        '%uid' => $this->currentUser()->id(),
        '%collection_id' => $collection->id(),
      ]);
    }
    else {
      $collection->setParameters([])->save();
      $this->logger('parameters')->notice("User with ID %uid deleted all parameters of collection having ID %collection_id.", [
        '%uid' => $this->currentUser()->id(),
        '%collection_id' => $collection->id(),
      ]);
    }
    $this->messenger()->addStatus($this->getDeletionMessage());
    $form_state->setRedirectUrl($this->getCancelUrl());
    $this->logDeletionMessage();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletionMessage() {
    /** @var \Drupal\parameters\Entity\ParametersCollection $collection */
    $collection = $this->getEntity();
    return $this->t('All parameters of collection %label have been deleted.', [
      '%label' => $collection->label() ?? $collection->id(),
    ]);
  }

}
