<?php

namespace Drupal\parameters_ui\Form;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Plugin\ParameterInterface;

/**
 * Form for deleting a configured parameter from a collection.
 */
class ParameterDeleteForm extends ParameterForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?ParametersCollectionInterface $collection = NULL, ?ParameterInterface $parameter = NULL) {
    $form = parent::buildForm($form, $form_state, $collection, $parameter);

    $form['#attributes']['class'][] = 'confirmation';
    $form['title'] = [
      '#type' => 'markup',
      '#markup' => '<h2>' . $this->t('You are about to delete the parameter <em>@parameter</em> from the collection @collection.', [
        '@parameter' => $this->parameter->getName(),
        '@collection' => $this->collection->label(),
      ]) . '</h2>',
    ];
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('This action cannot be undone.') . '</p>',
    ];

    unset($form['parameter'], $form['actions']['delete']);

    $form['actions']['submit']['#value'] = $this->t('Confirm');
    $form['actions']['submit']['#submit'] = ['::delete', '::redirectAfterSave'];
    $form['actions']['submit']['#button_type'] = 'danger';
    $weight = $form['actions']['submit']['#weight'];
    $weight += 10;
    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancel'],
      '#attributes' => [
        'class' => ['button'],
      ],
      '#weight' => $weight++,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array &$form, FormStateInterface $form_state): void {
    if (!$this->collection->access('delete')) {
      return;
    }
    $collection = $this->collection;
    $parameter = $this->parameter;
    $config = $parameter->getConfiguration();
    $parameters_array = $collection->get('parameters');
    unset($parameters_array[$parameter->getName()]);
    if (!empty($parameters_array) || $collection->isLocked() || !strpos($collection->id(), '.')) {
      $collection->setParameters($parameters_array);
      $collection->save();
    }
    else {
      $collection->delete();
    }
    \Drupal::logger('parameters')->notice("User with ID %uid deleted parameter %parameter_name from collection having ID %collection_id.", [
      '%uid' => \Drupal::currentUser()->id(),
      '%parameter_name' => $config['name'],
      '%collection_id' => $collection->id(),
    ]);
    $this->messenger->addStatus($this->t('The parameter "%name" has been successfully removed.', ['%name' => $config['name']]));
  }

  /**
   * Cancel submission callback.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function cancel(array &$form, FormStateInterface $form_state) {
    $collection = $this->collection;
    $parts = explode('.', $collection->id());
    $target_type = $this->entityTypeManager->hasDefinition($parts[0]) ? $this->entityTypeManager->getDefinition($parts[0]) : NULL;
    $bundle_type_id = $target_type ? ($target_type->getBundleEntityType() ?: 'bundle') : NULL;
    $is_field_ui = $target_type && $target_type->entityClassImplements(FieldableEntityInterface::class) && (substr($this->routeMatch->getRouteName(), 0, 7) === 'entity.') && (substr($this->routeMatch->getRouteName(), 0, 29) !== 'entity.parameters_collection.');
    if ($is_field_ui) {
      $form_state->setRedirect("entity.parameters.{$parts[0]}.edit", [
        'entity_type_id' => $parts[0],
        $bundle_type_id => $parts[1],
      ]);
    }
    else {
      $form_state->setRedirect("entity.parameters_collection.edit_form", [
        'parameters_collection' => $collection->id(),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->collection->access('delete')) {
      $form_state->setError($form, $this->t('You don\'t have permission to manage this configuration.'));
    }
  }

}
