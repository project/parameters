<?php

namespace Drupal\parameters_ui\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Entity\ParametersCollectionStorage;

/**
 * Form for adding a new parameters collection.
 */
class ParametersCollectionAddForm implements FormInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'parameters_collection_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Collection label'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this, 'collectionExists'],
        'source' => ['label'],
      ],
      '#title' => $this->t('Collection ID'),
      '#required' => TRUE,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

   /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $collection = ParametersCollectionStorage::get()->create([
      'id' => $form_state->getValue('id'),
      'label' => $form_state->getValue('label'),
      'status' => TRUE,
      'deletable' => TRUE,
      'parameters' => [],
    ]);
    $collection->save();
    \Drupal::messenger()->addMessage($this->t('The parameters collection has been added.'));
    $form_state->setRedirect('entity.' . ParametersCollectionInterface::ENTITY_TYPE_ID . '.edit_form', ['parameters_collection' => $collection->id()]);
  }

  /**
   * Exists callback.
   *
   * @return bool
   *   Returns TRUE if collection exists.
   */
  public function collectionExists(string $id): bool {
    return !empty(ParametersCollectionStorage::get()->load($id));
  }

}
