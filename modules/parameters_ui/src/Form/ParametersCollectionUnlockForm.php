<?php

namespace Drupal\parameters_ui\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form to unlock a parameters collection.
 *
 * @ingroup entity_api
 */
class ParametersCollectionUnlockForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->getEntity()->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('You are about to unlock the parameters collection %label.', [
      '%label' => $this->getEntity()->label() ?? $this->getEntity()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('You are about to unlock the parameters collection %label. Once a collection is unlocked, contained parameters can be removed from the system. Please only unlock collections with care, as other components in the system may rely on the existence of contained parameters.', [
      '%label' => $this->getEntity()->label() ?? $this->getEntity()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    $collection = $this->getEntity();
    $collection->lockAndSave(FALSE);
    $this->messenger()->addStatus($this->t("The collection has been unlocked."));
    $this->logger('parameters')->notice("User with ID %uid unlocked parameters collection having ID %collection_id.", [
      '%uid' => $this->currentUser()->id(),
      '%collection_id' => $collection->id(),
    ]);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
